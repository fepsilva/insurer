package com.fepsilva.insurer.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "CARS")
public class CarEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false)
	private Long id;

	@Column(name = "MODEL", nullable = false)
	private String model;

	@Column(name = "MANUFACTURER", nullable = false)
	private String manufacturer;

	@Column(name = "MODEL_YEAR", nullable = false)
	private String modelYear;

	@Column(name = "FIPE_VALUE", nullable = false)
	private Float fipeValue;

}