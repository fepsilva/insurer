package com.fepsilva.insurer.usecase.cardriver;

public interface CarDriverDeleteUseCase {

	public void execute(Long carDriverId);

}