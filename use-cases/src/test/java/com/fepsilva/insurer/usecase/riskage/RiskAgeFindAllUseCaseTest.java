package com.fepsilva.insurer.usecase.riskage;

import com.fepsilva.insurer.converter.RiskAgeModelConverterTest;
import com.fepsilva.insurer.dao.RiskAgeDao;
import com.fepsilva.insurer.entity.RiskAgeEntity;
import com.fepsilva.insurer.model.RiskAge;
import com.fepsilva.insurer.usecase.riskage.impl.RiskAgeFindAllUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class RiskAgeFindAllUseCaseTest {

	@Mock
	private RiskAgeDao riskAgeDao;

	@InjectMocks
	private RiskAgeFindAllUseCaseImpl riskAgeFindAllUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void executeSuccessful() {
		// Given
		List<RiskAgeEntity> listRiskAgeEntity = List.of(
				RiskAgeModelConverterTest.createNewRiskAgeEntity(1L, 18L, 25L, BigDecimal.ZERO),
				RiskAgeModelConverterTest.createNewRiskAgeEntity(2L, 18L, 25L, BigDecimal.ZERO),
				RiskAgeModelConverterTest.createNewRiskAgeEntity(3L, 18L, 25L, BigDecimal.ZERO));

		// When
		Mockito.when(this.riskAgeDao.findAll())
				.thenReturn(listRiskAgeEntity);

		// Then
		List<RiskAge> listRiskAge = this.riskAgeFindAllUseCase.execute();
		Assertions.assertNotNull(listRiskAge);
		Assertions.assertEquals(listRiskAge.size(), 3);

		for (int i = 0; i < listRiskAgeEntity.size(); i++) {
			RiskAgeModelConverterTest.isEquals(listRiskAgeEntity.get(i), listRiskAge.get(i));
		}
	}

	@Test
	@DisplayName("Method execute() - Empty")
	public void executeEmpty() {
		// When
		Mockito.when(this.riskAgeDao.findAll())
				.thenReturn(null);

		// Then
		List<RiskAge> listRiskAge = this.riskAgeFindAllUseCase.execute();
		Assertions.assertNotNull(listRiskAge);
		Assertions.assertEquals(listRiskAge.size(), 0);
	}

}