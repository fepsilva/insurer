package com.fepsilva.insurer.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RiskAge {

	private Long id;
	private Long beginAge;
	private Long endAge;
	private BigDecimal percentRisk;

}