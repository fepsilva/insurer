package com.fepsilva.insurer.repository;

import com.fepsilva.insurer.entity.DriverEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DriverRepository extends JpaRepository<DriverEntity, Long> {

}