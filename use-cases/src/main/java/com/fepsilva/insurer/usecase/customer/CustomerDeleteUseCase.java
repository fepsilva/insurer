package com.fepsilva.insurer.usecase.customer;

public interface CustomerDeleteUseCase {

	public void execute(Long customerId);

}