package com.fepsilva.insurer.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CarDriver {

	private Long id;
	private Car car;
	private Driver driver;
	private Boolean mainDriver;

}