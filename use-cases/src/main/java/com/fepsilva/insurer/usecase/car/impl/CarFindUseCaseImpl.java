package com.fepsilva.insurer.usecase.car.impl;

import com.fepsilva.insurer.converter.CarModelConverter;
import com.fepsilva.insurer.dao.CarDao;
import com.fepsilva.insurer.model.Car;
import com.fepsilva.insurer.usecase.car.CarFindUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CarFindUseCaseImpl implements CarFindUseCase {

	@Autowired
	private CarDao carDao;

	public Car execute(Long carId) {
		return this.carDao.find(carId)
				.map(CarModelConverter::entityToModel)
				.orElseGet(() -> null);
	}

}