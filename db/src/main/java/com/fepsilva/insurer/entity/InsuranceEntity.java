package com.fepsilva.insurer.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "INSURANCES")
public class InsuranceEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "CUSTOMER_ID", nullable = false)
	private CustomerEntity customer;

	@Column(name = "CREATION_DT", nullable = false, updatable = false)
	private LocalDateTime creationDt;

	@Column(name = "UPDATED_DT", nullable = false)
	private LocalDateTime updatedDt;

	@OneToOne
	@JoinColumn(name = "CAR_ID", referencedColumnName = "ID", nullable = false)
	private CarEntity car;

	@Column(name = "IS_ACTIVE", nullable = false)
	private Boolean active;

	@Column(name = "PERCENT_RISK", nullable = false)
	private Float percentRisk;

	@Column(name = "FIPE_VALUE", nullable = false)
	private Float fipeValue;

}