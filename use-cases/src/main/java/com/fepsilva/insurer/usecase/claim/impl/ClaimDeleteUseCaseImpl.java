package com.fepsilva.insurer.usecase.claim.impl;

import com.fepsilva.insurer.dao.ClaimDao;
import com.fepsilva.insurer.usecase.claim.ClaimDeleteUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClaimDeleteUseCaseImpl implements ClaimDeleteUseCase {

	@Autowired
	private ClaimDao claimDao;

	public void execute(Long claimId) {
		this.claimDao.delete(claimId);
	}

}