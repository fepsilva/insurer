package com.fepsilva.insurer.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Insurance {

	private Long id;
	private Customer customer;
	private LocalDateTime creationDt;
	private LocalDateTime updatedDt;
	private Car car;
	private Boolean active;
	private Float percentRisk;
	private Float fipeValue;

}