package com.fepsilva.insurer.usecase.customer.impl;

import com.fepsilva.insurer.dao.CustomerDao;
import com.fepsilva.insurer.usecase.customer.CustomerDeleteUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerDeleteUseCaseImpl implements CustomerDeleteUseCase {

	@Autowired
	private CustomerDao customerDao;

	public void execute(Long customerId) {
		this.customerDao.delete(customerId);
	}

}