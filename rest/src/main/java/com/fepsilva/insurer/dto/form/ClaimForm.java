package com.fepsilva.insurer.dto.form;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Formulário de cadastro de sinistro")
public class ClaimForm {

	@NotNull(message = "Não pode estar vazio")
	@Schema(description = "Identificador do veículo envolvido no sinistro")
	private Long carId;

	@NotNull(message = "Não pode estar vazio")
	@Schema(description = "Identificador do motorista envolvido no sinistro")
	private Long driverId;

	@NotNull(message = "Não pode estar vazio")
	@Schema(description = "Data do evento do sinistro")
	private LocalDate eventDate;

}