package com.fepsilva.insurer.usecase.riskage;

import com.fepsilva.insurer.converter.RiskAgeModelConverter;
import com.fepsilva.insurer.converter.RiskAgeModelConverterTest;
import com.fepsilva.insurer.dao.RiskAgeDao;
import com.fepsilva.insurer.entity.RiskAgeEntity;
import com.fepsilva.insurer.model.RiskAge;
import com.fepsilva.insurer.usecase.riskage.impl.RiskAgeCreateUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

@ExtendWith(MockitoExtension.class)
public class RiskAgeCreateUseCaseTest {

	@Mock
	private RiskAgeDao riskAgeDao;

	@InjectMocks
	private RiskAgeCreateUseCaseImpl riskAgeCreateUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void executeSuccessful() {
		// Given
		RiskAge riskAge = RiskAgeModelConverterTest.createNewRiskAge(null, 18L, 25L, BigDecimal.ZERO);
		RiskAgeEntity riskAgeEntity = RiskAgeModelConverter.modelToEntity(riskAge);
		riskAgeEntity.setId(1L);

		// When
		Mockito.when(this.riskAgeDao.persist(RiskAgeModelConverter.modelToEntity(riskAge)))
				.thenReturn(riskAgeEntity);

		// Then
		riskAge = this.riskAgeCreateUseCase.execute(riskAge);
		Assertions.assertNotNull(riskAge);
		Assertions.assertNotNull(riskAge.getId());
	}

	@Test
	@DisplayName("Method execute() - Should return null")
	public void executeNull() {
		// Given
		RiskAge riskAge = RiskAgeModelConverterTest.createNewRiskAge(null, 18L, 25L, BigDecimal.ZERO);

		// When
		Mockito.when(this.riskAgeDao.persist(RiskAgeModelConverter.modelToEntity(riskAge)))
				.thenReturn(null);

		// Then
		riskAge = this.riskAgeCreateUseCase.execute(riskAge);
		Assertions.assertNull(riskAge);
	}

}