package com.fepsilva.insurer.usecase.cardriver.impl;

import com.fepsilva.insurer.dao.CarDriverDao;
import com.fepsilva.insurer.usecase.cardriver.CarDriverDeleteUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CarDriverDeleteUseCaseImpl implements CarDriverDeleteUseCase {

	@Autowired
	private CarDriverDao carDriverDao;

	public void execute(Long carDriverId) {
		this.carDriverDao.delete(carDriverId);
	}

}