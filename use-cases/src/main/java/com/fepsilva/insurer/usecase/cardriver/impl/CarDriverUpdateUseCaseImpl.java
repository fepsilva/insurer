package com.fepsilva.insurer.usecase.cardriver.impl;

import com.fepsilva.insurer.converter.CarDriverModelConverter;
import com.fepsilva.insurer.dao.CarDriverDao;
import com.fepsilva.insurer.model.CarDriver;
import com.fepsilva.insurer.usecase.cardriver.CarDriverUpdateUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CarDriverUpdateUseCaseImpl implements CarDriverUpdateUseCase {

	@Autowired
	private CarDriverDao carDriverDao;

	public CarDriver execute(Long carId, CarDriver carDriver) {
		carDriver.setId(carId);

		return Optional.ofNullable(this.carDriverDao.update(CarDriverModelConverter.modelToEntity(carDriver)))
				.map(CarDriverModelConverter::entityToModel)
				.orElseGet(() -> null);
	}

}