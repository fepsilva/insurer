package com.fepsilva.insurer.usecase.customer;

import com.fepsilva.insurer.model.Customer;

public interface CustomerFindUseCase {

	public Customer execute(Long customerId);

}