package com.fepsilva.insurer.usecase.customer.impl;

import com.fepsilva.insurer.converter.CustomerModelConverter;
import com.fepsilva.insurer.dao.CustomerDao;
import com.fepsilva.insurer.model.Customer;
import com.fepsilva.insurer.usecase.customer.CustomerFindUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerFindUseCaseImpl implements CustomerFindUseCase {

	@Autowired
	private CustomerDao customerDao;

	public Customer execute(Long customerId) {
		return this.customerDao.find(customerId)
				.map(CustomerModelConverter::entityToModel)
				.orElseGet(() -> null);
	}

}