package com.fepsilva.insurer.usecase.cardriver.impl;

import com.fepsilva.insurer.converter.CarDriverModelConverter;
import com.fepsilva.insurer.dao.CarDriverDao;
import com.fepsilva.insurer.model.CarDriver;
import com.fepsilva.insurer.usecase.cardriver.CarDriverCreateUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CarDriverCreateUseCaseImpl implements CarDriverCreateUseCase {

	@Autowired
	private CarDriverDao carDriverDao;

	public CarDriver execute(CarDriver carDriver) {
		return Optional.ofNullable(this.carDriverDao.persist(CarDriverModelConverter.modelToEntity(carDriver)))
				.map(CarDriverModelConverter::entityToModel)
				.orElseGet(() -> null);
	}

}