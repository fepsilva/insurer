package com.fepsilva.insurer.dao;

import com.fepsilva.insurer.entity.CustomerEntity;

import java.util.List;
import java.util.Optional;

public interface CustomerDao {

	public CustomerEntity persist(CustomerEntity customerEntity);
	public List<CustomerEntity> findAll();
	public Optional<CustomerEntity> find(Long customerId);
	public CustomerEntity update(CustomerEntity customerEntity);
	public void delete(Long customerId);

}