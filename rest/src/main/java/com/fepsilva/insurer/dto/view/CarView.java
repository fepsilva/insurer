package com.fepsilva.insurer.dto.view;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Visualização do cadastro de carro")
public class CarView {

	@Schema(description = "Identificador do carro")
	private Long id;

	@Schema(description = "Modelo do veículo")
	private String model;

	@Schema(description = "Fabricante do veículo")
	private String manufacturer;

	@Schema(description = "Ano do modelo do veículo")
	private String modelYear;

	@Schema(description = "Valor do veículo")
	private Float fipeValue;

}