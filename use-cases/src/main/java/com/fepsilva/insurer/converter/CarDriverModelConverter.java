package com.fepsilva.insurer.converter;

import com.fepsilva.insurer.entity.CarDriverEntity;
import com.fepsilva.insurer.entity.ClaimEntity;
import com.fepsilva.insurer.model.CarDriver;
import com.fepsilva.insurer.model.Claim;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

public class CarDriverModelConverter {

	public static CarDriverEntity modelToEntity(CarDriver car) {
		if (car == null) {
			return null;
		}

		CarDriverEntity carDriverEntity = getModelMapper().map(car, CarDriverEntity.class);
		carDriverEntity.setCar(CarModelConverter.modelToEntity(car.getCar()));
		carDriverEntity.setDriver(DriverModelConverter.modelToEntity(car.getDriver()));

		return carDriverEntity;
	}

	public static CarDriver entityToModel(CarDriverEntity carEntity) {
		if (carEntity == null) {
			return null;
		}

		CarDriver carDriver = getModelMapper().map(carEntity, CarDriver.class);
		carDriver.setCar(CarModelConverter.entityToModel(carEntity.getCar()));
		carDriver.setDriver(DriverModelConverter.entityToModel(carEntity.getDriver()));

		return carDriver;
	}

	private static ModelMapper getModelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

		return modelMapper;
	}

}