package com.fepsilva.insurer.usecase.cardriver;

import com.fepsilva.insurer.model.CarDriver;

public interface CarDriverCreateUseCase {

	public CarDriver execute(CarDriver carDriver);

}