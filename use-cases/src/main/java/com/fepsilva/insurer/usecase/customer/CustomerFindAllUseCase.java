package com.fepsilva.insurer.usecase.customer;

import com.fepsilva.insurer.model.Customer;

import java.util.List;

public interface CustomerFindAllUseCase {

	public List<Customer> execute();

}