package com.fepsilva.insurer.usecase.driver;

import com.fepsilva.insurer.converter.DriverModelConverter;
import com.fepsilva.insurer.converter.DriverModelConverterTest;
import com.fepsilva.insurer.dao.DriverDao;
import com.fepsilva.insurer.entity.DriverEntity;
import com.fepsilva.insurer.model.Driver;
import com.fepsilva.insurer.usecase.driver.impl.DriverCreateUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

@ExtendWith(MockitoExtension.class)
public class DriverCreateUseCaseTest {

	@Mock
	private DriverDao driverDao;

	@InjectMocks
	private DriverCreateUseCaseImpl driverCreateUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void executeSuccessful() {
		// Given
		Driver driver = DriverModelConverterTest.createNewDriver(1L, "teste", LocalDate.now());
		DriverEntity driverEntity = DriverModelConverter.modelToEntity(driver);
		driverEntity.setId(1L);

		// When
		Mockito.when(this.driverDao.persist(DriverModelConverter.modelToEntity(driver)))
				.thenReturn(driverEntity);

		// Then
		driver = this.driverCreateUseCase.execute(driver);
		Assertions.assertNotNull(driver);
		Assertions.assertNotNull(driver.getId());
	}

	@Test
	@DisplayName("Method execute() - Should return null")
	public void executeNull() {
		// Given
		Driver driver = DriverModelConverterTest.createNewDriver(1L, "teste", LocalDate.now());

		// When
		Mockito.when(this.driverDao.persist(DriverModelConverter.modelToEntity(driver)))
				.thenReturn(null);

		// Then
		driver = this.driverCreateUseCase.execute(driver);
		Assertions.assertNull(driver);
	}

}