package com.fepsilva.insurer.converter;

import com.fepsilva.insurer.dto.form.RiskAgeForm;
import com.fepsilva.insurer.dto.view.RiskAgeView;
import com.fepsilva.insurer.model.RiskAge;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

@ExtendWith(MockitoExtension.class)
public class RiskAgeDtoConverterTest {

	@Test
	@DisplayName("Method idToEntity() - Should execute successful")
	public void idToEntitySuccessful() {
		// Given
		Long riskAgeId = 1L;

		// Then
		RiskAge riskAge = RiskAgeDtoConverter.idToEntity(riskAgeId);
		Assertions.assertNotNull(riskAge);
		Assertions.assertEquals(riskAge.getId(), riskAgeId);
	}

	@Test
	@DisplayName("Method dtoToEntity() - Should execute successful")
	public void dtoToEntitySuccessful() {
		// Given
		RiskAgeForm riskAgeForm = createNewRiskAgeForm(18L, 25L, BigDecimal.ONE);

		// Then
		RiskAge riskAge = RiskAgeDtoConverter.dtoToEntity(riskAgeForm);
		isEquals(riskAge, riskAgeForm);
	}

	@Test
	@DisplayName("Method dtoToEntity() - Without value")
	public void dtoToEntityWithoutValue() {
		// Given
		RiskAgeForm riskAgeForm = null;

		// Then
		RiskAge riskAge = RiskAgeDtoConverter.dtoToEntity(riskAgeForm);
		Assertions.assertNull(riskAge);
	}

	@Test
	@DisplayName("Method entityToDto() - Should execute successful")
	public void entityToDtoSuccessful() {
		// Given
		RiskAge riskAge = createNewRiskAge(1L, 18L, 25L, BigDecimal.ONE);

		// Then
		RiskAgeView riskAgeView = RiskAgeDtoConverter.entityToDto(riskAge);
		isEquals(riskAgeView, riskAge);
	}

	@Test
	@DisplayName("Method entityToModel() - Without value")
	public void entityToModelWithoutValue() {
		// Given
		RiskAge riskAge = null;

		// Then
		RiskAgeView riskAgeView = RiskAgeDtoConverter.entityToDto(riskAge);
		Assertions.assertNull(riskAgeView);
	}

	public static RiskAgeForm createNewRiskAgeForm(Long beginAge, Long endAge, BigDecimal percentRisk) {
		return RiskAgeForm.builder()
				.beginAge(beginAge)
				.endAge(endAge)
				.percentRisk(percentRisk)
				.build();
	}

	public static RiskAge createNewRiskAge(Long id, Long beginAge, Long endAge, BigDecimal percentRisk) {
		return RiskAge.builder()
				.id(id)
				.beginAge(beginAge)
				.endAge(endAge)
				.percentRisk(percentRisk)
				.build();
	}

	public static void isEquals(RiskAge riskAge, RiskAgeForm riskAgeForm) {
		Assertions.assertNotNull(riskAge);
		Assertions.assertNotNull(riskAgeForm);

		Assertions.assertEquals(riskAge.getBeginAge(), riskAgeForm.getBeginAge());
		Assertions.assertEquals(riskAge.getEndAge(), riskAgeForm.getEndAge());
		Assertions.assertEquals(riskAge.getPercentRisk(), riskAgeForm.getPercentRisk());
	}

	public static void isEquals(RiskAgeView riskAgeView, RiskAge riskAge) {
		Assertions.assertNotNull(riskAgeView);
		Assertions.assertNotNull(riskAge);

		Assertions.assertEquals(riskAgeView.getId(), riskAge.getId());
		Assertions.assertEquals(riskAgeView.getBeginAge(), riskAge.getBeginAge());
		Assertions.assertEquals(riskAgeView.getEndAge(), riskAge.getEndAge());
		Assertions.assertEquals(riskAgeView.getPercentRisk(), riskAge.getPercentRisk());
	}

}