# Execução do projeto

Essa documentação é para ensinar como rodar o projeto local na máquina

## Requisitos

- Ter o Git instalado, caso não tenha utilize este <a href="https://git-scm.com/downloads" download>link</a>
- Ter uma IDE instalada, para este exemplo iremos utilizar o IntelliJ IDEA Community Edition, caso não tenha pode baixar no <a href="https://www.jetbrains.com/idea/download/#section=windows">link</a>

## Passo a Passo

1. Realize o clone do projeto através do comando

``git clone https://gitlab.com/fepsilva/insurer.git``

![run01.PNG](run01.PNG)

2. Como pode ver ele irá realizar o download corretamente na branch **develop**

![run02.PNG](run02.PNG)

3. Abra o IntelliJ IDEA Community Edition e selecione o botão **Open**

![run03.PNG](run03.PNG)

4. Ache nas suas pastas o projeto que acabou de clonar e abra ele

![run04.PNG](run04.PNG)

5. Ache a classe de inicialização conforme demonstrado e clique no botão verde de **Play**

![run05.PNG](run05.PNG)

6. Este projeto utiliza Lombok, caso aparece uma janela pedindo para habilitar, clique em sim

![run06.PNG](run06.PNG)

7. O projeto deve subir sem nenhum problema

![run06.PNG](run07.PNG)

[<< Voltar para página principal](/README.md)