package com.fepsilva.insurer.dao;

import com.fepsilva.insurer.dao.impl.CarDaoImpl;
import com.fepsilva.insurer.entity.CarEntity;
import com.fepsilva.insurer.repository.CarRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class CarDaoTest {

	@Mock
	private CarRepository carRepository;

	@InjectMocks
	private CarDaoImpl carDao;

	@Test
	@DisplayName("Method persist() - Should execute successful")
	public void persistSuccessful() {
		// Given
		Long id = 1L;
		String model = "teste";
		String manufacturer = "teste";
		String modelYear = "2023";
		Float fipeValue = 0F;

		CarEntity carEntity = createNewCarEntity(null, model, manufacturer, modelYear, fipeValue);
		CarEntity carEntityNew = createNewCarEntity(id, model, manufacturer, modelYear, fipeValue);

		// When
		Mockito.when(this.carRepository.save(carEntity))
				.thenReturn(carEntityNew);

		// Then
		carEntity = carDao.persist(carEntity);
		Assertions.assertNotNull(carEntity.getId());
	}

	@Test
	@DisplayName("Method findAll() - Should execute successful")
	public void findAllSuccessful() {
		// Given
		List<CarEntity> listCarEntity = List.of(
				createNewCarEntity(1L, "teste1", "teste1", "2023", 1F),
				createNewCarEntity(2L, "teste2", "teste2", "2023", 2F),
				createNewCarEntity(3L, "teste3", "teste3", "2023", 3F));

		// When
		Mockito.when(this.carRepository.findAll())
				.thenReturn(listCarEntity);

		// Then
		listCarEntity = carDao.findAll();
		Assertions.assertNotNull(listCarEntity);
		Assertions.assertEquals(listCarEntity.size(), 3);
	}

	@Test
	@DisplayName("Method find() - Should execute successful")
	public void findSuccessful() {
		// Given
		Long id = 1L;
		CarEntity carEntity = createNewCarEntity(id, "teste1", "teste1", "2023", 1F);

		// When
		Mockito.when(this.carRepository.findById(id))
				.thenReturn(Optional.of(carEntity));

		// Then
		Optional<CarEntity> optionalCarEntity = carDao.find(id);
		Assertions.assertNotNull(optionalCarEntity.get());
		Assertions.assertEquals(optionalCarEntity.get().getId(), id);
	}

	@Test
	@DisplayName("Method update() - Should execute successful")
	public void updateSuccessful() {
		// Given
		CarEntity carEntity = createNewCarEntity(1L, "teste", "teste", "2023", 1F);

		// When
		Mockito.when(this.carRepository.existsById(carEntity.getId()))
				.thenReturn(true);
		Mockito.when(this.carRepository.save(carEntity))
				.thenReturn(carEntity);

		// Then
		carEntity = carDao.update(carEntity);
		Assertions.assertNotNull(carEntity.getId());
	}

	@Test
	@DisplayName("Method update() - Should not found")
	public void updateNotFound() {
		// Given
		CarEntity carEntity = createNewCarEntity(1L, "teste", "teste", "2023", 1F);

		// When
		Mockito.when(this.carRepository.existsById(carEntity.getId()))
				.thenReturn(false);

		// Then
		carEntity = carDao.update(carEntity);
		Assertions.assertNull(carEntity);
	}

	@Test
	@DisplayName("Method delete() - Should execute successful")
	public void deleteSuccessful() {
		// Given
		Long id = 1L;

		// Then
		Assertions.assertDoesNotThrow(() -> carDao.delete(id));
	}

	public static CarEntity createNewCarEntity(Long id, String model, String manufacturer, String modelYear, Float fipeValue) {
		return CarEntity.builder()
				.id(id)
				.model(model)
				.manufacturer(manufacturer)
				.modelYear(modelYear)
				.fipeValue(fipeValue)
				.build();
	}

}