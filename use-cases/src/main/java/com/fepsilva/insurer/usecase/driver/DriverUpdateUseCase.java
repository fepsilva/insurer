package com.fepsilva.insurer.usecase.driver;

import com.fepsilva.insurer.model.Driver;

public interface DriverUpdateUseCase {

	public Driver execute(Long driverId, Driver driver);

}