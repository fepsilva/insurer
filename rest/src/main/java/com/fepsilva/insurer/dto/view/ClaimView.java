package com.fepsilva.insurer.dto.view;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Visualização do cadastro de sinistro")
public class ClaimView {

	@Schema(description = "Identificador do carro")
	private Long id;

	@Schema(description = "Veículo envolvido no sinistro")
	private CarView car;

	@Schema(description = "Motorista envolvido no sinistro")
	private DriverView driver;

	@Schema(description = "Data do evento do sinistro")
	private LocalDate eventDate;

}