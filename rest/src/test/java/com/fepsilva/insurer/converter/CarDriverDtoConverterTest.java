package com.fepsilva.insurer.converter;

import com.fepsilva.insurer.dto.form.CarDriverForm;
import com.fepsilva.insurer.dto.view.CarDriverView;
import com.fepsilva.insurer.model.Car;
import com.fepsilva.insurer.model.CarDriver;
import com.fepsilva.insurer.model.Driver;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

@ExtendWith(MockitoExtension.class)
public class CarDriverDtoConverterTest {

	@Test
	@DisplayName("Method dtoToEntity() - Should execute successful")
	public void dtoToEntitySuccessful() {
		// Given
		CarDriverForm carDriverForm = createNewCarDriverForm(1L, 1L, true);

		// Then
		CarDriver carDriver = CarDriverDtoConverter.dtoToEntity(carDriverForm);
		isEquals(carDriver, carDriverForm);
	}

	@Test
	@DisplayName("Method dtoToEntity() - Without value")
	public void dtoToEntityWithoutValue() {
		// Given
		CarDriverForm carDriverForm = null;

		// Then
		CarDriver carDriver = CarDriverDtoConverter.dtoToEntity(carDriverForm);
		Assertions.assertNull(carDriver);
	}

	@Test
	@DisplayName("Method entityToDto() - Should execute successful")
	public void entityToDtoSuccessful() {
		// Given
		Car car = CarDtoConverterTest.createNewCar(1L, "teste", "teste", "2023", 0F);
		Driver driver = DriverDtoConverterTest.createNewDriver(1L, "teste", LocalDate.now());
		CarDriver carDriver = createNewCarDriver(1L, car, driver, true);

		// Then
		CarDriverView carDriverView = CarDriverDtoConverter.entityToDto(carDriver);
		isEquals(carDriverView, carDriver);
	}

	@Test
	@DisplayName("Method entityToModel() - Without value")
	public void entityToModelWithoutValue() {
		// Given
		CarDriver carDriver = null;

		// Then
		CarDriverView carDriverView = CarDriverDtoConverter.entityToDto(carDriver);
		Assertions.assertNull(carDriverView);
	}

	public static CarDriverForm createNewCarDriverForm(Long carId, Long driverId, Boolean mainDriver) {
		return CarDriverForm.builder()
				.carId(carId)
				.driverId(driverId)
				.mainDriver(mainDriver)
				.build();
	}

	public static CarDriver createNewCarDriver(Long id, Car car, Driver driver, Boolean mainDriver) {
		return CarDriver.builder()
				.id(id)
				.car(car)
				.driver(driver)
				.mainDriver(mainDriver)
				.build();
	}

	public static void isEquals(CarDriver carDriver, CarDriverForm carDriverForm) {
		Assertions.assertNotNull(carDriver);
		Assertions.assertNotNull(carDriverForm);

		Assertions.assertEquals(carDriver.getMainDriver(), carDriverForm.getMainDriver());
		Assertions.assertEquals(carDriver.getCar().getId(), carDriverForm.getCarId());
		Assertions.assertEquals(carDriver.getDriver().getId(), carDriverForm.getDriverId());
	}

	public static void isEquals(CarDriverView carDriverView, CarDriver carDriver) {
		Assertions.assertNotNull(carDriverView);
		Assertions.assertNotNull(carDriver);

		Assertions.assertEquals(carDriverView.getId(), carDriver.getId());
		Assertions.assertEquals(carDriverView.getMainDriver(), carDriver.getMainDriver());

		CarDtoConverterTest.isEquals(carDriverView.getCar(), carDriver.getCar());
		DriverDtoConverterTest.isEquals(carDriverView.getDriver(), carDriver.getDriver());
	}

}