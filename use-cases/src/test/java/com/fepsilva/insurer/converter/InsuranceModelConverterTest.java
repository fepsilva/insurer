package com.fepsilva.insurer.converter;

import com.fepsilva.insurer.entity.CarEntity;
import com.fepsilva.insurer.entity.CustomerEntity;
import com.fepsilva.insurer.entity.DriverEntity;
import com.fepsilva.insurer.entity.InsuranceEntity;
import com.fepsilva.insurer.model.Car;
import com.fepsilva.insurer.model.Customer;
import com.fepsilva.insurer.model.Driver;
import com.fepsilva.insurer.model.Insurance;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalDateTime;

@ExtendWith(MockitoExtension.class)
public class InsuranceModelConverterTest {

	@Test
	@DisplayName("Method modelToEntity() - Should execute successful")
	public void modelToEntitySuccessful() {
		// Given
		Car car = CarModelConverterTest.createNewCar(1L, "teste", "teste", "2023", 0F);
		Driver driver = DriverModelConverterTest.createNewDriver(1L, "teste", LocalDate.now());
		Customer customer = CustomerModelConverterTest.createNewCustomer(1L, "teste", driver);
		Insurance insurance = createNewInsurance(1L, customer, LocalDateTime.now(), LocalDateTime.now(), car, true, 0F, 0F);

		// Then
		InsuranceEntity insuranceEntity = InsuranceModelConverter.modelToEntity(insurance);
		isEquals(insuranceEntity, insurance);
	}

	@Test
	@DisplayName("Method modelToEntity() - Without value")
	public void modelToEntityWithoutValue() {
		// Given
		Insurance insurance = null;

		// Then
		InsuranceEntity insuranceEntity = InsuranceModelConverter.modelToEntity(insurance);
		Assertions.assertNull(insuranceEntity);
	}

	@Test
	@DisplayName("Method entityToModel() - Should execute successful")
	public void entityToModelSuccessful() {
		// Given
		CarEntity car = CarModelConverterTest.createNewCarEntity(1L, "teste", "teste", "2023", 0F);
		DriverEntity driver = DriverModelConverterTest.createNewDriverEntity(1L, "teste", LocalDate.now());
		CustomerEntity customer = CustomerModelConverterTest.createNewCustomerEntity(1L, "teste", driver);
		InsuranceEntity insuranceEntity = createNewInsuranceEntity(1L, customer, LocalDateTime.now(), LocalDateTime.now(), car, true, 0F, 0F);

		// Then
		Insurance insurance = InsuranceModelConverter.entityToModel(insuranceEntity);
		isEquals(insuranceEntity, insurance);
	}

	@Test
	@DisplayName("Method entityToModel() - Without value")
	public void entityToModelWithoutValue() {
		// Given
		InsuranceEntity insuranceEntity = null;

		// Then
		Insurance insurance = InsuranceModelConverter.entityToModel(insuranceEntity);
		Assertions.assertNull(insurance);
	}

	public static InsuranceEntity createNewInsuranceEntity(Long id, CustomerEntity customer, LocalDateTime creationDt, LocalDateTime updatedDt, CarEntity car, Boolean active, Float percentRisk, Float fipeValue) {
		return InsuranceEntity.builder()
				.id(id)
				.customer(customer)
				.creationDt(creationDt)
				.updatedDt(updatedDt)
				.car(car)
				.active(active)
				.percentRisk(percentRisk)
				.fipeValue(fipeValue)
				.build();
	}

	public static Insurance createNewInsurance(Long id, Customer customer, LocalDateTime creationDt, LocalDateTime updatedDt, Car car, Boolean active, Float percentRisk, Float fipeValue) {
		return Insurance.builder()
				.id(id)
				.customer(customer)
				.creationDt(creationDt)
				.updatedDt(updatedDt)
				.car(car)
				.active(active)
				.percentRisk(percentRisk)
				.fipeValue(fipeValue)
				.build();
	}

	public static void isEquals(InsuranceEntity insuranceEntity, Insurance insurance) {
		Assertions.assertNotNull(insuranceEntity);
		Assertions.assertNotNull(insurance);

		Assertions.assertEquals(insuranceEntity.getId(), insurance.getId());
		Assertions.assertEquals(insuranceEntity.getCreationDt(), insurance.getCreationDt());
		Assertions.assertEquals(insuranceEntity.getUpdatedDt(), insurance.getUpdatedDt());
		Assertions.assertEquals(insuranceEntity.getActive(), insurance.getActive());
		Assertions.assertEquals(insuranceEntity.getPercentRisk(), insurance.getPercentRisk());
		Assertions.assertEquals(insuranceEntity.getFipeValue(), insurance.getFipeValue());

		CustomerModelConverterTest.isEquals(insuranceEntity.getCustomer(), insurance.getCustomer());
		CarModelConverterTest.isEquals(insuranceEntity.getCar(), insurance.getCar());
	}

}