package com.fepsilva.insurer.dao;

import com.fepsilva.insurer.entity.ClaimEntity;

import java.util.List;
import java.util.Optional;

public interface ClaimDao {

	public ClaimEntity persist(ClaimEntity claimEntity);
	public List<ClaimEntity> findAll();
	public Optional<ClaimEntity> find(Long claimId);
	public ClaimEntity update(ClaimEntity claimEntity);
	public void delete(Long claimId);

}