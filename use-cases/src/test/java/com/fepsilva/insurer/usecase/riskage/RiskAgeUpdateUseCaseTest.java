package com.fepsilva.insurer.usecase.riskage;

import com.fepsilva.insurer.converter.RiskAgeModelConverter;
import com.fepsilva.insurer.converter.RiskAgeModelConverterTest;
import com.fepsilva.insurer.dao.RiskAgeDao;
import com.fepsilva.insurer.entity.RiskAgeEntity;
import com.fepsilva.insurer.model.RiskAge;
import com.fepsilva.insurer.usecase.riskage.impl.RiskAgeUpdateUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

@ExtendWith(MockitoExtension.class)
public class RiskAgeUpdateUseCaseTest {

	@Mock
	private RiskAgeDao riskAgeDao;

	@InjectMocks
	private RiskAgeUpdateUseCaseImpl riskAgeUpdateUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void executeSuccessful() {
		// Given
		Long riskAgeId = 1L;
		RiskAge riskAge = RiskAgeModelConverterTest.createNewRiskAge(riskAgeId, 18L, 25L, BigDecimal.ZERO);
		RiskAgeEntity riskAgeEntity = RiskAgeModelConverter.modelToEntity(riskAge);

		// When
		Mockito.when(this.riskAgeDao.update(RiskAgeModelConverter.modelToEntity(riskAge)))
				.thenReturn(riskAgeEntity);

		// Then
		riskAge = this.riskAgeUpdateUseCase.execute(riskAgeId, riskAge);
		Assertions.assertNotNull(riskAge);
	}

	@Test
	@DisplayName("Method execute() - Not Found")
	public void executeNotFound() {
		// Given
		Long riskAgeId = 1L;
		RiskAge riskAge = RiskAgeModelConverterTest.createNewRiskAge(riskAgeId, 18L, 25L, BigDecimal.ZERO);

		// When
		Mockito.when(this.riskAgeDao.update(RiskAgeModelConverter.modelToEntity(riskAge)))
				.thenReturn(null);

		// Then
		riskAge = this.riskAgeUpdateUseCase.execute(riskAgeId, riskAge);
		Assertions.assertNull(riskAge);
	}

}