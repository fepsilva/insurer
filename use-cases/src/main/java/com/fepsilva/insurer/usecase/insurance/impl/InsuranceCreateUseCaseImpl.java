package com.fepsilva.insurer.usecase.insurance.impl;

import com.fepsilva.insurer.converter.InsuranceModelConverter;
import com.fepsilva.insurer.model.Insurance;
import com.fepsilva.insurer.dao.InsuranceDao;
import com.fepsilva.insurer.usecase.car.CarFindUseCase;
import com.fepsilva.insurer.usecase.insurance.InsuranceBudgetCalculationUseCase;
import com.fepsilva.insurer.usecase.insurance.InsuranceCreateUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class InsuranceCreateUseCaseImpl implements InsuranceCreateUseCase {

	@Autowired
	private InsuranceDao insuranceDao;

	@Autowired
	private InsuranceBudgetCalculationUseCase insuranceBudgetCalculationUseCase;

	@Autowired
	private CarFindUseCase carFindUseCase;

	public Insurance execute(Insurance insurance) {
		insurance.setPercentRisk(this.insuranceBudgetCalculationUseCase.execute(insurance));
		insurance.setCar(this.carFindUseCase.execute(insurance.getCar().getId()));
		insurance.setFipeValue(insurance.getCar().getFipeValue());
		insurance.setCreationDt(LocalDateTime.now());
		insurance.setUpdatedDt(LocalDateTime.now());

		return Optional.ofNullable(this.insuranceDao.persist(InsuranceModelConverter.modelToEntity(insurance)))
				.map(InsuranceModelConverter::entityToModel)
				.orElseGet(() -> null);
	}

}