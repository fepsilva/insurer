package com.fepsilva.insurer.converter;

import com.fepsilva.insurer.entity.InsuranceEntity;
import com.fepsilva.insurer.model.Insurance;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

public class InsuranceModelConverter {

	public static InsuranceEntity modelToEntity(Insurance insurance) {
		if (insurance == null) {
			return null;
		}

		InsuranceEntity insuranceEntity = getModelMapper().map(insurance, InsuranceEntity.class);
		insuranceEntity.setCustomer(CustomerModelConverter.modelToEntity(insurance.getCustomer()));
		insuranceEntity.setCar(CarModelConverter.modelToEntity(insurance.getCar()));

		return insuranceEntity;
	}

	public static Insurance entityToModel(InsuranceEntity insuranceEntity) {
		if (insuranceEntity == null) {
			return null;
		}

		Insurance insurance = getModelMapper().map(insuranceEntity, Insurance.class);
		insurance.setCustomer(CustomerModelConverter.entityToModel(insuranceEntity.getCustomer()));
		insurance.setCar(CarModelConverter.entityToModel(insuranceEntity.getCar()));

		return insurance;
	}

	private static ModelMapper getModelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

		return modelMapper;
	}

}