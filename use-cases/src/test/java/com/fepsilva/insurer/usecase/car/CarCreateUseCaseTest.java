package com.fepsilva.insurer.usecase.car;

import com.fepsilva.insurer.converter.CarModelConverter;
import com.fepsilva.insurer.converter.CarModelConverterTest;
import com.fepsilva.insurer.dao.CarDao;
import com.fepsilva.insurer.entity.CarEntity;
import com.fepsilva.insurer.model.Car;
import com.fepsilva.insurer.usecase.car.impl.CarCreateUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CarCreateUseCaseTest {

	@Mock
	private CarDao carDao;

	@InjectMocks
	private CarCreateUseCaseImpl carCreateUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void executeSuccessful() {
		// Given
		Car car = CarModelConverterTest.createNewCar(null, "teste", "teste", "2023", 0F);
		CarEntity carEntity = CarModelConverter.modelToEntity(car);
		carEntity.setId(1L);

		// When
		Mockito.when(this.carDao.persist(CarModelConverter.modelToEntity(car)))
				.thenReturn(carEntity);

		// Then
		car = this.carCreateUseCase.execute(car);
		Assertions.assertNotNull(car);
		Assertions.assertNotNull(car.getId());
	}

	@Test
	@DisplayName("Method execute() - Should return null")
	public void executeNull() {
		// Given
		Car car = CarModelConverterTest.createNewCar(null, "teste", "teste", "2023", 0F);

		// When
		Mockito.when(this.carDao.persist(CarModelConverter.modelToEntity(car)))
				.thenReturn(null);

		// Then
		car = this.carCreateUseCase.execute(car);
		Assertions.assertNull(car);
	}

}