package com.fepsilva.insurer.controller;

import com.fepsilva.insurer.converter.DriverDtoConverter;
import com.fepsilva.insurer.dto.form.DriverForm;
import com.fepsilva.insurer.dto.view.DriverView;
import com.fepsilva.insurer.usecase.driver.DriverCreateUseCase;
import com.fepsilva.insurer.usecase.driver.DriverDeleteUseCase;
import com.fepsilva.insurer.usecase.driver.DriverFindAllUseCase;
import com.fepsilva.insurer.usecase.driver.DriverFindUseCase;
import com.fepsilva.insurer.usecase.driver.DriverUpdateUseCase;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Validated
@RestController
@RequestMapping("/driver")
@Tag(name = "API de Motorista", description = "Endpoints responsáveis pela gestão do cadastro de motoristas")
public class DriverController {

	@Autowired
	private DriverCreateUseCase driverCreateUseCase;

	@Autowired
	private DriverFindAllUseCase driverFindAllUseCase;

	@Autowired
	private DriverFindUseCase driverFindUseCase;

	@Autowired
	private DriverUpdateUseCase driverUpdateUseCase;

	@Autowired
	private DriverDeleteUseCase driverDeleteUseCase;

	@Operation(summary = "Endpoint para criar um novo motorista")
	@ApiResponse(responseCode = "200", description = "Motorista cadastrado")
	@PostMapping(consumes = "application/json", produces = "application/json")
	public ResponseEntity<DriverView> create(@Valid @RequestBody DriverForm driverForm) {
		try {
			return Optional.ofNullable(this.driverCreateUseCase.execute(DriverDtoConverter.dtoToEntity(driverForm)))
					.map(DriverDtoConverter::entityToDto)
					.map(ResponseEntity::ok)
					.get();
		} catch (DataIntegrityViolationException e) {
			log.error("Dados inválidos", e);
			return ResponseEntity.badRequest().build();
		} catch (Exception e) {
			log.error("Erro ao cadastrar motorista", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@Operation(summary = "Endpoint para consultar todos os motoristas")
	@ApiResponse(responseCode = "200", description = "Motoristas consultados")
	@GetMapping
	public ResponseEntity<List<DriverView>> findAll() {
		try {
			return ResponseEntity.ok(this.driverFindAllUseCase.execute().stream()
					.map(DriverDtoConverter::entityToDto)
					.collect(Collectors.toList()));
		} catch (Exception e) {
			log.error("Erro ao procurar motorista", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@Operation(summary = "Endpoint para consultar um motorista")
	@ApiResponse(responseCode = "200", description = "Motorista consultado")
	@ApiResponse(responseCode = "204", description = "Motorista não encontrado")
	@GetMapping("/{driverId}")
	public ResponseEntity<DriverView> find(@Parameter(description = "Id do Motorista") @PathVariable Long driverId) {
		try {
			return Optional.ofNullable(this.driverFindUseCase.execute(driverId))
					.map(DriverDtoConverter::entityToDto)
					.map(ResponseEntity::ok)
					.orElseGet(() -> ResponseEntity.noContent().build());
		} catch (Exception e) {
			log.error("Erro ao procurar motorista", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@Operation(summary = "Endpoint para alterar um motorista")
	@ApiResponse(responseCode = "200", description = "Motorista alterado")
	@ApiResponse(responseCode = "204", description = "Motorista não encontrado")
	@PutMapping("/{driverId}")
	public ResponseEntity<DriverView> update(
			@Parameter(description = "Id do Motorista") @PathVariable Long driverId,
			@Valid @RequestBody DriverForm driverForm) {
		try {
			return Optional.ofNullable(this.driverUpdateUseCase.execute(driverId, DriverDtoConverter.dtoToEntity(driverForm)))
					.map(DriverDtoConverter::entityToDto)
					.map(ResponseEntity::ok)
					.orElseGet(() -> ResponseEntity.noContent().build());
		} catch (DataIntegrityViolationException e) {
			log.error("Dados inválidos", e);
			return ResponseEntity.badRequest().build();
		} catch (Exception e) {
			log.error("Erro ao alterar motorista", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@Operation(summary = "Endpoint para remover um motorista")
	@ApiResponse(responseCode = "200", description = "Motorista removido")
	@ApiResponse(responseCode = "204", description = "Motorista não encontrado")
	@DeleteMapping("/{driverId}")
	public ResponseEntity delete(@Parameter(description = "Id do Motorista") @PathVariable Long driverId) {
		try {
			this.driverDeleteUseCase.execute(driverId);
			return ResponseEntity.ok().build();
		} catch (EmptyResultDataAccessException e) {
			log.info("Motorista não encontrado");
			return ResponseEntity.noContent().build();
		} catch (Exception e) {
			log.error("Erro ao remover motorista", e);
			return ResponseEntity.internalServerError().build();
		}
	}

}