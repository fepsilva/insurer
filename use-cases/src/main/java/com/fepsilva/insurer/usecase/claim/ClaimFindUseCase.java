package com.fepsilva.insurer.usecase.claim;

import com.fepsilva.insurer.model.Claim;

public interface ClaimFindUseCase {

	public Claim execute(Long claimId);

}