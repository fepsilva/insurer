package com.fepsilva.insurer.controller;

import com.fepsilva.insurer.converter.ClaimDtoConverter;
import com.fepsilva.insurer.dto.form.ClaimForm;
import com.fepsilva.insurer.dto.view.ClaimView;
import com.fepsilva.insurer.usecase.claim.ClaimCreateUseCase;
import com.fepsilva.insurer.usecase.claim.ClaimDeleteUseCase;
import com.fepsilva.insurer.usecase.claim.ClaimFindAllUseCase;
import com.fepsilva.insurer.usecase.claim.ClaimFindUseCase;
import com.fepsilva.insurer.usecase.claim.ClaimUpdateUseCase;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Validated
@RestController
@RequestMapping("/claim")
@Tag(name = "API de Sinistro", description = "Endpoints responsáveis pela gestão do cadastro de sinistros")
public class ClaimController {

	@Autowired
	private ClaimCreateUseCase claimCreateUseCase;

	@Autowired
	private ClaimFindAllUseCase claimFindAllUseCase;

	@Autowired
	private ClaimFindUseCase claimFindUseCase;

	@Autowired
	private ClaimUpdateUseCase claimUpdateUseCase;

	@Autowired
	private ClaimDeleteUseCase claimDeleteUseCase;

	@Operation(summary = "Endpoint para criar um novo sinistro")
	@ApiResponse(responseCode = "200", description = "Sinistro cadastrado")
	@PostMapping(consumes = "application/json", produces = "application/json")
	public ResponseEntity<ClaimView> create(@Valid @RequestBody ClaimForm claimForm) {
		try {
			return Optional.ofNullable(this.claimCreateUseCase.execute(ClaimDtoConverter.dtoToEntity(claimForm)))
					.map(ClaimDtoConverter::entityToDto)
					.map(ResponseEntity::ok)
					.get();
		} catch (DataIntegrityViolationException e) {
			log.error("Dados inválidos", e);
			return ResponseEntity.badRequest().build();
		} catch (Exception e) {
			log.error("Erro ao cadastrar sinistro", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@Operation(summary = "Endpoint para consultar todos os sinistros")
	@ApiResponse(responseCode = "200", description = "Sinistros consultados")
	@GetMapping
	public ResponseEntity<List<ClaimView>> findAll() {
		try {
			return ResponseEntity.ok(this.claimFindAllUseCase.execute().stream()
					.map(ClaimDtoConverter::entityToDto)
					.collect(Collectors.toList()));
		} catch (Exception e) {
			log.error("Erro ao procurar sinistro", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@Operation(summary = "Endpoint para consultar um sinistro")
	@ApiResponse(responseCode = "200", description = "Sinistro consultado")
	@ApiResponse(responseCode = "204", description = "Sinistro não encontrado")
	@GetMapping("/{claimId}")
	public ResponseEntity<ClaimView> find(@Parameter(description = "Id do Sinistro") @PathVariable Long claimId) {
		try {
			return Optional.ofNullable(this.claimFindUseCase.execute(claimId))
					.map(ClaimDtoConverter::entityToDto)
					.map(ResponseEntity::ok)
					.orElseGet(() -> ResponseEntity.noContent().build());
		} catch (Exception e) {
			log.error("Erro ao procurar sinistro", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@Operation(summary = "Endpoint para alterar um sinistro")
	@ApiResponse(responseCode = "200", description = "Sinistro alterado")
	@ApiResponse(responseCode = "204", description = "Sinistro não encontrado")
	@PutMapping("/{claimId}")
	public ResponseEntity<ClaimView> update(
			@Parameter(description = "Id do Sinistro") @PathVariable Long claimId,
			@Valid @RequestBody ClaimForm claimForm) {
		try {
			return Optional.ofNullable(this.claimUpdateUseCase.execute(claimId, ClaimDtoConverter.dtoToEntity(claimForm)))
					.map(ClaimDtoConverter::entityToDto)
					.map(ResponseEntity::ok)
					.orElseGet(() -> ResponseEntity.noContent().build());
		} catch (DataIntegrityViolationException e) {
			log.error("Dados inválidos", e);
			return ResponseEntity.badRequest().build();
		} catch (Exception e) {
			log.error("Erro ao alterar sinistro", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@Operation(summary = "Endpoint para remover um sinistro")
	@ApiResponse(responseCode = "200", description = "Sinistro removido")
	@ApiResponse(responseCode = "204", description = "Sinistro não encontrado")
	@DeleteMapping("/{claimId}")
	public ResponseEntity delete(@Parameter(description = "Id do Sinistro") @PathVariable Long claimId) {
		try {
			this.claimDeleteUseCase.execute(claimId);
			return ResponseEntity.ok().build();
		} catch (EmptyResultDataAccessException e) {
			log.info("Sinistro não encontrado");
			return ResponseEntity.noContent().build();
		} catch (Exception e) {
			log.error("Erro ao remover sinistro", e);
			return ResponseEntity.internalServerError().build();
		}
	}

}