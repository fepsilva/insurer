package com.fepsilva.insurer.usecase.insurance;

import com.fepsilva.insurer.model.Insurance;

public interface InsuranceUpdateUseCase {

	public Insurance execute(Long insuranceId, Insurance insurance);

}