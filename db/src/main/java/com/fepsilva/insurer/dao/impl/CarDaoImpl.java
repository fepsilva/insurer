package com.fepsilva.insurer.dao.impl;

import com.fepsilva.insurer.dao.CarDao;
import com.fepsilva.insurer.entity.CarEntity;
import com.fepsilva.insurer.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CarDaoImpl implements CarDao {

	@Autowired
	private CarRepository carRepository;

	public CarEntity persist(CarEntity carEntity) {
		return this.carRepository.save(carEntity);
	}

	public List<CarEntity> findAll() {
		return this.carRepository.findAll();
	}

	public Optional<CarEntity> find(Long carId) {
		return this.carRepository.findById(carId);
	}

	public CarEntity update(CarEntity carEntity) {
		if (this.carRepository.existsById(carEntity.getId())) {
			return this.carRepository.save(carEntity);
		}

		return null;
	}

	public void delete(Long carId) {
		this.carRepository.deleteById(carId);
	}

}