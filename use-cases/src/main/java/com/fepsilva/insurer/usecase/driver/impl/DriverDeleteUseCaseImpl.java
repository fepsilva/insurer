package com.fepsilva.insurer.usecase.driver.impl;

import com.fepsilva.insurer.dao.DriverDao;
import com.fepsilva.insurer.usecase.driver.DriverDeleteUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DriverDeleteUseCaseImpl implements DriverDeleteUseCase {

	@Autowired
	private DriverDao driverDao;

	public void execute(Long driverId) {
		this.driverDao.delete(driverId);
	}

}