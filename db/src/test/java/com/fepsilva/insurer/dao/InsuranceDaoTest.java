package com.fepsilva.insurer.dao;

import com.fepsilva.insurer.dao.impl.CarDaoImpl;
import com.fepsilva.insurer.dao.impl.CustomerDaoImpl;
import com.fepsilva.insurer.dao.impl.InsuranceDaoImpl;
import com.fepsilva.insurer.entity.CarEntity;
import com.fepsilva.insurer.entity.CustomerEntity;
import com.fepsilva.insurer.entity.InsuranceEntity;
import com.fepsilva.insurer.repository.InsuranceRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class InsuranceDaoTest {

	@Mock
	private CustomerDaoImpl customerDaoImpl;

	@Mock
	private CarDaoImpl carDao;

	@Mock
	private InsuranceRepository insuranceRepository;

	@InjectMocks
	private InsuranceDaoImpl insuranceDao;

	@Test
	@DisplayName("Method persist() - Should execute successful")
	public void persistSuccessful() {
		// Given
		Long id = 1L;
		CustomerEntity customer = CustomerDaoTest.createNewCustomerEntity(1L, "teste1", null);
		LocalDateTime creationDt = LocalDateTime.now();
		LocalDateTime updatedDt = LocalDateTime.now();
		CarEntity car = CarDaoTest.createNewCarEntity(1L, "model", "manufacturer", "2023", 0F);
		Boolean active = true;
		Float percentRisk = 0F;
		Float fipeValue = 0F;

		InsuranceEntity insuranceEntity = createNewInsuranceEntity(null, customer, creationDt, updatedDt, car, active, percentRisk, fipeValue);
		InsuranceEntity insuranceEntityNew = createNewInsuranceEntity(id, customer, creationDt, updatedDt, car, active, percentRisk, fipeValue);

		// When
		Mockito.when(this.customerDaoImpl.find(customer.getId()))
				.thenReturn(Optional.of(customer));
		Mockito.when(this.carDao.find(car.getId()))
				.thenReturn(Optional.of(car));
		Mockito.when(this.insuranceRepository.save(insuranceEntity))
				.thenReturn(insuranceEntityNew);

		// Then
		insuranceEntity = insuranceDao.persist(insuranceEntity);
		Assertions.assertNotNull(insuranceEntity.getId());
		Assertions.assertNotNull(insuranceEntity.getCustomer().getName());
		Assertions.assertNotNull(insuranceEntity.getCar().getModel());
	}

	@Test
	@DisplayName("Method persist() - Without complement entity")
	public void persistWithoutComplement() {
		// Given
		Long id = 1L;
		CustomerEntity customer = CustomerDaoTest.createNewCustomerEntity(1L, null, null);
		LocalDateTime creationDt = LocalDateTime.now();
		LocalDateTime updatedDt = LocalDateTime.now();
		CarEntity car = CarDaoTest.createNewCarEntity(1L, null, null, null, null);
		Boolean active = true;
		Float percentRisk = 0F;
		Float fipeValue = 0F;

		InsuranceEntity insuranceEntity = createNewInsuranceEntity(null, customer, creationDt, updatedDt, car, active, percentRisk, fipeValue);
		InsuranceEntity insuranceEntityNew = createNewInsuranceEntity(id, customer, creationDt, updatedDt, car, active, percentRisk, fipeValue);

		// When
		Mockito.when(this.customerDaoImpl.find(customer.getId()))
				.thenReturn(Optional.ofNullable(null));
		Mockito.when(this.carDao.find(car.getId()))
				.thenReturn(Optional.ofNullable(null));
		Mockito.when(this.insuranceRepository.save(insuranceEntity))
				.thenReturn(insuranceEntityNew);

		// Then
		insuranceEntity = insuranceDao.persist(insuranceEntity);
		Assertions.assertNotNull(insuranceEntity.getId());
		Assertions.assertNull(insuranceEntity.getCustomer().getName());
		Assertions.assertNull(insuranceEntity.getCar().getModel());
	}

	@Test
	@DisplayName("Method findAll() - Should execute successful")
	public void findAllSuccessful() {
		// Given
		CustomerEntity customer = CustomerDaoTest.createNewCustomerEntity(1L, "teste1", null);
		CarEntity car = CarDaoTest.createNewCarEntity(1L, "model", "manufacturer", "2023", 0F);

		List<InsuranceEntity> listInsuranceEntity = List.of(
				createNewInsuranceEntity(1L, customer, LocalDateTime.now(), LocalDateTime.now(), car, true, 0F, 0F),
				createNewInsuranceEntity(2L, customer, LocalDateTime.now(), LocalDateTime.now(), car, true, 0F, 0F),
				createNewInsuranceEntity(3L, customer, LocalDateTime.now(), LocalDateTime.now(), car, true, 0F, 0F));

		// When
		Mockito.when(this.insuranceRepository.findAll())
				.thenReturn(listInsuranceEntity);

		// Then
		listInsuranceEntity = insuranceDao.findAll();
		Assertions.assertNotNull(listInsuranceEntity);
		Assertions.assertEquals(listInsuranceEntity.size(), 3);
	}

	@Test
	@DisplayName("Method find() - Should execute successful")
	public void findSuccessful() {
		// Given
		Long id = 1L;
		CustomerEntity customer = CustomerDaoTest.createNewCustomerEntity(1L, "teste1", null);
		CarEntity car = CarDaoTest.createNewCarEntity(1L, "model", "manufacturer", "2023", 0F);
		InsuranceEntity insuranceEntity = createNewInsuranceEntity(id, customer, LocalDateTime.now(), LocalDateTime.now(), car, true, 0F, 0F);

		// When
		Mockito.when(this.insuranceRepository.findById(id))
				.thenReturn(Optional.of(insuranceEntity));

		// Then
		Optional<InsuranceEntity> optionalInsuranceEntity = insuranceDao.find(id);
		Assertions.assertNotNull(optionalInsuranceEntity.get());
		Assertions.assertEquals(optionalInsuranceEntity.get().getId(), id);
	}

	@Test
	@DisplayName("Method update() - Should execute successful")
	public void updateSuccessful() {
		// Given
		CustomerEntity customer = CustomerDaoTest.createNewCustomerEntity(1L, "teste1", null);
		CarEntity car = CarDaoTest.createNewCarEntity(1L, "model", "manufacturer", "2023", 0F);
		InsuranceEntity insuranceEntity = createNewInsuranceEntity(1L, customer, LocalDateTime.now(), LocalDateTime.now(), car, true, 0F, 0F);

		// When
		Mockito.when(this.insuranceRepository.existsById(insuranceEntity.getId()))
				.thenReturn(true);
		Mockito.when(this.customerDaoImpl.find(customer.getId()))
				.thenReturn(Optional.of(customer));
		Mockito.when(this.carDao.find(car.getId()))
				.thenReturn(Optional.of(car));
		Mockito.when(this.insuranceRepository.save(insuranceEntity))
				.thenReturn(insuranceEntity);

		// Then
		insuranceEntity = insuranceDao.update(insuranceEntity);
		Assertions.assertNotNull(insuranceEntity.getId());
		Assertions.assertNotNull(insuranceEntity.getCustomer().getName());
		Assertions.assertNotNull(insuranceEntity.getCar().getModel());
	}

	@Test
	@DisplayName("Method update() - Without complement entity")
	public void updateWithoutComplement() {
		// Given
		CustomerEntity customer = CustomerDaoTest.createNewCustomerEntity(1L, null, null);
		CarEntity car = CarDaoTest.createNewCarEntity(1L, null, null, null, null);
		InsuranceEntity insuranceEntity = createNewInsuranceEntity(1L, customer, LocalDateTime.now(), LocalDateTime.now(), car, true, 0F, 0F);

		// When
		Mockito.when(this.insuranceRepository.existsById(insuranceEntity.getId()))
				.thenReturn(true);
		Mockito.when(this.customerDaoImpl.find(customer.getId()))
				.thenReturn(Optional.ofNullable(null));
		Mockito.when(this.carDao.find(car.getId()))
				.thenReturn(Optional.ofNullable(null));
		Mockito.when(this.insuranceRepository.save(insuranceEntity))
				.thenReturn(insuranceEntity);

		// Then
		insuranceEntity = insuranceDao.update(insuranceEntity);
		Assertions.assertNotNull(insuranceEntity.getId());
		Assertions.assertNull(insuranceEntity.getCustomer().getName());
		Assertions.assertNull(insuranceEntity.getCar().getModel());
	}

	@Test
	@DisplayName("Method update() - Should not found")
	public void updateNotFound() {
		// Given
		CustomerEntity customer = CustomerDaoTest.createNewCustomerEntity(1L, "teste1", null);
		CarEntity car = CarDaoTest.createNewCarEntity(1L, "model", "manufacturer", "2023", 0F);
		InsuranceEntity insuranceEntity = createNewInsuranceEntity(1L, customer, LocalDateTime.now(), LocalDateTime.now(), car, true, 0F, 0F);

		// When
		Mockito.when(this.insuranceRepository.existsById(insuranceEntity.getId()))
				.thenReturn(false);

		// Then
		insuranceEntity = insuranceDao.update(insuranceEntity);
		Assertions.assertNull(insuranceEntity);
	}

	@Test
	@DisplayName("Method delete() - Should execute successful")
	public void deleteSuccessful() {
		// Given
		Long id = 1L;

		// Then
		Assertions.assertDoesNotThrow(() -> carDao.delete(id));
	}

	public static InsuranceEntity createNewInsuranceEntity(Long id, CustomerEntity customer, LocalDateTime creationDt, LocalDateTime updatedDt, CarEntity car, Boolean active, Float percentRisk, Float fipeValue) {
		return InsuranceEntity.builder()
				.id(id)
				.customer(customer)
				.creationDt(creationDt)
				.updatedDt(updatedDt)
				.car(car)
				.active(active)
				.percentRisk(percentRisk)
				.fipeValue(fipeValue)
				.build();
	}

}