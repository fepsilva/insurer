package com.fepsilva.insurer.usecase.insurance;

import com.fepsilva.insurer.converter.CarModelConverterTest;
import com.fepsilva.insurer.converter.CustomerModelConverterTest;
import com.fepsilva.insurer.converter.DriverModelConverterTest;
import com.fepsilva.insurer.converter.InsuranceModelConverterTest;
import com.fepsilva.insurer.dao.InsuranceDao;
import com.fepsilva.insurer.entity.CarEntity;
import com.fepsilva.insurer.entity.CustomerEntity;
import com.fepsilva.insurer.entity.DriverEntity;
import com.fepsilva.insurer.entity.InsuranceEntity;
import com.fepsilva.insurer.model.Insurance;
import com.fepsilva.insurer.usecase.insurance.impl.InsuranceFindUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class InsuranceFindUseCaseTest {

	@Mock
	private InsuranceDao insuranceDao;

	@InjectMocks
	private InsuranceFindUseCaseImpl insuranceFindUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void executeSuccessful() {
		// Given
		Long insuranceId = 1L;
		CarEntity car = CarModelConverterTest.createNewCarEntity(1L, "teste", "teste", "2023", 0F);
		DriverEntity driver = DriverModelConverterTest.createNewDriverEntity(1L, "teste", LocalDate.now());
		CustomerEntity customer = CustomerModelConverterTest.createNewCustomerEntity(1L, "teste", driver);
		InsuranceEntity insuranceEntity = InsuranceModelConverterTest.createNewInsuranceEntity(1L, customer, LocalDateTime.now(), LocalDateTime.now(), car, true, 0F, 0F);

		// When
		Mockito.when(this.insuranceDao.find(insuranceId))
				.thenReturn(Optional.ofNullable(insuranceEntity));

		// Then
		Insurance insurance = this.insuranceFindUseCase.execute(insuranceId);
		Assertions.assertNotNull(insurance);

		InsuranceModelConverterTest.isEquals(insuranceEntity, insurance);
	}

	@Test
	@DisplayName("Method execute() - Not Found")
	public void executeNotFound() {
		// Given
		Long insuranceId = 1L;

		// When
		Mockito.when(this.insuranceDao.find(insuranceId))
				.thenReturn(Optional.ofNullable(null));

		// Then
		Insurance insurance = this.insuranceFindUseCase.execute(insuranceId);
		Assertions.assertNull(insurance);
	}

}