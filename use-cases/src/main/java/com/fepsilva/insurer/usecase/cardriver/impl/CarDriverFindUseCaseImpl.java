package com.fepsilva.insurer.usecase.cardriver.impl;

import com.fepsilva.insurer.converter.CarDriverModelConverter;
import com.fepsilva.insurer.dao.CarDriverDao;
import com.fepsilva.insurer.model.CarDriver;
import com.fepsilva.insurer.usecase.cardriver.CarDriverFindUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CarDriverFindUseCaseImpl implements CarDriverFindUseCase {

	@Autowired
	private CarDriverDao carDriverDao;

	public CarDriver execute(Long carDriverId) {
		return this.carDriverDao.find(carDriverId)
				.map(CarDriverModelConverter::entityToModel)
				.orElseGet(() -> null);
	}

}