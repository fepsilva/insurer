package com.fepsilva.insurer.usecase.cardriver;

import com.fepsilva.insurer.model.CarDriver;

public interface CarDriverFindUseCase {

	public CarDriver execute(Long carDriverId);

}