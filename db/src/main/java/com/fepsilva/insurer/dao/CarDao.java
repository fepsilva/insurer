package com.fepsilva.insurer.dao;

import com.fepsilva.insurer.entity.CarEntity;

import java.util.List;
import java.util.Optional;

public interface CarDao {

	public CarEntity persist(CarEntity carEntity);
	public List<CarEntity> findAll();
	public Optional<CarEntity> find(Long carId);
	public CarEntity update(CarEntity carEntity);
	public void delete(Long carId);

}