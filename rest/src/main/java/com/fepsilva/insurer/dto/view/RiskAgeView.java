package com.fepsilva.insurer.dto.view;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Visualização do cadastro de risco por idade")
public class RiskAgeView {

	@Schema(description = "Identificador do risco da idade")
	private Long id;

	@Schema(description = "Idade inicial (será considerada essa idade e para cima)")
	private Long beginAge;

	@Schema(description = "Idade inicial (será considerada essa idade e para baixo)")
	private Long endAge;

	@Schema(description = "Porcentagem cobrada baseado no risco")
	private BigDecimal percentRisk;

}