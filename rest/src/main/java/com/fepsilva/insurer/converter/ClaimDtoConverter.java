package com.fepsilva.insurer.converter;

import com.fepsilva.insurer.dto.form.ClaimForm;
import com.fepsilva.insurer.dto.view.ClaimView;
import com.fepsilva.insurer.model.Claim;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

public class ClaimDtoConverter {

	public static Claim dtoToEntity(ClaimForm claimForm) {
		if (claimForm == null) {
			return null;
		}

		Claim claim = getModelMapper().map(claimForm, Claim.class);
		claim.setCar(CarDtoConverter.idToEntity(claimForm.getCarId()));
		claim.setDriver(DriverDtoConverter.idToEntity(claimForm.getDriverId()));

		return claim;
	}

	public static ClaimView entityToDto(Claim claim) {
		if (claim == null) {
			return null;
		}

		ClaimView claimView = getModelMapper().map(claim, ClaimView.class);
		claimView.setCar(CarDtoConverter.entityToDto(claim.getCar()));
		claimView.setDriver(DriverDtoConverter.entityToDto(claim.getDriver()));

		return claimView;
	}

	private static ModelMapper getModelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

		return modelMapper;
	}

}