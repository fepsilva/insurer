package com.fepsilva.insurer.usecase.claim.impl;

import com.fepsilva.insurer.converter.ClaimModelConverter;
import com.fepsilva.insurer.dao.ClaimDao;
import com.fepsilva.insurer.model.Claim;
import com.fepsilva.insurer.usecase.claim.ClaimUpdateUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClaimUpdateUseCaseImpl implements ClaimUpdateUseCase {

	@Autowired
	private ClaimDao claimDao;

	public Claim execute(Long carId, Claim claim) {
		claim.setId(carId);

		return Optional.ofNullable(this.claimDao.update(ClaimModelConverter.modelToEntity(claim)))
				.map(ClaimModelConverter::entityToModel)
				.orElseGet(() -> null);
	}

}