package com.fepsilva.insurer.usecase.driver.impl;

import com.fepsilva.insurer.converter.DriverModelConverter;
import com.fepsilva.insurer.model.Driver;
import com.fepsilva.insurer.dao.DriverDao;
import com.fepsilva.insurer.usecase.driver.DriverCreateUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DriverCreateUseCaseImpl implements DriverCreateUseCase {

	@Autowired
	private DriverDao driverDao;

	public Driver execute(Driver driver) {
		return Optional.ofNullable(this.driverDao.persist(DriverModelConverter.modelToEntity(driver)))
				.map(DriverModelConverter::entityToModel)
				.orElseGet(() -> null);
	}

}