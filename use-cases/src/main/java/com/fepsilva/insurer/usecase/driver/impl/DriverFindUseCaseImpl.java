package com.fepsilva.insurer.usecase.driver.impl;

import com.fepsilva.insurer.converter.DriverModelConverter;
import com.fepsilva.insurer.dao.DriverDao;
import com.fepsilva.insurer.model.Driver;
import com.fepsilva.insurer.usecase.driver.DriverFindUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DriverFindUseCaseImpl implements DriverFindUseCase {

	@Autowired
	private DriverDao driverDao;

	public Driver execute(Long driverId) {
		return this.driverDao.find(driverId)
				.map(DriverModelConverter::entityToModel)
				.orElseGet(() -> null);
	}

}