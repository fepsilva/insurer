package com.fepsilva.insurer.usecase.riskage.impl;

import com.fepsilva.insurer.converter.RiskAgeModelConverter;
import com.fepsilva.insurer.dao.RiskAgeDao;
import com.fepsilva.insurer.model.RiskAge;
import com.fepsilva.insurer.usecase.riskage.RiskAgeFindUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RiskAgeFindUseCaseImpl implements RiskAgeFindUseCase {

	@Autowired
	private RiskAgeDao riskAgeDao;

	public RiskAge execute(Long riskAgeId) {
		return this.riskAgeDao.find(riskAgeId)
				.map(RiskAgeModelConverter::entityToModel)
				.orElseGet(() -> null);
	}

}