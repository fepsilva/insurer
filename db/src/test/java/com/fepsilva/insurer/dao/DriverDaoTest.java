package com.fepsilva.insurer.dao;

import com.fepsilva.insurer.dao.impl.DriverDaoImpl;
import com.fepsilva.insurer.entity.DriverEntity;
import com.fepsilva.insurer.repository.DriverRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class DriverDaoTest {

	@Mock
	private DriverRepository carRepository;

	@InjectMocks
	private DriverDaoImpl carDao;

	@Test
	@DisplayName("Method persist() - Should execute successful")
	public void persistSuccessful() {
		// Given
		Long id = 1L;
		String document = "teste";
		LocalDate birthDate = LocalDate.now();

		DriverEntity carEntity = createNewDriverEntity(null, document, birthDate);
		DriverEntity carEntityNew = createNewDriverEntity(id, document, birthDate);

		// When
		Mockito.when(this.carRepository.save(carEntity))
				.thenReturn(carEntityNew);

		// Then
		carEntity = carDao.persist(carEntity);
		Assertions.assertNotNull(carEntity.getId());
	}

	@Test
	@DisplayName("Method findAll() - Should execute successful")
	public void findAllSuccessful() {
		// Given
		List<DriverEntity> listDriverEntity = List.of(
				createNewDriverEntity(1L, "teste1", LocalDate.now()),
				createNewDriverEntity(2L, "teste2", LocalDate.now()),
				createNewDriverEntity(3L, "teste3", LocalDate.now()));

		// When
		Mockito.when(this.carRepository.findAll())
				.thenReturn(listDriverEntity);

		// Then
		listDriverEntity = carDao.findAll();
		Assertions.assertNotNull(listDriverEntity);
		Assertions.assertEquals(listDriverEntity.size(), 3);
	}

	@Test
	@DisplayName("Method find() - Should execute successful")
	public void findSuccessful() {
		// Given
		Long id = 1L;
		DriverEntity carEntity = createNewDriverEntity(id, "teste1", LocalDate.now());

		// When
		Mockito.when(this.carRepository.findById(id))
				.thenReturn(Optional.of(carEntity));

		// Then
		Optional<DriverEntity> optionalDriverEntity = carDao.find(id);
		Assertions.assertNotNull(optionalDriverEntity.get());
		Assertions.assertEquals(optionalDriverEntity.get().getId(), id);
	}

	@Test
	@DisplayName("Method update() - Should execute successful")
	public void updateSuccessful() {
		// Given
		DriverEntity carEntity = createNewDriverEntity(1L, "teste", LocalDate.now());

		// When
		Mockito.when(this.carRepository.existsById(carEntity.getId()))
				.thenReturn(true);
		Mockito.when(this.carRepository.save(carEntity))
				.thenReturn(carEntity);

		// Then
		carEntity = carDao.update(carEntity);
		Assertions.assertNotNull(carEntity.getId());
	}

	@Test
	@DisplayName("Method update() - Should not found")
	public void updateNotFound() {
		// Given
		DriverEntity carEntity = createNewDriverEntity(1L, "teste", LocalDate.now());

		// When
		Mockito.when(this.carRepository.existsById(carEntity.getId()))
				.thenReturn(false);

		// Then
		carEntity = carDao.update(carEntity);
		Assertions.assertNull(carEntity);
	}

	@Test
	@DisplayName("Method delete() - Should execute successful")
	public void deleteSuccessful() {
		// Given
		Long id = 1L;

		// Then
		Assertions.assertDoesNotThrow(() -> carDao.delete(id));
	}

	public static DriverEntity createNewDriverEntity(Long id, String document, LocalDate birthDate) {
		return DriverEntity.builder()
				.id(id)
				.document(document)
				.birthDate(birthDate)
				.build();
	}

}