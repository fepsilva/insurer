package com.fepsilva.insurer.dao.impl;

import com.fepsilva.insurer.dao.CustomerDao;
import com.fepsilva.insurer.dao.DriverDao;
import com.fepsilva.insurer.entity.CustomerEntity;
import com.fepsilva.insurer.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerDaoImpl implements CustomerDao {

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private DriverDao driverDao;

	public CustomerEntity persist(CustomerEntity customerEntity) {
		customerEntity = this.customerRepository.save(customerEntity);
		this.fillComplementEntity(customerEntity);

		return customerEntity;
	}

	public List<CustomerEntity> findAll() {
		return this.customerRepository.findAll();
	}

	public Optional<CustomerEntity> find(Long customerId) {
		return this.customerRepository.findById(customerId);
	}

	public CustomerEntity update(CustomerEntity customerEntity) {
		if (this.customerRepository.existsById(customerEntity.getId())) {
			customerEntity = this.customerRepository.save(customerEntity);
			this.fillComplementEntity(customerEntity);

			return customerEntity;
		}

		return null;
	}

	public void delete(Long customerId) {
		this.customerRepository.deleteById(customerId);
	}

	private void fillComplementEntity(CustomerEntity customerEntity) {
		this.driverDao.find(customerEntity.getDriver().getId())
				.ifPresent(driver -> customerEntity.setDriver(driver));
	}

}