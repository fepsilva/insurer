package com.fepsilva.insurer.usecase.riskage;

import com.fepsilva.insurer.model.RiskAge;

public interface RiskAgeUpdateUseCase {

	public RiskAge execute(Long riskAgeId, RiskAge riskAge);

}