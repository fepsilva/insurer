package com.fepsilva.insurer.usecase.cardriver;

import com.fepsilva.insurer.converter.CarDriverModelConverterTest;
import com.fepsilva.insurer.converter.CarModelConverterTest;
import com.fepsilva.insurer.converter.DriverModelConverterTest;
import com.fepsilva.insurer.dao.CarDriverDao;
import com.fepsilva.insurer.entity.CarDriverEntity;
import com.fepsilva.insurer.entity.CarEntity;
import com.fepsilva.insurer.entity.DriverEntity;
import com.fepsilva.insurer.model.CarDriver;
import com.fepsilva.insurer.usecase.cardriver.impl.CarDriverFindAllUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class CarDriverFindAllUseCaseTest {

	@Mock
	private CarDriverDao carDriverDao;

	@InjectMocks
	private CarDriverFindAllUseCaseImpl carDriverFindAllUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void executeSuccessful() {
		// Given
		CarEntity car = CarModelConverterTest.createNewCarEntity(1L, "teste", "teste", "2023", 0F);
		DriverEntity driver = DriverModelConverterTest.createNewDriverEntity(1L, "teste", LocalDate.now());

		List<CarDriverEntity> listCarDriverEntity = List.of(
				CarDriverModelConverterTest.createNewCarDriverEntity(1L, car, driver, true),
				CarDriverModelConverterTest.createNewCarDriverEntity(2L, car, driver, true),
				CarDriverModelConverterTest.createNewCarDriverEntity(3L, car, driver, true));

		// When
		Mockito.when(this.carDriverDao.findAll())
				.thenReturn(listCarDriverEntity);

		// Then
		List<CarDriver> listCarDriver = this.carDriverFindAllUseCase.execute();
		Assertions.assertNotNull(listCarDriver);
		Assertions.assertEquals(listCarDriver.size(), 3);

		for (int i = 0; i < listCarDriverEntity.size(); i++) {
			CarDriverModelConverterTest.isEquals(listCarDriverEntity.get(i), listCarDriver.get(i));
		}
	}

	@Test
	@DisplayName("Method execute() - Empty")
	public void executeEmpty() {
		// When
		Mockito.when(this.carDriverDao.findAll())
				.thenReturn(null);

		// Then
		List<CarDriver> listCarDriver = this.carDriverFindAllUseCase.execute();
		Assertions.assertNotNull(listCarDriver);
		Assertions.assertEquals(listCarDriver.size(), 0);
	}

}