package com.fepsilva.insurer.converter;

import com.fepsilva.insurer.dto.form.CustomerForm;
import com.fepsilva.insurer.dto.view.CustomerView;
import com.fepsilva.insurer.model.Customer;
import com.fepsilva.insurer.model.Driver;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

@ExtendWith(MockitoExtension.class)
public class CustomerDtoConverterTest {

	@Test
	@DisplayName("Method dtoToEntity() - Should execute successful")
	public void dtoToEntitySuccessful() {
		// Given
		CustomerForm customerForm = createNewCustomerForm("teste", 1L);

		// Then
		Customer customer = CustomerDtoConverter.dtoToEntity(customerForm);
		isEquals(customer, customerForm);
	}

	@Test
	@DisplayName("Method dtoToEntity() - Without value")
	public void dtoToEntityWithoutValue() {
		// Given
		CustomerForm customerForm = null;

		// Then
		Customer customer = CustomerDtoConverter.dtoToEntity(customerForm);
		Assertions.assertNull(customer);
	}

	@Test
	@DisplayName("Method entityToDto() - Should execute successful")
	public void entityToDtoSuccessful() {
		// Given
		Driver driver = DriverDtoConverterTest.createNewDriver(1L, "teste", LocalDate.now());
		Customer customer = createNewCustomer(1L, "teste", driver);

		// Then
		CustomerView customerView = CustomerDtoConverter.entityToDto(customer);
		isEquals(customerView, customer);
	}

	@Test
	@DisplayName("Method entityToModel() - Without value")
	public void entityToModelWithoutValue() {
		// Given
		Customer customer = null;

		// Then
		CustomerView customerView = CustomerDtoConverter.entityToDto(customer);
		Assertions.assertNull(customerView);
	}

	public static CustomerForm createNewCustomerForm(String name, Long driverId) {
		return CustomerForm.builder()
				.name(name)
				.driverId(driverId)
				.build();
	}

	public static Customer createNewCustomer(Long id, String name, Driver driver) {
		return Customer.builder()
				.id(id)
				.name(name)
				.driver(driver)
				.build();
	}

	public static void isEquals(Customer customer, CustomerForm customerForm) {
		Assertions.assertNotNull(customer);
		Assertions.assertNotNull(customerForm);

		Assertions.assertEquals(customer.getName(), customerForm.getName());
		Assertions.assertEquals(customer.getDriver().getId(), customerForm.getDriverId());
	}

	public static void isEquals(CustomerView customerView, Customer customer) {
		Assertions.assertNotNull(customerView);
		Assertions.assertNotNull(customer);

		Assertions.assertEquals(customerView.getId(), customer.getId());
		Assertions.assertEquals(customerView.getName(), customer.getName());

		DriverDtoConverterTest.isEquals(customerView.getDriver(), customer.getDriver());
	}

}