package com.fepsilva.insurer.usecase.customer;

import com.fepsilva.insurer.converter.CustomerModelConverter;
import com.fepsilva.insurer.converter.CustomerModelConverterTest;
import com.fepsilva.insurer.converter.DriverModelConverterTest;
import com.fepsilva.insurer.dao.CustomerDao;
import com.fepsilva.insurer.entity.CustomerEntity;
import com.fepsilva.insurer.model.Customer;
import com.fepsilva.insurer.model.Driver;
import com.fepsilva.insurer.usecase.customer.impl.CustomerCreateUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

@ExtendWith(MockitoExtension.class)
public class CustomerCreateUseCaseTest {

	@Mock
	private CustomerDao customerDao;

	@InjectMocks
	private CustomerCreateUseCaseImpl customerCreateUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void executeSuccessful() {
		// Given
		Driver driver = DriverModelConverterTest.createNewDriver(1L, "teste", LocalDate.now());
		Customer customer = CustomerModelConverterTest.createNewCustomer(1L, "teste", driver);

		CustomerEntity customerEntity = CustomerModelConverter.modelToEntity(customer);
		customerEntity.setId(1L);

		// When
		Mockito.when(this.customerDao.persist(CustomerModelConverter.modelToEntity(customer)))
				.thenReturn(customerEntity);

		// Then
		customer = this.customerCreateUseCase.execute(customer);
		Assertions.assertNotNull(customer);
		Assertions.assertNotNull(customer.getId());
	}

	@Test
	@DisplayName("Method execute() - Should return null")
	public void executeNull() {
		// Given
		Driver driver = DriverModelConverterTest.createNewDriver(1L, "teste", LocalDate.now());
		Customer customer = CustomerModelConverterTest.createNewCustomer(1L, "teste", driver);

		// When
		Mockito.when(this.customerDao.persist(CustomerModelConverter.modelToEntity(customer)))
				.thenReturn(null);

		// Then
		customer = this.customerCreateUseCase.execute(customer);
		Assertions.assertNull(customer);
	}

}