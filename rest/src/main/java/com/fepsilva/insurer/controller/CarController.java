package com.fepsilva.insurer.controller;

import com.fepsilva.insurer.converter.CarDtoConverter;
import com.fepsilva.insurer.dto.form.CarForm;
import com.fepsilva.insurer.dto.view.CarView;
import com.fepsilva.insurer.usecase.car.CarCreateUseCase;
import com.fepsilva.insurer.usecase.car.CarDeleteUseCase;
import com.fepsilva.insurer.usecase.car.CarFindAllUseCase;
import com.fepsilva.insurer.usecase.car.CarFindUseCase;
import com.fepsilva.insurer.usecase.car.CarUpdateUseCase;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Validated
@RestController
@RequestMapping("/car")
@Tag(name = "API de Carro", description = "Endpoints responsáveis pela gestão do cadastro de carros")
public class CarController {

	@Autowired
	private CarCreateUseCase carCreateUseCase;

	@Autowired
	private CarFindAllUseCase carFindAllUseCase;

	@Autowired
	private CarFindUseCase carFindUseCase;

	@Autowired
	private CarUpdateUseCase carUpdateUseCase;

	@Autowired
	private CarDeleteUseCase carDeleteUseCase;

	@Operation(summary = "Endpoint para criar um novo carro")
	@ApiResponse(responseCode = "200", description = "Carro cadastrado")
	@PostMapping(consumes = "application/json", produces = "application/json")
	public ResponseEntity<CarView> create(@Valid @RequestBody CarForm carForm) {
		try {
			return Optional.ofNullable(this.carCreateUseCase.execute(CarDtoConverter.dtoToEntity(carForm)))
					.map(CarDtoConverter::entityToDto)
					.map(ResponseEntity::ok)
					.get();
		} catch (DataIntegrityViolationException e) {
			log.error("Dados inválidos", e);
			return ResponseEntity.badRequest().build();
		} catch (Exception e) {
			log.error("Erro ao cadastrar carro", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@Operation(summary = "Endpoint para consultar todos os carros")
	@ApiResponse(responseCode = "200", description = "Carros consultados")
	@GetMapping
	public ResponseEntity<List<CarView>> findAll() {
		try {
			return ResponseEntity.ok(this.carFindAllUseCase.execute().stream()
					.map(CarDtoConverter::entityToDto)
					.collect(Collectors.toList()));
		} catch (Exception e) {
			log.error("Erro ao procurar carro", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@Operation(summary = "Endpoint para consultar um carro")
	@ApiResponse(responseCode = "200", description = "Carro consultado")
	@ApiResponse(responseCode = "204", description = "Carro não encontrado")
	@GetMapping("/{carId}")
	public ResponseEntity<CarView> find(@Parameter(description = "Id do Carro") @PathVariable Long carId) {
		try {
			return Optional.ofNullable(this.carFindUseCase.execute(carId))
					.map(CarDtoConverter::entityToDto)
					.map(ResponseEntity::ok)
					.orElseGet(() -> ResponseEntity.noContent().build());
		} catch (Exception e) {
			log.error("Erro ao procurar carro", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@Operation(summary = "Endpoint para alterar um carro")
	@ApiResponse(responseCode = "200", description = "Carro alterado")
	@ApiResponse(responseCode = "204", description = "Carro não encontrado")
	@PutMapping("/{carId}")
	public ResponseEntity<CarView> update(
			@Parameter(description = "Id do Carro") @PathVariable Long carId,
			@Valid @RequestBody CarForm carForm) {
		try {
			return Optional.ofNullable(this.carUpdateUseCase.execute(carId, CarDtoConverter.dtoToEntity(carForm)))
					.map(CarDtoConverter::entityToDto)
					.map(ResponseEntity::ok)
					.orElseGet(() -> ResponseEntity.noContent().build());
		} catch (DataIntegrityViolationException e) {
			log.error("Dados inválidos", e);
			return ResponseEntity.badRequest().build();
		} catch (Exception e) {
			log.error("Erro ao alterar carro", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@Operation(summary = "Endpoint para remover um carro")
	@ApiResponse(responseCode = "200", description = "Carro removido")
	@ApiResponse(responseCode = "204", description = "Carro não encontrado")
	@DeleteMapping("/{carId}")
	public ResponseEntity delete(@Parameter(description = "Id do Carro") @PathVariable Long carId) {
		try {
			this.carDeleteUseCase.execute(carId);
			return ResponseEntity.ok().build();
		} catch (EmptyResultDataAccessException e) {
			log.info("Carro não encontrado");
			return ResponseEntity.noContent().build();
		} catch (Exception e) {
			log.error("Erro ao remover carro", e);
			return ResponseEntity.internalServerError().build();
		}
	}

}