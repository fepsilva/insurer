package com.fepsilva.insurer.usecase.claim;

import com.fepsilva.insurer.dao.ClaimDao;
import com.fepsilva.insurer.usecase.claim.impl.ClaimDeleteUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ClaimDeleteUseCaseTest {

	@Mock
	private ClaimDao claimDao;

	@InjectMocks
	private ClaimDeleteUseCaseImpl claimDeleteUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void execute() {
		// Given
		Long claimId = 1L;

		// Then
		Assertions.assertDoesNotThrow(() -> this.claimDeleteUseCase.execute(claimId));
	}

}