package com.fepsilva.insurer.dto.form;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Formulário de cadastro de motorista")
public class DriverForm {

	@NotNull(message = "Não pode estar vazio")
	@Schema(description = "CNH do motorista")
	private String document;

	@NotNull(message = "Não pode estar vazio")
	@Schema(description = "Data de nascimento do motorista")
	private LocalDate birthDate;

}