package com.fepsilva.insurer.dao;

import com.fepsilva.insurer.entity.InsuranceEntity;

import java.util.List;
import java.util.Optional;

public interface InsuranceDao {

	public InsuranceEntity persist(InsuranceEntity insuranceEntity);
	public List<InsuranceEntity> findAll();
	public Optional<InsuranceEntity> find(Long insuranceId);
	public InsuranceEntity update(InsuranceEntity insuranceEntity);
	public void delete(Long insuranceId);

}