package com.fepsilva.insurer.converter;

import com.fepsilva.insurer.dto.form.RiskAgeForm;
import com.fepsilva.insurer.dto.view.RiskAgeView;
import com.fepsilva.insurer.model.RiskAge;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

public class RiskAgeDtoConverter {

	public static RiskAge idToEntity(Long riskAgeId) {
		return RiskAge.builder()
				.id(riskAgeId)
				.build();
	}

	public static RiskAge dtoToEntity(RiskAgeForm riskAgeForm) {
		if (riskAgeForm == null) {
			return null;
		}

		return getModelMapper().map(riskAgeForm, RiskAge.class);
	}

	public static RiskAgeView entityToDto(RiskAge riskAge) {
		if (riskAge == null) {
			return null;
		}

		return getModelMapper().map(riskAge, RiskAgeView.class);
	}

	private static ModelMapper getModelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

		return modelMapper;
	}

}