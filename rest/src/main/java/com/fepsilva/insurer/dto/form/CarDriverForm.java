package com.fepsilva.insurer.dto.form;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Formulário de cadastro de vínculo entre carro e motorista")
public class CarDriverForm {

	@NotNull(message = "Não pode estar vazio")
	@Schema(description = "Identificador do carro")
	private Long carId;

	@NotNull(message = "Não pode estar vazio")
	@Schema(description = "Id do motorista que será condutor do veículo")
	private Long driverId;

	@NotNull(message = "Não pode estar vazio")
	@Schema(description = "Flag que sinaliza se o motorista é o principal condutor do veículo")
	private Boolean mainDriver;

}