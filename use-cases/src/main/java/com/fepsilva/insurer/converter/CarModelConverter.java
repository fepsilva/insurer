package com.fepsilva.insurer.converter;

import com.fepsilva.insurer.entity.CarEntity;
import com.fepsilva.insurer.model.Car;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

public class CarModelConverter {

	public static CarEntity modelToEntity(Car car) {
		if (car == null) {
			return null;
		}

		return getModelMapper().map(car, CarEntity.class);
	}

	public static Car entityToModel(CarEntity carEntity) {
		if (carEntity == null) {
			return null;
		}

		return getModelMapper().map(carEntity, Car.class);
	}

	private static ModelMapper getModelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

		return modelMapper;
	}

}