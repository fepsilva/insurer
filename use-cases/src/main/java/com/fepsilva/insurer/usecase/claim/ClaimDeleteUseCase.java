package com.fepsilva.insurer.usecase.claim;

public interface ClaimDeleteUseCase {

	public void execute(Long claimId);

}