package com.fepsilva.insurer.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "CAR_DRIVERS")
public class CarDriverEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "CAR_ID", nullable = false)
	private CarEntity car;

	@ManyToOne
	@JoinColumn(name = "DRIVER_ID", nullable = false)
	private DriverEntity driver;

	@Column(name = "IS_MAIN_DRIVER", nullable = false)
	private Boolean mainDriver;

}