package com.fepsilva.insurer.usecase.car;

import com.fepsilva.insurer.model.Car;

public interface CarUpdateUseCase {

	public Car execute(Long carId, Car car);

}