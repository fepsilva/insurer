package com.fepsilva.insurer.dao;

import com.fepsilva.insurer.dao.impl.CarDaoImpl;
import com.fepsilva.insurer.dao.impl.ClaimDaoImpl;
import com.fepsilva.insurer.dao.impl.DriverDaoImpl;
import com.fepsilva.insurer.entity.CarEntity;
import com.fepsilva.insurer.entity.ClaimEntity;
import com.fepsilva.insurer.entity.DriverEntity;
import com.fepsilva.insurer.repository.ClaimRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class ClaimDaoTest {

	@Mock
	private CarDaoImpl carDao;

	@Mock
	private DriverDaoImpl driverDao;

	@Mock
	private ClaimRepository claimRepository;

	@InjectMocks
	private ClaimDaoImpl claimDao;

	@Test
	@DisplayName("Method persist() - Should execute successful")
	public void persistSuccessful() {
		// Given
		Long id = 1L;
		CarEntity car = CarDaoTest.createNewCarEntity(1L, "model", "manufacturer", "2023", 0F);
		DriverEntity driver = DriverDaoTest.createNewDriverEntity(1L, "document", LocalDate.now());
		LocalDate eventDate = LocalDate.now();

		ClaimEntity claimEntity = this.createNewClaimEntity(null, car, driver, LocalDate.now());
		ClaimEntity claimEntityNew = this.createNewClaimEntity(id, car, driver, LocalDate.now());

		// When
		Mockito.when(this.carDao.find(car.getId()))
				.thenReturn(Optional.of(car));
		Mockito.when(this.driverDao.find(driver.getId()))
				.thenReturn(Optional.of(driver));
		Mockito.when(this.claimRepository.save(claimEntity))
				.thenReturn(claimEntityNew);

		// Then
		claimEntity = claimDao.persist(claimEntity);
		Assertions.assertNotNull(claimEntity.getId());
		Assertions.assertNotNull(claimEntity.getCar().getModel());
		Assertions.assertNotNull(claimEntity.getDriver().getDocument());
	}

	@Test
	@DisplayName("Method persist() - Without complement entity")
	public void persistWithoutComplement() {
		// Given
		Long id = 1L;
		CarEntity car = CarDaoTest.createNewCarEntity(1L, null, null, null, null);
		DriverEntity driver = DriverDaoTest.createNewDriverEntity(1L, null, null);
		LocalDate eventDate = LocalDate.now();

		ClaimEntity claimEntity = this.createNewClaimEntity(null, car, driver, LocalDate.now());
		ClaimEntity claimEntityNew = this.createNewClaimEntity(id, car, driver, LocalDate.now());

		// When
		Mockito.when(this.carDao.find(car.getId()))
				.thenReturn(Optional.ofNullable(null));
		Mockito.when(this.driverDao.find(driver.getId()))
				.thenReturn(Optional.ofNullable(null));
		Mockito.when(this.claimRepository.save(claimEntity))
				.thenReturn(claimEntityNew);

		// Then
		claimEntity = claimDao.persist(claimEntity);
		Assertions.assertNotNull(claimEntity.getId());
		Assertions.assertNull(claimEntity.getCar().getModel());
		Assertions.assertNull(claimEntity.getDriver().getDocument());
	}

	@Test
	@DisplayName("Method findAll() - Should execute successful")
	public void findAllSuccessful() {
		// Given
		CarEntity car = CarDaoTest.createNewCarEntity(1L, "model", "manufacturer", "2023", 0F);
		DriverEntity driver = DriverDaoTest.createNewDriverEntity(1L, "document", LocalDate.now());

		List<ClaimEntity> listClaimEntity = List.of(
				this.createNewClaimEntity(1L, car, driver, LocalDate.now()),
				this.createNewClaimEntity(2L, car, driver, LocalDate.now()),
				this.createNewClaimEntity(3L, car, driver, LocalDate.now()));

		// When
		Mockito.when(this.claimRepository.findAll())
				.thenReturn(listClaimEntity);

		// Then
		listClaimEntity = claimDao.findAll();
		Assertions.assertNotNull(listClaimEntity);
		Assertions.assertEquals(listClaimEntity.size(), 3);
	}

	@Test
	@DisplayName("Method find() - Should execute successful")
	public void findSuccessful() {
		// Given
		Long id = 1L;
		CarEntity car = CarDaoTest.createNewCarEntity(1L, "model", "manufacturer", "2023", 0F);
		DriverEntity driver = DriverDaoTest.createNewDriverEntity(1L, "document", LocalDate.now());
		ClaimEntity claimEntity = this.createNewClaimEntity(id, car, driver, LocalDate.now());

		// When
		Mockito.when(this.claimRepository.findById(id))
				.thenReturn(Optional.of(claimEntity));

		// Then
		Optional<ClaimEntity> optionalClaimEntity = claimDao.find(id);
		Assertions.assertNotNull(optionalClaimEntity.get());
		Assertions.assertEquals(optionalClaimEntity.get().getId(), id);
	}

	@Test
	@DisplayName("Method update() - Should execute successful")
	public void updateSuccessful() {
		// Given
		CarEntity car = CarDaoTest.createNewCarEntity(1L, "model", "manufacturer", "2023", 0F);
		DriverEntity driver = DriverDaoTest.createNewDriverEntity(1L, "document", LocalDate.now());
		ClaimEntity claimEntity = this.createNewClaimEntity(1L, car, driver, LocalDate.now());

		// When
		Mockito.when(this.claimRepository.existsById(claimEntity.getId()))
				.thenReturn(true);
		Mockito.when(this.carDao.find(car.getId()))
				.thenReturn(Optional.of(car));
		Mockito.when(this.driverDao.find(driver.getId()))
				.thenReturn(Optional.of(driver));
		Mockito.when(this.claimRepository.save(claimEntity))
				.thenReturn(claimEntity);

		// Then
		claimEntity = claimDao.update(claimEntity);
		Assertions.assertNotNull(claimEntity.getId());
		Assertions.assertNotNull(claimEntity.getCar().getModel());
		Assertions.assertNotNull(claimEntity.getDriver().getDocument());
	}

	@Test
	@DisplayName("Method update() - Without complement entity")
	public void updateWithoutComplement() {
		// Given
		CarEntity car = CarDaoTest.createNewCarEntity(1L, null, null, null, null);
		DriverEntity driver = DriverDaoTest.createNewDriverEntity(1L, null, null);
		ClaimEntity claimEntity = this.createNewClaimEntity(1L, car, driver, LocalDate.now());

		// When
		Mockito.when(this.claimRepository.existsById(claimEntity.getId()))
				.thenReturn(true);
		Mockito.when(this.carDao.find(car.getId()))
				.thenReturn(Optional.ofNullable(null));
		Mockito.when(this.driverDao.find(driver.getId()))
				.thenReturn(Optional.ofNullable(null));
		Mockito.when(this.claimRepository.save(claimEntity))
				.thenReturn(claimEntity);

		// Then
		claimEntity = claimDao.update(claimEntity);
		Assertions.assertNotNull(claimEntity.getId());
		Assertions.assertNull(claimEntity.getCar().getModel());
		Assertions.assertNull(claimEntity.getDriver().getDocument());
	}

	@Test
	@DisplayName("Method update() - Should not found")
	public void updateNotFound() {
		// Given
		CarEntity car = CarDaoTest.createNewCarEntity(1L, "model", "manufacturer", "2023", 0F);
		DriverEntity driver = DriverDaoTest.createNewDriverEntity(1L, "document", LocalDate.now());
		ClaimEntity claimEntity = this.createNewClaimEntity(1L, car, driver, LocalDate.now());

		// When
		Mockito.when(this.claimRepository.existsById(claimEntity.getId()))
				.thenReturn(false);

		// Then
		claimEntity = claimDao.update(claimEntity);
		Assertions.assertNull(claimEntity);
	}

	@Test
	@DisplayName("Method delete() - Should execute successful")
	public void deleteSuccessful() {
		// Given
		Long id = 1L;

		// Then
		Assertions.assertDoesNotThrow(() -> carDao.delete(id));
	}

	public static ClaimEntity createNewClaimEntity(Long id, CarEntity car, DriverEntity driver, LocalDate eventDate) {
		return ClaimEntity.builder()
				.id(id)
				.car(car)
				.driver(driver)
				.eventDate(eventDate)
				.build();
	}

}