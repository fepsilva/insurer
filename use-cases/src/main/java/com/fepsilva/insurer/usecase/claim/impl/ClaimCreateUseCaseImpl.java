package com.fepsilva.insurer.usecase.claim.impl;

import com.fepsilva.insurer.converter.ClaimModelConverter;
import com.fepsilva.insurer.dao.ClaimDao;
import com.fepsilva.insurer.model.Claim;
import com.fepsilva.insurer.usecase.claim.ClaimCreateUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClaimCreateUseCaseImpl implements ClaimCreateUseCase {

	@Autowired
	private ClaimDao claimDao;

	public Claim execute(Claim claim) {
		return Optional.ofNullable(this.claimDao.persist(ClaimModelConverter.modelToEntity(claim)))
				.map(ClaimModelConverter::entityToModel)
				.orElseGet(() -> null);
	}

}