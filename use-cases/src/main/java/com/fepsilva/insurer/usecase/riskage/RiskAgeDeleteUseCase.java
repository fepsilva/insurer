package com.fepsilva.insurer.usecase.riskage;

public interface RiskAgeDeleteUseCase {

	public void execute(Long riskAgeId);

}