package com.fepsilva.insurer.usecase.riskage;

import com.fepsilva.insurer.model.RiskAge;

public interface RiskAgeCreateUseCase {

	public RiskAge execute(RiskAge riskAge);

}