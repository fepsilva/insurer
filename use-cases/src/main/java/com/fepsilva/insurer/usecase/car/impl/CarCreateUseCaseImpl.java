package com.fepsilva.insurer.usecase.car.impl;

import com.fepsilva.insurer.converter.CarModelConverter;
import com.fepsilva.insurer.model.Car;
import com.fepsilva.insurer.dao.CarDao;
import com.fepsilva.insurer.usecase.car.CarCreateUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CarCreateUseCaseImpl implements CarCreateUseCase {

	@Autowired
	private CarDao carDao;

	public Car execute(Car car) {
		return Optional.ofNullable(this.carDao.persist(CarModelConverter.modelToEntity(car)))
				.map(CarModelConverter::entityToModel)
				.orElseGet(() -> null);
	}

}