package com.fepsilva.insurer.usecase.customer;

import com.fepsilva.insurer.model.Customer;

public interface CustomerCreateUseCase {

	public Customer execute(Customer customer);

}