package com.fepsilva.insurer.converter;

import com.fepsilva.insurer.dto.form.CarForm;
import com.fepsilva.insurer.dto.view.CarView;
import com.fepsilva.insurer.model.Car;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CarDtoConverterTest {

	@Test
	@DisplayName("Method idToEntity() - Should execute successful")
	public void idToEntitySuccessful() {
		// Given
		Long carId = 1L;

		// Then
		Car car = CarDtoConverter.idToEntity(carId);
		Assertions.assertNotNull(car);
		Assertions.assertEquals(car.getId(), carId);
	}

	@Test
	@DisplayName("Method dtoToEntity() - Should execute successful")
	public void dtoToEntitySuccessful() {
		// Given
		CarForm carForm = createNewCarForm("teste", "teste", "2023", 0F);

		// Then
		Car car = CarDtoConverter.dtoToEntity(carForm);
		isEquals(car, carForm);
	}

	@Test
	@DisplayName("Method dtoToEntity() - Without value")
	public void dtoToEntityWithoutValue() {
		// Given
		CarForm carForm = null;

		// Then
		Car car = CarDtoConverter.dtoToEntity(carForm);
		Assertions.assertNull(car);
	}

	@Test
	@DisplayName("Method entityToDto() - Should execute successful")
	public void entityToDtoSuccessful() {
		// Given
		Car car = createNewCar(1L, "teste", "teste", "2023", 0F);

		// Then
		CarView carView = CarDtoConverter.entityToDto(car);
		isEquals(carView, car);
	}

	@Test
	@DisplayName("Method entityToModel() - Without value")
	public void entityToModelWithoutValue() {
		// Given
		Car car = null;

		// Then
		CarView carView = CarDtoConverter.entityToDto(car);
		Assertions.assertNull(carView);
	}

	public static CarForm createNewCarForm(String model, String manufacturer, String modelYear, Float fipeValue) {
		return CarForm.builder()
				.model(model)
				.manufacturer(manufacturer)
				.modelYear(modelYear)
				.fipeValue(fipeValue)
				.build();
	}

	public static Car createNewCar(Long id, String model, String manufacturer, String modelYear, Float fipeValue) {
		return Car.builder()
				.id(id)
				.model(model)
				.manufacturer(manufacturer)
				.modelYear(modelYear)
				.fipeValue(fipeValue)
				.build();
	}

	public static void isEquals(Car car, CarForm carForm) {
		Assertions.assertNotNull(car);
		Assertions.assertNotNull(carForm);

		Assertions.assertEquals(car.getModel(), carForm.getModel());
		Assertions.assertEquals(car.getManufacturer(), carForm.getManufacturer());
		Assertions.assertEquals(car.getModelYear(), carForm.getModelYear());
		Assertions.assertEquals(car.getFipeValue(), carForm.getFipeValue());
	}

	public static void isEquals(CarView carView, Car car) {
		Assertions.assertNotNull(carView);
		Assertions.assertNotNull(car);

		Assertions.assertEquals(carView.getId(), car.getId());
		Assertions.assertEquals(carView.getModel(), car.getModel());
		Assertions.assertEquals(carView.getManufacturer(), car.getManufacturer());
		Assertions.assertEquals(carView.getModelYear(), car.getModelYear());
		Assertions.assertEquals(carView.getFipeValue(), car.getFipeValue());
	}

}