package com.fepsilva.insurer.converter;

import com.fepsilva.insurer.entity.CarDriverEntity;
import com.fepsilva.insurer.entity.CarEntity;
import com.fepsilva.insurer.entity.DriverEntity;
import com.fepsilva.insurer.model.Car;
import com.fepsilva.insurer.model.CarDriver;
import com.fepsilva.insurer.model.Driver;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

@ExtendWith(MockitoExtension.class)
public class CarDriverModelConverterTest {

	@Test
	@DisplayName("Method modelToEntity() - Should execute successful")
	public void modelToEntitySuccessful() {
		// Given
		Car car = CarModelConverterTest.createNewCar(1L, "teste", "teste", "2023", 0F);
		Driver driver = DriverModelConverterTest.createNewDriver(1L, "teste", LocalDate.now());
		CarDriver carDriver = createNewCarDriver(1L, car, driver, true);

		// Then
		CarDriverEntity carDriverEntity = CarDriverModelConverter.modelToEntity(carDriver);
		isEquals(carDriverEntity, carDriver);
	}

	@Test
	@DisplayName("Method modelToEntity() - Without value")
	public void modelToEntityWithoutValue() {
		// Given
		CarDriver carDriver = null;

		// Then
		CarDriverEntity carDriverEntity = CarDriverModelConverter.modelToEntity(carDriver);
		Assertions.assertNull(carDriverEntity);
	}

	@Test
	@DisplayName("Method entityToModel() - Should execute successful")
	public void entityToModelSuccessful() {
		// Given
		CarEntity car = CarModelConverterTest.createNewCarEntity(1L, "teste", "teste", "2023", 0F);
		DriverEntity driver = DriverModelConverterTest.createNewDriverEntity(1L, "teste", LocalDate.now());
		CarDriverEntity carDriverEntity = createNewCarDriverEntity(1L, car, driver, true);

		// Then
		CarDriver carDriver = CarDriverModelConverter.entityToModel(carDriverEntity);
		isEquals(carDriverEntity, carDriver);
	}

	@Test
	@DisplayName("Method entityToModel() - Without value")
	public void entityToModelWithoutValue() {
		// Given
		CarDriverEntity carDriverEntity = null;

		// Then
		CarDriver carDriver = CarDriverModelConverter.entityToModel(carDriverEntity);
		Assertions.assertNull(carDriver);
	}

	public static CarDriverEntity createNewCarDriverEntity(Long id, CarEntity car, DriverEntity driver, Boolean mainDriver) {
		return CarDriverEntity.builder()
				.id(id)
				.car(car)
				.driver(driver)
				.mainDriver(mainDriver)
				.build();
	}

	public static CarDriver createNewCarDriver(Long id, Car car, Driver driver, Boolean mainDriver) {
		return CarDriver.builder()
				.id(id)
				.car(car)
				.driver(driver)
				.mainDriver(mainDriver)
				.build();
	}

	public static void isEquals(CarDriverEntity carDriverEntity, CarDriver carDriver) {
		Assertions.assertNotNull(carDriverEntity);
		Assertions.assertNotNull(carDriver);

		Assertions.assertEquals(carDriverEntity.getId(), carDriver.getId());
		Assertions.assertEquals(carDriverEntity.getMainDriver(), carDriver.getMainDriver());

		CarModelConverterTest.isEquals(carDriverEntity.getCar(), carDriver.getCar());
		DriverModelConverterTest.isEquals(carDriverEntity.getDriver(), carDriver.getDriver());
	}

}