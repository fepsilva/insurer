package com.fepsilva.insurer.dto.form;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Formulário de cadastro de carro")
public class CarForm {

	@NotNull(message = "Não pode estar vazio")
	@Schema(description = "Modelo do veículo")
	private String model;

	@NotNull(message = "Não pode estar vazio")
	@Schema(description = "Fabricante do veículo")
	private String manufacturer;

	@NotNull(message = "Não pode estar vazio")
	@Schema(description = "Ano do modelo do veículo")
	private String modelYear;

	@NotNull(message = "Não pode estar vazio")
	@Schema(description = "Valor do veículo")
	private Float fipeValue;

}