package com.fepsilva.insurer.dao;

import com.fepsilva.insurer.entity.DriverEntity;

import java.util.List;
import java.util.Optional;

public interface DriverDao {

	public DriverEntity persist(DriverEntity carEntity);
	public List<DriverEntity> findAll();
	public Optional<DriverEntity> find(Long carId);
	public DriverEntity update(DriverEntity carEntity);
	public void delete(Long carId);

}