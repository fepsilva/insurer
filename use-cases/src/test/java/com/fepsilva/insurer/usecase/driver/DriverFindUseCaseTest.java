package com.fepsilva.insurer.usecase.driver;

import com.fepsilva.insurer.converter.DriverModelConverterTest;
import com.fepsilva.insurer.dao.DriverDao;
import com.fepsilva.insurer.entity.DriverEntity;
import com.fepsilva.insurer.model.Driver;
import com.fepsilva.insurer.usecase.driver.impl.DriverFindUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class DriverFindUseCaseTest {

	@Mock
	private DriverDao driverDao;

	@InjectMocks
	private DriverFindUseCaseImpl driverFindUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void executeSuccessful() {
		// Given
		Long driverId = 1L;
		DriverEntity driverEntity = DriverModelConverterTest.createNewDriverEntity(driverId, "teste", LocalDate.now());

		// When
		Mockito.when(this.driverDao.find(driverId))
				.thenReturn(Optional.ofNullable(driverEntity));

		// Then
		Driver driver = this.driverFindUseCase.execute(driverId);
		Assertions.assertNotNull(driver);

		DriverModelConverterTest.isEquals(driverEntity, driver);
	}

	@Test
	@DisplayName("Method execute() - Not Found")
	public void executeNotFound() {
		// Given
		Long driverId = 1L;

		// When
		Mockito.when(this.driverDao.find(driverId))
				.thenReturn(Optional.ofNullable(null));

		// Then
		Driver driver = this.driverFindUseCase.execute(driverId);
		Assertions.assertNull(driver);
	}

}