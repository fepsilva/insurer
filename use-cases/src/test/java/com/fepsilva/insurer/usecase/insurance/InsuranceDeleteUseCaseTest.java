package com.fepsilva.insurer.usecase.insurance;

import com.fepsilva.insurer.dao.InsuranceDao;
import com.fepsilva.insurer.usecase.insurance.impl.InsuranceDeleteUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class InsuranceDeleteUseCaseTest {

	@Mock
	private InsuranceDao insuranceDao;

	@InjectMocks
	private InsuranceDeleteUseCaseImpl insuranceDeleteUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void execute() {
		// Given
		Long insuranceId = 1L;

		// Then
		Assertions.assertDoesNotThrow(() -> this.insuranceDeleteUseCase.execute(insuranceId));
	}

}