package com.fepsilva.insurer.repository;

import com.fepsilva.insurer.entity.CarDriverEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarDriverRepository extends JpaRepository<CarDriverEntity, Long> {

}