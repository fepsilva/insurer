package com.fepsilva.insurer.usecase.insurance;

import com.fepsilva.insurer.converter.CarModelConverterTest;
import com.fepsilva.insurer.converter.CustomerModelConverterTest;
import com.fepsilva.insurer.converter.DriverModelConverterTest;
import com.fepsilva.insurer.converter.InsuranceModelConverterTest;
import com.fepsilva.insurer.dao.InsuranceDao;
import com.fepsilva.insurer.entity.CarEntity;
import com.fepsilva.insurer.entity.CustomerEntity;
import com.fepsilva.insurer.entity.DriverEntity;
import com.fepsilva.insurer.entity.InsuranceEntity;
import com.fepsilva.insurer.model.Insurance;
import com.fepsilva.insurer.usecase.insurance.impl.InsuranceFindAllUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class InsuranceFindAllUseCaseTest {

	@Mock
	private InsuranceDao insuranceDao;

	@InjectMocks
	private InsuranceFindAllUseCaseImpl insuranceFindAllUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void executeSuccessful() {
		// Given
		CarEntity car = CarModelConverterTest.createNewCarEntity(1L, "teste", "teste", "2023", 0F);
		DriverEntity driver = DriverModelConverterTest.createNewDriverEntity(1L, "teste", LocalDate.now());
		CustomerEntity customer = CustomerModelConverterTest.createNewCustomerEntity(1L, "teste", driver);

		List<InsuranceEntity> listInsuranceEntity = List.of(
				InsuranceModelConverterTest.createNewInsuranceEntity(1L, customer, LocalDateTime.now(), LocalDateTime.now(), car, true, 0F, 0F),
				InsuranceModelConverterTest.createNewInsuranceEntity(2L, customer, LocalDateTime.now(), LocalDateTime.now(), car, true, 0F, 0F),
				InsuranceModelConverterTest.createNewInsuranceEntity(3L, customer, LocalDateTime.now(), LocalDateTime.now(), car, true, 0F, 0F));

		// When
		Mockito.when(this.insuranceDao.findAll())
				.thenReturn(listInsuranceEntity);

		// Then
		List<Insurance> listInsurance = this.insuranceFindAllUseCase.execute();
		Assertions.assertNotNull(listInsurance);
		Assertions.assertEquals(listInsurance.size(), 3);

		for (int i = 0; i < listInsuranceEntity.size(); i++) {
			InsuranceModelConverterTest.isEquals(listInsuranceEntity.get(i), listInsurance.get(i));
		}
	}

	@Test
	@DisplayName("Method execute() - Empty")
	public void executeEmpty() {
		// When
		Mockito.when(this.insuranceDao.findAll())
				.thenReturn(null);

		// Then
		List<Insurance> listInsurance = this.insuranceFindAllUseCase.execute();
		Assertions.assertNotNull(listInsurance);
		Assertions.assertEquals(listInsurance.size(), 0);
	}

}