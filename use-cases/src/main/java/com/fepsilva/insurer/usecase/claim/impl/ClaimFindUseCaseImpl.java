package com.fepsilva.insurer.usecase.claim.impl;

import com.fepsilva.insurer.converter.ClaimModelConverter;
import com.fepsilva.insurer.dao.ClaimDao;
import com.fepsilva.insurer.model.Claim;
import com.fepsilva.insurer.usecase.claim.ClaimFindUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClaimFindUseCaseImpl implements ClaimFindUseCase {

	@Autowired
	private ClaimDao claimDao;

	public Claim execute(Long claimId) {
		return this.claimDao.find(claimId)
				.map(ClaimModelConverter::entityToModel)
				.orElseGet(() -> null);
	}

}