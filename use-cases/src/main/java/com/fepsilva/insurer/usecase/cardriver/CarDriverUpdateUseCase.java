package com.fepsilva.insurer.usecase.cardriver;

import com.fepsilva.insurer.model.CarDriver;

public interface CarDriverUpdateUseCase {

	public CarDriver execute(Long carDriverId, CarDriver carDriver);

}