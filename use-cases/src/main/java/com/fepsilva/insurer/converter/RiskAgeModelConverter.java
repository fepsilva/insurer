package com.fepsilva.insurer.converter;

import com.fepsilva.insurer.entity.RiskAgeEntity;
import com.fepsilva.insurer.model.RiskAge;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

public class RiskAgeModelConverter {

	public static RiskAgeEntity modelToEntity(RiskAge riskAge) {
		if (riskAge == null) {
			return null;
		}

		return getModelMapper().map(riskAge, RiskAgeEntity.class);
	}

	public static RiskAge entityToModel(RiskAgeEntity riskAgeEntity) {
		if (riskAgeEntity == null) {
			return null;
		}

		return getModelMapper().map(riskAgeEntity, RiskAge.class);
	}

	private static ModelMapper getModelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

		return modelMapper;
	}

}