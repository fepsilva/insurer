package com.fepsilva.insurer.repository;

import com.fepsilva.insurer.entity.CarEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<CarEntity, Long> {

}