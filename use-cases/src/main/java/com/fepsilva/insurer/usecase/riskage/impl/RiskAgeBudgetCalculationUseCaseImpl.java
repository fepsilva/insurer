package com.fepsilva.insurer.usecase.riskage.impl;

import com.fepsilva.insurer.model.Driver;
import com.fepsilva.insurer.model.Insurance;
import com.fepsilva.insurer.model.RiskAge;
import com.fepsilva.insurer.usecase.customer.CustomerFindUseCase;
import com.fepsilva.insurer.usecase.riskage.RiskAgeBudgetCalculationUseCase;
import com.fepsilva.insurer.usecase.riskage.RiskAgeFindAllUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;

@Service
public class RiskAgeBudgetCalculationUseCaseImpl implements RiskAgeBudgetCalculationUseCase {

	@Autowired
	private RiskAgeFindAllUseCase riskAgeFindAllUseCase;

	@Autowired
	private CustomerFindUseCase customerFindUseCase;

	public BigDecimal execute(Insurance insurance) {
		if (insurance.getCustomer() != null) {
			if (insurance.getCustomer().getDriver() == null) {
				insurance.setCustomer(this.customerFindUseCase.execute(insurance.getCustomer().getId()));
			}

			Driver driver = insurance.getCustomer().getDriver();
			List<RiskAge> listRiskAge = riskAgeFindAllUseCase.execute();

			if (listRiskAge != null) {
				return listRiskAge.stream()
						.filter(item -> this.getAge(driver) >= item.getBeginAge() && this.getAge(driver) <= item.getEndAge())
						.findFirst()
						.map(RiskAge::getPercentRisk)
						.orElseGet(() -> BigDecimal.ZERO);
			}
		}

		return BigDecimal.ZERO;
	}

	private Long getAge(Driver driver) {
		return Long.valueOf(Period.between(driver.getBirthDate(), LocalDate.now()).getYears());
	}

}