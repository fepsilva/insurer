package com.fepsilva.insurer.dao;

import com.fepsilva.insurer.entity.CarDriverEntity;

import java.util.List;
import java.util.Optional;

public interface CarDriverDao {

	public CarDriverEntity persist(CarDriverEntity carDriverEntity);
	public List<CarDriverEntity> findAll();
	public Optional<CarDriverEntity> find(Long carDriverId);
	public CarDriverEntity update(CarDriverEntity carDriverEntity);
	public void delete(Long carDriverId);

}