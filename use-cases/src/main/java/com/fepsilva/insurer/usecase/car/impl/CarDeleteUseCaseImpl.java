package com.fepsilva.insurer.usecase.car.impl;

import com.fepsilva.insurer.dao.CarDao;
import com.fepsilva.insurer.usecase.car.CarDeleteUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CarDeleteUseCaseImpl implements CarDeleteUseCase {

	@Autowired
	private CarDao carDao;

	public void execute(Long carId) {
		this.carDao.delete(carId);
	}

}