package com.fepsilva.insurer.usecase.customer;

import com.fepsilva.insurer.converter.CustomerModelConverter;
import com.fepsilva.insurer.converter.CustomerModelConverterTest;
import com.fepsilva.insurer.converter.DriverModelConverterTest;
import com.fepsilva.insurer.dao.CustomerDao;
import com.fepsilva.insurer.entity.CustomerEntity;
import com.fepsilva.insurer.model.Customer;
import com.fepsilva.insurer.model.Driver;
import com.fepsilva.insurer.usecase.customer.impl.CustomerUpdateUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

@ExtendWith(MockitoExtension.class)
public class CustomerUpdateUseCaseTest {

	@Mock
	private CustomerDao customerDao;

	@InjectMocks
	private CustomerUpdateUseCaseImpl customerUpdateUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void executeSuccessful() {
		// Given
		Long customerId = 1L;
		Driver driver = DriverModelConverterTest.createNewDriver(1L, "teste", LocalDate.now());
		Customer customer = CustomerModelConverterTest.createNewCustomer(customerId, "teste", driver);
		CustomerEntity customerEntity = CustomerModelConverter.modelToEntity(customer);

		// When
		Mockito.when(this.customerDao.update(CustomerModelConverter.modelToEntity(customer)))
				.thenReturn(customerEntity);

		// Then
		customer = this.customerUpdateUseCase.execute(customerId, customer);
		Assertions.assertNotNull(customer);
	}

	@Test
	@DisplayName("Method execute() - Not Found")
	public void executeNotFound() {
		// Given
		Long customerId = 1L;
		Driver driver = DriverModelConverterTest.createNewDriver(1L, "teste", LocalDate.now());
		Customer customer = CustomerModelConverterTest.createNewCustomer(customerId, "teste", driver);

		// When
		Mockito.when(this.customerDao.update(CustomerModelConverter.modelToEntity(customer)))
				.thenReturn(null);

		// Then
		customer = this.customerUpdateUseCase.execute(customerId, customer);
		Assertions.assertNull(customer);
	}

}