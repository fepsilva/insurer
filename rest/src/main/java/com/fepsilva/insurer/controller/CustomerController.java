package com.fepsilva.insurer.controller;

import com.fepsilva.insurer.converter.CustomerDtoConverter;
import com.fepsilva.insurer.dto.form.CustomerForm;
import com.fepsilva.insurer.dto.view.CustomerView;
import com.fepsilva.insurer.usecase.customer.CustomerCreateUseCase;
import com.fepsilva.insurer.usecase.customer.CustomerDeleteUseCase;
import com.fepsilva.insurer.usecase.customer.CustomerFindAllUseCase;
import com.fepsilva.insurer.usecase.customer.CustomerFindUseCase;
import com.fepsilva.insurer.usecase.customer.CustomerUpdateUseCase;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Validated
@RestController
@RequestMapping("/customer")
@Tag(name = "API de Cliente", description = "Endpoints responsáveis pela gestão do cadastro de clientes")
public class CustomerController {

	@Autowired
	private CustomerCreateUseCase customerCreateUseCase;

	@Autowired
	private CustomerFindAllUseCase customerFindAllUseCase;

	@Autowired
	private CustomerFindUseCase customerFindUseCase;

	@Autowired
	private CustomerUpdateUseCase customerUpdateUseCase;

	@Autowired
	private CustomerDeleteUseCase customerDeleteUseCase;

	@Operation(summary = "Endpoint para criar um novo cliente")
	@ApiResponse(responseCode = "200", description = "Cliente cadastrado")
	@PostMapping(consumes = "application/json", produces = "application/json")
	public ResponseEntity<CustomerView> create(@Valid @RequestBody CustomerForm customerForm) {
		try {
			return Optional.ofNullable(this.customerCreateUseCase.execute(CustomerDtoConverter.dtoToEntity(customerForm)))
					.map(CustomerDtoConverter::entityToDto)
					.map(ResponseEntity::ok)
					.get();
		} catch (DataIntegrityViolationException e) {
			log.error("Dados inválidos", e);
			return ResponseEntity.badRequest().build();
		} catch (Exception e) {
			log.error("Erro ao cadastrar cliente", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@Operation(summary = "Endpoint para consultar todos os clientes")
	@ApiResponse(responseCode = "200", description = "Clientes consultados")
	@GetMapping
	public ResponseEntity<List<CustomerView>> findAll() {
		try {
			return ResponseEntity.ok(this.customerFindAllUseCase.execute().stream()
					.map(CustomerDtoConverter::entityToDto)
					.collect(Collectors.toList()));
		} catch (Exception e) {
			log.error("Erro ao procurar cliente", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@Operation(summary = "Endpoint para consultar um cliente")
	@ApiResponse(responseCode = "200", description = "Cliente consultado")
	@ApiResponse(responseCode = "204", description = "Cliente não encontrado")
	@GetMapping("/{customerId}")
	public ResponseEntity<CustomerView> find(@Parameter(description = "Id do Cliente") @PathVariable Long customerId) {
		try {
			return Optional.ofNullable(this.customerFindUseCase.execute(customerId))
					.map(CustomerDtoConverter::entityToDto)
					.map(ResponseEntity::ok)
					.orElseGet(() -> ResponseEntity.noContent().build());
		} catch (Exception e) {
			log.error("Erro ao procurar cliente", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@Operation(summary = "Endpoint para alterar um cliente")
	@ApiResponse(responseCode = "200", description = "Cliente alterado")
	@ApiResponse(responseCode = "204", description = "Cliente não encontrado")
	@PutMapping("/{customerId}")
	public ResponseEntity<CustomerView> update(
			@Parameter(description = "Id do Cliente") @PathVariable Long customerId,
			@Valid @RequestBody CustomerForm customerForm) {
		try {
			return Optional.ofNullable(this.customerUpdateUseCase.execute(customerId, CustomerDtoConverter.dtoToEntity(customerForm)))
					.map(CustomerDtoConverter::entityToDto)
					.map(ResponseEntity::ok)
					.orElseGet(() -> ResponseEntity.noContent().build());
		} catch (DataIntegrityViolationException e) {
			log.error("Dados inválidos", e);
			return ResponseEntity.badRequest().build();
		} catch (Exception e) {
			log.error("Erro ao alterar cliente", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@Operation(summary = "Endpoint para remover um cliente")
	@ApiResponse(responseCode = "200", description = "Cliente removido")
	@ApiResponse(responseCode = "204", description = "Cliente não encontrado")
	@DeleteMapping("/{customerId}")
	public ResponseEntity delete(@Parameter(description = "Id do Cliente") @PathVariable Long customerId) {
		try {
			this.customerDeleteUseCase.execute(customerId);
			return ResponseEntity.ok().build();
		} catch (EmptyResultDataAccessException e) {
			log.info("Cliente não encontrado");
			return ResponseEntity.noContent().build();
		} catch (Exception e) {
			log.error("Erro ao remover cliente", e);
			return ResponseEntity.internalServerError().build();
		}
	}

}