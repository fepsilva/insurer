package com.fepsilva.insurer.usecase.driver;

import com.fepsilva.insurer.dao.DriverDao;
import com.fepsilva.insurer.usecase.driver.impl.DriverDeleteUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class DriverDeleteUseCaseTest {

	@Mock
	private DriverDao driverDao;

	@InjectMocks
	private DriverDeleteUseCaseImpl driverDeleteUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void execute() {
		// Given
		Long driverId = 1L;

		// Then
		Assertions.assertDoesNotThrow(() -> this.driverDeleteUseCase.execute(driverId));
	}

}