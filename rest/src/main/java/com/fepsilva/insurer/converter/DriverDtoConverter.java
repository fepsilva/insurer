package com.fepsilva.insurer.converter;

import com.fepsilva.insurer.dto.form.DriverForm;
import com.fepsilva.insurer.dto.view.DriverView;
import com.fepsilva.insurer.model.Driver;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

public class DriverDtoConverter {

	public static Driver idToEntity(Long driverId) {
		return Driver.builder()
				.id(driverId)
				.build();
	}

	public static Driver dtoToEntity(DriverForm driverForm) {
		if (driverForm == null) {
			return null;
		}

		return getModelMapper().map(driverForm, Driver.class);
	}

	public static DriverView entityToDto(Driver driver) {
		if (driver == null) {
			return null;
		}

		return getModelMapper().map(driver, DriverView.class);
	}

	private static ModelMapper getModelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

		return modelMapper;
	}

}