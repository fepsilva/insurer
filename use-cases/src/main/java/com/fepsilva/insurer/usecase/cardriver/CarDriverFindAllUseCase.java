package com.fepsilva.insurer.usecase.cardriver;

import com.fepsilva.insurer.model.CarDriver;

import java.util.List;

public interface CarDriverFindAllUseCase {

	public List<CarDriver> execute();

}