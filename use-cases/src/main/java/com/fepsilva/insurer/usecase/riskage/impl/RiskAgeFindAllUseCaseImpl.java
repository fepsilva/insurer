package com.fepsilva.insurer.usecase.riskage.impl;

import com.fepsilva.insurer.converter.RiskAgeModelConverter;
import com.fepsilva.insurer.dao.RiskAgeDao;
import com.fepsilva.insurer.model.RiskAge;
import com.fepsilva.insurer.usecase.riskage.RiskAgeFindAllUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RiskAgeFindAllUseCaseImpl implements RiskAgeFindAllUseCase {

	@Autowired
	private RiskAgeDao riskAgeDao;

	public List<RiskAge> execute() {
		return Optional.ofNullable(this.riskAgeDao.findAll())
				.orElseGet(ArrayList::new)
				.stream().map(RiskAgeModelConverter::entityToModel)
				.collect(Collectors.toList());
	}

}