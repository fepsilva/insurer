package com.fepsilva.insurer.usecase.car.impl;

import com.fepsilva.insurer.converter.CarModelConverter;
import com.fepsilva.insurer.dao.CarDao;
import com.fepsilva.insurer.model.Car;
import com.fepsilva.insurer.usecase.car.CarFindAllUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CarFindAllUseCaseImpl implements CarFindAllUseCase {

	@Autowired
	private CarDao carDao;

	public List<Car> execute() {
		return Optional.ofNullable(this.carDao.findAll())
				.orElseGet(ArrayList::new)
				.stream().map(CarModelConverter::entityToModel)
				.collect(Collectors.toList());
	}

}