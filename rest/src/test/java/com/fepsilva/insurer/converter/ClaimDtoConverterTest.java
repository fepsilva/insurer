package com.fepsilva.insurer.converter;

import com.fepsilva.insurer.dto.form.ClaimForm;
import com.fepsilva.insurer.dto.view.ClaimView;
import com.fepsilva.insurer.model.Car;
import com.fepsilva.insurer.model.Claim;
import com.fepsilva.insurer.model.Driver;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

@ExtendWith(MockitoExtension.class)
public class ClaimDtoConverterTest {

	@Test
	@DisplayName("Method dtoToEntity() - Should execute successful")
	public void dtoToEntitySuccessful() {
		// Given
		ClaimForm claimForm = createNewClaimForm(1L, 1L, LocalDate.now());

		// Then
		Claim claim = ClaimDtoConverter.dtoToEntity(claimForm);
		isEquals(claim, claimForm);
	}

	@Test
	@DisplayName("Method dtoToEntity() - Without value")
	public void dtoToEntityWithoutValue() {
		// Given
		ClaimForm claimForm = null;

		// Then
		Claim claim = ClaimDtoConverter.dtoToEntity(claimForm);
		Assertions.assertNull(claim);
	}

	@Test
	@DisplayName("Method entityToDto() - Should execute successful")
	public void entityToDtoSuccessful() {
		// Given
		Car car = CarDtoConverterTest.createNewCar(1L, "teste", "teste", "2023", 0F);
		Driver driver = DriverDtoConverterTest.createNewDriver(1L, "teste", LocalDate.now());
		Claim claim = createNewClaim(1L, car, driver, LocalDate.now());

		// Then
		ClaimView claimView = ClaimDtoConverter.entityToDto(claim);
		isEquals(claimView, claim);
	}

	@Test
	@DisplayName("Method entityToModel() - Without value")
	public void entityToModelWithoutValue() {
		// Given
		Claim claim = null;

		// Then
		ClaimView claimView = ClaimDtoConverter.entityToDto(claim);
		Assertions.assertNull(claimView);
	}

	public static ClaimForm createNewClaimForm(Long carId, Long driverId, LocalDate eventDate) {
		return ClaimForm.builder()
				.carId(carId)
				.driverId(driverId)
				.eventDate(eventDate)
				.build();
	}

	public static Claim createNewClaim(Long id, Car car, Driver driver, LocalDate eventDate) {
		return Claim.builder()
				.id(id)
				.car(car)
				.driver(driver)
				.eventDate(eventDate)
				.build();
	}

	public static void isEquals(Claim claim, ClaimForm claimForm) {
		Assertions.assertNotNull(claim);
		Assertions.assertNotNull(claimForm);

		Assertions.assertEquals(claim.getEventDate(), claimForm.getEventDate());
		Assertions.assertEquals(claim.getCar().getId(), claimForm.getCarId());
		Assertions.assertEquals(claim.getDriver().getId(), claimForm.getDriverId());
	}

	public static void isEquals(ClaimView claimView, Claim claim) {
		Assertions.assertNotNull(claimView);
		Assertions.assertNotNull(claim);

		Assertions.assertEquals(claimView.getId(), claim.getId());
		Assertions.assertEquals(claimView.getEventDate(), claim.getEventDate());

		CarDtoConverterTest.isEquals(claimView.getCar(), claim.getCar());
		DriverDtoConverterTest.isEquals(claimView.getDriver(), claim.getDriver());
	}

}