package com.fepsilva.insurer.converter;

import com.fepsilva.insurer.dto.form.InsuranceForm;
import com.fepsilva.insurer.dto.view.InsuranceView;
import com.fepsilva.insurer.model.Insurance;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

public class InsuranceDtoConverter {

	public static Insurance dtoToEntity(InsuranceForm insuranceForm) {
		if (insuranceForm == null) {
			return null;
		}

		Insurance insurance = getModelMapper().map(insuranceForm, Insurance.class);
		insurance.setCustomer(CustomerDtoConverter.idToEntity(insuranceForm.getCustomerId()));
		insurance.setCar(CarDtoConverter.idToEntity(insuranceForm.getCarId()));

		return insurance;
	}

	public static InsuranceView entityToDto(Insurance insurance) {
		if (insurance == null) {
			return null;
		}

		InsuranceView insuranceView = getModelMapper().map(insurance, InsuranceView.class);
		insuranceView.setCustomer(CustomerDtoConverter.entityToDto(insurance.getCustomer()));
		insuranceView.setCar(CarDtoConverter.entityToDto(insurance.getCar()));
		insuranceView.setFinalValue(insuranceView.getFipeValue() * (1F + insuranceView.getPercentRisk()));

		return insuranceView;
	}

	private static ModelMapper getModelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

		return modelMapper;
	}

}