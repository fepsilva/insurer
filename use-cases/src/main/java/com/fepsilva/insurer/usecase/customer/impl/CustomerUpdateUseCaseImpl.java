package com.fepsilva.insurer.usecase.customer.impl;

import com.fepsilva.insurer.converter.CustomerModelConverter;
import com.fepsilva.insurer.dao.CustomerDao;
import com.fepsilva.insurer.model.Customer;
import com.fepsilva.insurer.usecase.customer.CustomerUpdateUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerUpdateUseCaseImpl implements CustomerUpdateUseCase {

	@Autowired
	private CustomerDao customerDao;

	public Customer execute(Long carId, Customer customer) {
		customer.setId(carId);

		return Optional.ofNullable(this.customerDao.update(CustomerModelConverter.modelToEntity(customer)))
				.map(CustomerModelConverter::entityToModel)
				.orElseGet(() -> null);
	}

}