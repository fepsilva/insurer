package com.fepsilva.insurer.usecase.customer;

import com.fepsilva.insurer.dao.CustomerDao;
import com.fepsilva.insurer.usecase.customer.impl.CustomerDeleteUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CustomerDeleteUseCaseTest {

	@Mock
	private CustomerDao customerDao;

	@InjectMocks
	private CustomerDeleteUseCaseImpl customerDeleteUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void execute() {
		// Given
		Long customerId = 1L;

		// Then
		Assertions.assertDoesNotThrow(() -> this.customerDeleteUseCase.execute(customerId));
	}

}