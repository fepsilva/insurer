package com.fepsilva.insurer.usecase.riskage;

import com.fepsilva.insurer.converter.CustomerModelConverterTest;
import com.fepsilva.insurer.converter.DriverModelConverterTest;
import com.fepsilva.insurer.converter.InsuranceModelConverterTest;
import com.fepsilva.insurer.converter.RiskAgeModelConverterTest;
import com.fepsilva.insurer.dao.RiskAgeDao;
import com.fepsilva.insurer.entity.RiskAgeEntity;
import com.fepsilva.insurer.model.Customer;
import com.fepsilva.insurer.model.Driver;
import com.fepsilva.insurer.model.Insurance;
import com.fepsilva.insurer.model.RiskAge;
import com.fepsilva.insurer.usecase.customer.CustomerFindUseCase;
import com.fepsilva.insurer.usecase.riskage.impl.RiskAgeBudgetCalculationUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.util.Pair;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class RiskAgeBudgetCalculationUseCaseTest {

	@Mock
	private RiskAgeDao riskAgeDao;

	@Mock
	private RiskAgeFindAllUseCase riskAgeFindAllUseCase;

	@Mock
	private CustomerFindUseCase customerFindUseCase;

	@InjectMocks
	private RiskAgeBudgetCalculationUseCaseImpl riskAgeBudgetCalculationUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void executeSuccessful() {
		// Given
		Pair<Integer, BigDecimal> firstTest = Pair.of(18, BigDecimal.valueOf(1));
		Pair<Integer, BigDecimal> secondTest = Pair.of(30, BigDecimal.valueOf(3));
		Pair<Integer, BigDecimal> thirdTest = Pair.of(42, BigDecimal.valueOf(4));
		Driver driver = DriverModelConverterTest.createNewDriver(1L, "teste", null);

		List<RiskAge> listRiskAge = List.of(
				RiskAgeModelConverterTest.createNewRiskAge(1L, 18L, 25L, firstTest.getSecond()),
				RiskAgeModelConverterTest.createNewRiskAge(1L, 18L, 25L, BigDecimal.ZERO),
				RiskAgeModelConverterTest.createNewRiskAge(2L, 25L, 30L, secondTest.getSecond()),
				RiskAgeModelConverterTest.createNewRiskAge(3L, 30L, 45L, thirdTest.getSecond()));

		// First Test
		driver.setBirthDate(LocalDate.now().minusYears(firstTest.getFirst()));
		executeSuccessfulAux(driver, listRiskAge, firstTest.getSecond());

		// Second Test
		driver.setBirthDate(LocalDate.now().minusYears(secondTest.getFirst()));
		executeSuccessfulAux(driver, listRiskAge, secondTest.getSecond());

		// Third Test
		driver.setBirthDate(LocalDate.now().minusYears(thirdTest.getFirst()));
		executeSuccessfulAux(driver, listRiskAge, thirdTest.getSecond());
	}

	@Test
	@DisplayName("Method execute() - Risk age not found")
	public void executeRiskAgeNotFound() {
		// Given
		Driver driver = DriverModelConverterTest.createNewDriver(1L, "teste", LocalDate.now().minusYears(18));

		List<RiskAge> listRiskAge = List.of(
				RiskAgeModelConverterTest.createNewRiskAge(1L, 19L, 25L, BigDecimal.ONE));

		// Then
		executeSuccessfulAux(driver, listRiskAge, BigDecimal.ZERO);
	}

	@Test
	@DisplayName("Method execute() - Without driver")
	public void executeWithoutDriver() {
		// Given
		Customer customer = CustomerModelConverterTest.createNewCustomer(1L, "teste", null);
		Insurance insurance = InsuranceModelConverterTest.createNewInsurance(1L, customer, LocalDateTime.now(), LocalDateTime.now(), null, true, 0F, 0F);

		Driver driver = DriverModelConverterTest.createNewDriver(1L, "teste", LocalDate.now());
		Customer customerNew = CustomerModelConverterTest.createNewCustomer(1L, "teste", driver);

		// When
		Mockito.when(this.customerFindUseCase.execute(insurance.getCustomer().getId()))
				.thenReturn(customerNew);
		Mockito.when(this.riskAgeFindAllUseCase.execute())
				.thenReturn(null);

		// Then
		BigDecimal value = this.riskAgeBudgetCalculationUseCase.execute(insurance);
		Assertions.assertNotNull(value);
		Assertions.assertEquals(value, BigDecimal.ZERO);
	}

	@Test
	@DisplayName("Method execute() - Without list of risk ages")
	public void executeWithoutList() {
		// Given
		Driver driver = DriverModelConverterTest.createNewDriver(1L, "teste", LocalDate.now());
		Customer customer = CustomerModelConverterTest.createNewCustomer(1L, "teste", driver);
		Insurance insurance = InsuranceModelConverterTest.createNewInsurance(1L, customer, LocalDateTime.now(), LocalDateTime.now(), null, true, 0F, 0F);

		// When
		Mockito.when(this.riskAgeFindAllUseCase.execute())
				.thenReturn(null);

		// Then
		BigDecimal value = this.riskAgeBudgetCalculationUseCase.execute(insurance);
		Assertions.assertNotNull(value);
		Assertions.assertEquals(value, BigDecimal.ZERO);
	}

	@Test
	@DisplayName("Method execute() - Without customer")
	public void executeWithoutCustomer() {
		// Given
		Insurance insurance = InsuranceModelConverterTest.createNewInsurance(1L, null, LocalDateTime.now(), LocalDateTime.now(), null, true, 0F, 0F);

		// Then
		BigDecimal value = this.riskAgeBudgetCalculationUseCase.execute(insurance);
		Assertions.assertNotNull(value);
		Assertions.assertEquals(value, BigDecimal.ZERO);
	}

	private void executeSuccessfulAux(Driver driver, List<RiskAge> listRiskAge, BigDecimal result) {
		// Given
		Customer customer = CustomerModelConverterTest.createNewCustomer(1L, "teste", driver);
		Insurance insurance = InsuranceModelConverterTest.createNewInsurance(1L, customer, LocalDateTime.now(), LocalDateTime.now(), null, true, 0F, 0F);

		// When
		Mockito.when(this.riskAgeFindAllUseCase.execute())
				.thenReturn(listRiskAge);

		// Then
		BigDecimal value = this.riskAgeBudgetCalculationUseCase.execute(insurance);
		Assertions.assertNotNull(value);
		Assertions.assertEquals(value, result);
	}

}