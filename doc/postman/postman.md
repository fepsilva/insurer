# Postman

Está é a collection para poder realizar os testes local

## Requisitos

- Ter o Postman instalado, caso não tenha utilize este <a href="https://www.postman.com/downloads/" download>link</a>

## Instalação

1. Baixe o arquivo [Insurer.postman_collection.json](Insurer.postman_collection.json "download")
2. Abra o Postman
3. Clique no botão **Import**

![Print](postman01.PNG)

4. Selecione a opção **File**

![Print](postman02.PNG)

5. Selecione o arquivo que foi baixado

6. Clique no botão **Import**

![Print](postman03.PNG)

7. Pronto, agora só realizar os testes

![Print](postman04.PNG)

[<< Voltar para página principal](/README.md)