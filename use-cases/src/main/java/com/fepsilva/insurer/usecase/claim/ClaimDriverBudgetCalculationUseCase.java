package com.fepsilva.insurer.usecase.claim;

import com.fepsilva.insurer.model.Insurance;

import java.math.BigDecimal;

public interface ClaimDriverBudgetCalculationUseCase {

	public BigDecimal execute(Insurance insurance);

}