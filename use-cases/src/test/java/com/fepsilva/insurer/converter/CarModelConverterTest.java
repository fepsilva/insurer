package com.fepsilva.insurer.converter;

import com.fepsilva.insurer.entity.CarEntity;
import com.fepsilva.insurer.model.Car;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CarModelConverterTest {

	@Test
	@DisplayName("Method modelToEntity() - Should execute successful")
	public void modelToEntitySuccessful() {
		// Given
		Car car = createNewCar(1L, "teste", "teste", "2023", 0F);

		// Then
		CarEntity carEntity = CarModelConverter.modelToEntity(car);
		isEquals(carEntity, car);
	}

	@Test
	@DisplayName("Method modelToEntity() - Without value")
	public void modelToEntityWithoutValue() {
		// Given
		Car car = null;

		// Then
		CarEntity carEntity = CarModelConverter.modelToEntity(car);
		Assertions.assertNull(carEntity);
	}

	@Test
	@DisplayName("Method entityToModel() - Should execute successful")
	public void entityToModelSuccessful() {
		// Given
		CarEntity carEntity = createNewCarEntity(1L, "teste", "teste", "2023", 0F);

		// Then
		Car car = CarModelConverter.entityToModel(carEntity);
		isEquals(carEntity, car);
	}

	@Test
	@DisplayName("Method entityToModel() - Without value")
	public void entityToModelWithoutValue() {
		// Given
		CarEntity carEntity = null;

		// Then
		Car car = CarModelConverter.entityToModel(carEntity);
		Assertions.assertNull(car);
	}

	public static CarEntity createNewCarEntity(Long id, String model, String manufacturer, String modelYear, Float fipeValue) {
		return CarEntity.builder()
				.id(id)
				.model(model)
				.manufacturer(manufacturer)
				.modelYear(modelYear)
				.fipeValue(fipeValue)
				.build();
	}

	public static Car createNewCar(Long id, String model, String manufacturer, String modelYear, Float fipeValue) {
		return Car.builder()
				.id(id)
				.model(model)
				.manufacturer(manufacturer)
				.modelYear(modelYear)
				.fipeValue(fipeValue)
				.build();
	}

	public static void isEquals(CarEntity carEntity, Car car) {
		Assertions.assertNotNull(carEntity);
		Assertions.assertNotNull(car);

		Assertions.assertEquals(carEntity.getId(), car.getId());
		Assertions.assertEquals(carEntity.getModel(), car.getModel());
		Assertions.assertEquals(carEntity.getManufacturer(), car.getManufacturer());
		Assertions.assertEquals(carEntity.getModelYear(), car.getModelYear());
		Assertions.assertEquals(carEntity.getFipeValue(), car.getFipeValue());
	}

}