package com.fepsilva.insurer.usecase.car.impl;

import com.fepsilva.insurer.converter.CarModelConverter;
import com.fepsilva.insurer.dao.CarDao;
import com.fepsilva.insurer.model.Car;
import com.fepsilva.insurer.usecase.car.CarUpdateUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CarUpdateUseCaseImpl implements CarUpdateUseCase {

	@Autowired
	private CarDao carDao;

	public Car execute(Long carId, Car car) {
		car.setId(carId);

		return Optional.ofNullable(this.carDao.update(CarModelConverter.modelToEntity(car)))
				.map(CarModelConverter::entityToModel)
				.orElseGet(() -> null);
	}

}