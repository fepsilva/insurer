package com.fepsilva.insurer.dao;

import com.fepsilva.insurer.dao.impl.RiskAgeDaoImpl;
import com.fepsilva.insurer.entity.RiskAgeEntity;
import com.fepsilva.insurer.repository.RiskAgeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class RiskDaoTest {

	@Mock
	private RiskAgeRepository carRepository;

	@InjectMocks
	private RiskAgeDaoImpl carDao;

	@Test
	@DisplayName("Method persist() - Should execute successful")
	public void persistSuccessful() {
		// Given
		Long id = 1L;
		Long beginAge = 18L;
		Long endAge = 25L;
		BigDecimal percentRisk = BigDecimal.ZERO;

		RiskAgeEntity carEntity = createNewRiskAgeEntity(null, beginAge, endAge, percentRisk);
		RiskAgeEntity carEntityNew = createNewRiskAgeEntity(id, beginAge, endAge, percentRisk);

		// When
		Mockito.when(this.carRepository.save(carEntity))
				.thenReturn(carEntityNew);

		// Then
		carEntity = carDao.persist(carEntity);
		Assertions.assertNotNull(carEntity.getId());
	}

	@Test
	@DisplayName("Method findAll() - Should execute successful")
	public void findAllSuccessful() {
		// Given
		List<RiskAgeEntity> listRiskAgeEntity = List.of(
				createNewRiskAgeEntity(1L, 18L, 25L, BigDecimal.ZERO),
				createNewRiskAgeEntity(2L, 18L, 25L, BigDecimal.ZERO),
				createNewRiskAgeEntity(3L, 18L, 25L, BigDecimal.ZERO));

		// When
		Mockito.when(this.carRepository.findAll())
				.thenReturn(listRiskAgeEntity);

		// Then
		listRiskAgeEntity = carDao.findAll();
		Assertions.assertNotNull(listRiskAgeEntity);
		Assertions.assertEquals(listRiskAgeEntity.size(), 3);
	}

	@Test
	@DisplayName("Method find() - Should execute successful")
	public void findSuccessful() {
		// Given
		Long id = 1L;
		RiskAgeEntity carEntity = createNewRiskAgeEntity(id, 18L, 25L, BigDecimal.ZERO);

		// When
		Mockito.when(this.carRepository.findById(id))
				.thenReturn(Optional.of(carEntity));

		// Then
		Optional<RiskAgeEntity> optionalRiskAgeEntity = carDao.find(id);
		Assertions.assertNotNull(optionalRiskAgeEntity.get());
		Assertions.assertEquals(optionalRiskAgeEntity.get().getId(), id);
	}

	@Test
	@DisplayName("Method update() - Should execute successful")
	public void updateSuccessful() {
		// Given
		RiskAgeEntity carEntity = createNewRiskAgeEntity(1L, 18L, 25L, BigDecimal.ZERO);

		// When
		Mockito.when(this.carRepository.existsById(carEntity.getId()))
				.thenReturn(true);
		Mockito.when(this.carRepository.save(carEntity))
				.thenReturn(carEntity);

		// Then
		carEntity = carDao.update(carEntity);
		Assertions.assertNotNull(carEntity.getId());
	}

	@Test
	@DisplayName("Method update() - Should not found")
	public void updateNotFound() {
		// Given
		RiskAgeEntity carEntity = createNewRiskAgeEntity(1L, 18L, 25L, BigDecimal.ZERO);

		// When
		Mockito.when(this.carRepository.existsById(carEntity.getId()))
				.thenReturn(false);

		// Then
		carEntity = carDao.update(carEntity);
		Assertions.assertNull(carEntity);
	}

	@Test
	@DisplayName("Method delete() - Should execute successful")
	public void deleteSuccessful() {
		// Given
		Long id = 1L;

		// Then
		Assertions.assertDoesNotThrow(() -> carDao.delete(id));
	}

	public static RiskAgeEntity createNewRiskAgeEntity(Long id, Long beginAge, Long endAge, BigDecimal percentRisk) {
		return RiskAgeEntity.builder()
				.id(id)
				.beginAge(beginAge)
				.endAge(endAge)
				.percentRisk(percentRisk)
				.build();
	}

}