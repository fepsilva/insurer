package com.fepsilva.insurer.usecase.claim;

import com.fepsilva.insurer.model.Claim;

import java.util.List;

public interface ClaimFindAllUseCase {

	public List<Claim> execute();

}