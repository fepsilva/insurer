package com.fepsilva.insurer.dto.view;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Visualização do cadastro de seguro")
public class InsuranceView {

	@Schema(description = "Identificador do carro")
	private Long id;

	@Schema(description = "Cliente")
	private CustomerView customer;

	@Schema(description = "Data da solicitação do orçamento")
	private LocalDateTime creationDt;

	@Schema(description = "Data da atualização do orçamento")
	private LocalDateTime updatedDt;

	@Schema(description = "Carro")
	private CarView car;

	@Schema(description = "Flag se o orçamento está ativo")
	private Boolean active;

	@Schema(description = "Porcentagem cobrada baseado no risco")
	private Float percentRisk;

	@Schema(description = "Valor fipe no momento do orçamento")
	private Float fipeValue;

	@Schema(description = "Valor final do orçamento")
	private Float finalValue;

}