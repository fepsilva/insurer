package com.fepsilva.insurer.usecase.car;

import com.fepsilva.insurer.model.Car;

import java.util.List;

public interface CarFindAllUseCase {

	public List<Car> execute();

}