package com.fepsilva.insurer.usecase.riskage;

import com.fepsilva.insurer.converter.RiskAgeModelConverterTest;
import com.fepsilva.insurer.dao.RiskAgeDao;
import com.fepsilva.insurer.entity.RiskAgeEntity;
import com.fepsilva.insurer.model.RiskAge;
import com.fepsilva.insurer.usecase.riskage.impl.RiskAgeFindUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class RiskAgeFindUseCaseTest {

	@Mock
	private RiskAgeDao riskAgeDao;

	@InjectMocks
	private RiskAgeFindUseCaseImpl riskAgeFindUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void executeSuccessful() {
		// Given
		Long riskAgeId = 1L;
		RiskAgeEntity riskAgeEntity = RiskAgeModelConverterTest.createNewRiskAgeEntity(riskAgeId, 18L, 25L, BigDecimal.ZERO);

		// When
		Mockito.when(this.riskAgeDao.find(riskAgeId))
				.thenReturn(Optional.ofNullable(riskAgeEntity));

		// Then
		RiskAge riskAge = this.riskAgeFindUseCase.execute(riskAgeId);
		Assertions.assertNotNull(riskAge);

		RiskAgeModelConverterTest.isEquals(riskAgeEntity, riskAge);
	}

	@Test
	@DisplayName("Method execute() - Not Found")
	public void executeNotFound() {
		// Given
		Long riskAgeId = 1L;

		// When
		Mockito.when(this.riskAgeDao.find(riskAgeId))
				.thenReturn(Optional.ofNullable(null));

		// Then
		RiskAge riskAge = this.riskAgeFindUseCase.execute(riskAgeId);
		Assertions.assertNull(riskAge);
	}

}