package com.fepsilva.insurer.usecase.car;

import com.fepsilva.insurer.converter.CarModelConverterTest;
import com.fepsilva.insurer.dao.CarDao;
import com.fepsilva.insurer.entity.CarEntity;
import com.fepsilva.insurer.model.Car;
import com.fepsilva.insurer.usecase.car.impl.CarFindUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class CarFindUseCaseTest {

	@Mock
	private CarDao carDao;

	@InjectMocks
	private CarFindUseCaseImpl carFindUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void executeSuccessful() {
		// Given
		Long carId = 1L;
		CarEntity carEntity = CarModelConverterTest.createNewCarEntity(carId, "teste", "teste", "2023", 0F);

		// When
		Mockito.when(this.carDao.find(carId))
				.thenReturn(Optional.ofNullable(carEntity));

		// Then
		Car car = this.carFindUseCase.execute(carId);
		Assertions.assertNotNull(car);

		CarModelConverterTest.isEquals(carEntity, car);
	}

	@Test
	@DisplayName("Method execute() - Not Found")
	public void executeNotFound() {
		// Given
		Long carId = 1L;

		// When
		Mockito.when(this.carDao.find(carId))
				.thenReturn(Optional.ofNullable(null));

		// Then
		Car car = this.carFindUseCase.execute(carId);
		Assertions.assertNull(car);
	}

}