package com.fepsilva.insurer.usecase.riskage;

import com.fepsilva.insurer.model.Insurance;

import java.math.BigDecimal;

public interface RiskAgeBudgetCalculationUseCase {

	public BigDecimal execute(Insurance insurance);

}