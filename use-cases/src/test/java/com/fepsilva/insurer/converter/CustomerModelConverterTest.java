package com.fepsilva.insurer.converter;

import com.fepsilva.insurer.entity.CustomerEntity;
import com.fepsilva.insurer.entity.DriverEntity;
import com.fepsilva.insurer.model.Customer;
import com.fepsilva.insurer.model.Driver;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

@ExtendWith(MockitoExtension.class)
public class CustomerModelConverterTest {

	@Test
	@DisplayName("Method modelToEntity() - Should execute successful")
	public void modelToEntitySuccessful() {
		// Given
		Driver driver = DriverModelConverterTest.createNewDriver(1L, "teste", LocalDate.now());
		Customer customer = createNewCustomer(1L, "teste", driver);

		// Then
		CustomerEntity customerEntity = CustomerModelConverter.modelToEntity(customer);
		isEquals(customerEntity, customer);
	}

	@Test
	@DisplayName("Method modelToEntity() - Without value")
	public void modelToEntityWithoutValue() {
		// Given
		Customer customer = null;

		// Then
		CustomerEntity customerEntity = CustomerModelConverter.modelToEntity(customer);
		Assertions.assertNull(customerEntity);
	}

	@Test
	@DisplayName("Method entityToModel() - Should execute successful")
	public void entityToModelSuccessful() {
		// Given
		DriverEntity driver = DriverModelConverterTest.createNewDriverEntity(1L, "teste", LocalDate.now());
		CustomerEntity customerEntity = createNewCustomerEntity(1L, "teste", driver);

		// Then
		Customer customer = CustomerModelConverter.entityToModel(customerEntity);
		isEquals(customerEntity, customer);
	}

	@Test
	@DisplayName("Method entityToModel() - Without value")
	public void entityToModelWithoutValue() {
		// Given
		CustomerEntity customerEntity = null;

		// Then
		Customer customer = CustomerModelConverter.entityToModel(customerEntity);
		Assertions.assertNull(customer);
	}

	public static CustomerEntity createNewCustomerEntity(Long id, String name, DriverEntity driver) {
		return CustomerEntity.builder()
				.id(id)
				.name(name)
				.driver(driver)
				.build();
	}

	public static Customer createNewCustomer(Long id, String name, Driver driver) {
		return Customer.builder()
				.id(id)
				.name(name)
				.driver(driver)
				.build();
	}

	public static void isEquals(CustomerEntity customerEntity, Customer customer) {
		Assertions.assertNotNull(customerEntity);
		Assertions.assertNotNull(customer);

		Assertions.assertEquals(customerEntity.getId(), customer.getId());
		Assertions.assertEquals(customerEntity.getName(), customer.getName());

		DriverModelConverterTest.isEquals(customerEntity.getDriver(), customer.getDriver());
	}

}