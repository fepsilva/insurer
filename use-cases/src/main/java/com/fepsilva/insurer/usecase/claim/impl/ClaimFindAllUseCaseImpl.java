package com.fepsilva.insurer.usecase.claim.impl;

import com.fepsilva.insurer.converter.ClaimModelConverter;
import com.fepsilva.insurer.dao.ClaimDao;
import com.fepsilva.insurer.model.Claim;
import com.fepsilva.insurer.usecase.claim.ClaimFindAllUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ClaimFindAllUseCaseImpl implements ClaimFindAllUseCase {

	@Autowired
	private ClaimDao claimDao;

	public List<Claim> execute() {
		return Optional.ofNullable(this.claimDao.findAll())
				.orElseGet(ArrayList::new)
				.stream().map(ClaimModelConverter::entityToModel)
				.collect(Collectors.toList());
	}

}