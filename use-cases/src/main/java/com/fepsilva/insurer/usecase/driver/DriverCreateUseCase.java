package com.fepsilva.insurer.usecase.driver;

import com.fepsilva.insurer.model.Driver;

public interface DriverCreateUseCase {

	public Driver execute(Driver driver);

}