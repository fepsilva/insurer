package com.fepsilva.insurer.usecase.driver;

import com.fepsilva.insurer.model.Driver;

public interface DriverFindUseCase {

	public Driver execute(Long driverId);

}