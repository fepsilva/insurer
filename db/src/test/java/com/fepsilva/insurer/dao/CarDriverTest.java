package com.fepsilva.insurer.dao;

import com.fepsilva.insurer.dao.impl.CarDaoImpl;
import com.fepsilva.insurer.dao.impl.CarDriverDaoImpl;
import com.fepsilva.insurer.dao.impl.DriverDaoImpl;
import com.fepsilva.insurer.entity.CarDriverEntity;
import com.fepsilva.insurer.entity.CarEntity;
import com.fepsilva.insurer.entity.DriverEntity;
import com.fepsilva.insurer.repository.CarDriverRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class CarDriverTest {

	@Mock
	private CarDaoImpl carDao;

	@Mock
	private DriverDaoImpl driverDao;

	@Mock
	private CarDriverRepository carDriverRepository;

	@InjectMocks
	private CarDriverDaoImpl carDriverDao;

	@Test
	@DisplayName("Method persist() - Should execute successful")
	public void persistSuccessful() {
		// Given
		Long id = 1L;
		CarEntity car = CarDaoTest.createNewCarEntity(1L, "model", "manufacturer", "2023", 0F);
		DriverEntity driver = DriverDaoTest.createNewDriverEntity(1L, "document", LocalDate.now());
		Boolean mainDriver = true;

		CarDriverEntity carDriverEntity = createNewCarDriverEntity(null, car, driver, mainDriver);
		CarDriverEntity carDriverEntityNew = createNewCarDriverEntity(id, car, driver, mainDriver);

		// When
		Mockito.when(this.carDao.find(car.getId()))
				.thenReturn(Optional.of(car));
		Mockito.when(this.driverDao.find(driver.getId()))
				.thenReturn(Optional.of(driver));
		Mockito.when(this.carDriverRepository.save(carDriverEntity))
				.thenReturn(carDriverEntityNew);

		// Then
		carDriverEntity = carDriverDao.persist(carDriverEntity);
		Assertions.assertNotNull(carDriverEntity.getId());
		Assertions.assertNotNull(carDriverEntity.getCar().getModel());
		Assertions.assertNotNull(carDriverEntity.getDriver().getDocument());
	}

	@Test
	@DisplayName("Method persist() - Without complement entity")
	public void persistWithoutComplement() {
		// Given
		Long id = 1L;
		CarEntity car = CarDaoTest.createNewCarEntity(1L, null, null, null, null);
		DriverEntity driver = DriverDaoTest.createNewDriverEntity(1L, null, null);
		Boolean mainDriver = true;

		CarDriverEntity carDriverEntity = createNewCarDriverEntity(null, car, driver, mainDriver);
		CarDriverEntity carDriverEntityNew = createNewCarDriverEntity(id, car, driver, mainDriver);

		// When
		Mockito.when(this.carDao.find(car.getId()))
				.thenReturn(Optional.ofNullable(null));
		Mockito.when(this.driverDao.find(driver.getId()))
				.thenReturn(Optional.ofNullable(null));
		Mockito.when(this.carDriverRepository.save(carDriverEntity))
				.thenReturn(carDriverEntityNew);

		// Then
		carDriverEntity = carDriverDao.persist(carDriverEntity);
		Assertions.assertNotNull(carDriverEntity.getId());
		Assertions.assertNull(carDriverEntity.getCar().getModel());
		Assertions.assertNull(carDriverEntity.getDriver().getDocument());
	}

	@Test
	@DisplayName("Method findAll() - Should execute successful")
	public void findAllSuccessful() {
		// Given
		CarEntity car = CarDaoTest.createNewCarEntity(1L, "model", "manufacturer", "2023", 0F);
		DriverEntity driver = DriverDaoTest.createNewDriverEntity(1L, "document", LocalDate.now());

		List<CarDriverEntity> listCarDriverEntity = List.of(
				createNewCarDriverEntity(1L, car, driver, true),
				createNewCarDriverEntity(2L, car, driver, false),
				createNewCarDriverEntity(3L, car, driver, false));

		// When
		Mockito.when(this.carDriverRepository.findAll())
				.thenReturn(listCarDriverEntity);

		// Then
		listCarDriverEntity = carDriverDao.findAll();
		Assertions.assertNotNull(listCarDriverEntity);
		Assertions.assertEquals(listCarDriverEntity.size(), 3);
	}

	@Test
	@DisplayName("Method find() - Should execute successful")
	public void findSuccessful() {
		// Given
		Long id = 1L;
		CarEntity car = CarDaoTest.createNewCarEntity(1L, "model", "manufacturer", "2023", 0F);
		DriverEntity driver = DriverDaoTest.createNewDriverEntity(1L, "document", LocalDate.now());
		CarDriverEntity carDriverEntity = createNewCarDriverEntity(id, car, driver, true);

		// When
		Mockito.when(this.carDriverRepository.findById(id))
				.thenReturn(Optional.of(carDriverEntity));

		// Then
		Optional<CarDriverEntity> optionalCarDriverEntity = carDriverDao.find(id);
		Assertions.assertNotNull(optionalCarDriverEntity.get());
		Assertions.assertEquals(optionalCarDriverEntity.get().getId(), id);
	}

	@Test
	@DisplayName("Method update() - Should execute successful")
	public void updateSuccessful() {
		// Given
		CarEntity car = CarDaoTest.createNewCarEntity(1L, "model", "manufacturer", "2023", 0F);
		DriverEntity driver = DriverDaoTest.createNewDriverEntity(1L, "document", LocalDate.now());
		CarDriverEntity carDriverEntity = createNewCarDriverEntity(1L, car, driver, true);

		// When
		Mockito.when(this.carDriverRepository.existsById(carDriverEntity.getId()))
				.thenReturn(true);
		Mockito.when(this.carDao.find(car.getId()))
				.thenReturn(Optional.of(car));
		Mockito.when(this.driverDao.find(driver.getId()))
				.thenReturn(Optional.of(driver));
		Mockito.when(this.carDriverRepository.save(carDriverEntity))
				.thenReturn(carDriverEntity);

		// Then
		carDriverEntity = carDriverDao.update(carDriverEntity);
		Assertions.assertNotNull(carDriverEntity.getId());
		Assertions.assertNotNull(carDriverEntity.getCar().getModel());
		Assertions.assertNotNull(carDriverEntity.getDriver().getDocument());
	}

	@Test
	@DisplayName("Method update() - Without complement entity")
	public void updateWithoutComplement() {
		// Given
		CarEntity car = CarDaoTest.createNewCarEntity(1L, null, null, null, null);
		DriverEntity driver = DriverDaoTest.createNewDriverEntity(1L, null, null);
		CarDriverEntity carDriverEntity = createNewCarDriverEntity(1L, car, driver, true);

		// When
		Mockito.when(this.carDriverRepository.existsById(carDriverEntity.getId()))
				.thenReturn(true);
		Mockito.when(this.carDao.find(car.getId()))
				.thenReturn(Optional.ofNullable(null));
		Mockito.when(this.driverDao.find(driver.getId()))
				.thenReturn(Optional.ofNullable(null));
		Mockito.when(this.carDriverRepository.save(carDriverEntity))
				.thenReturn(carDriverEntity);

		// Then
		carDriverEntity = carDriverDao.update(carDriverEntity);
		Assertions.assertNotNull(carDriverEntity.getId());
		Assertions.assertNull(carDriverEntity.getCar().getModel());
		Assertions.assertNull(carDriverEntity.getDriver().getDocument());
	}

	@Test
	@DisplayName("Method update() - Should not found")
	public void updateNotFound() {
		// Given
		CarEntity car = CarDaoTest.createNewCarEntity(1L, "model", "manufacturer", "2023", 0F);
		DriverEntity driver = DriverDaoTest.createNewDriverEntity(1L, "document", LocalDate.now());
		CarDriverEntity carDriverEntity = createNewCarDriverEntity(1L, car, driver, true);

		// When
		Mockito.when(this.carDriverRepository.existsById(carDriverEntity.getId()))
				.thenReturn(false);

		// Then
		carDriverEntity = carDriverDao.update(carDriverEntity);
		Assertions.assertNull(carDriverEntity);
	}

	@Test
	@DisplayName("Method delete() - Should execute successful")
	public void deleteSuccessful() {
		// Given
		Long id = 1L;

		// Then
		Assertions.assertDoesNotThrow(() -> carDao.delete(id));
	}

	public static CarDriverEntity createNewCarDriverEntity(Long id, CarEntity car, DriverEntity driver, Boolean mainDriver) {
		return CarDriverEntity.builder()
				.id(id)
				.car(car)
				.driver(driver)
				.mainDriver(mainDriver)
				.build();
	}

}