# Documentação das APIs

Este projeto possui OpenAPI, caso queira apenas o OpenAPI pode baixar o arquivo [openapi.yaml](openapi.yaml "download"), segue abaixo detalhes de como acessar o Swagger

## Passo a Passo

1. Após subir o projeto na sua máquina (para mais detalhes veja o item [Execução do projeto](/doc/run/run.md)) entre no <a href="http://localhost:8080/swagger-ui/index.html#/">link</a>

2. Nessa página você terá acesso a toda a documentação das APIs

![swagger01.png](swagger01.png)

3. Caso queira acessar a documentação OpenAPI é só acessar o <a href="http://localhost:8080/api-docs">link</a>

![swagger02.png](swagger02.png)

[<< Voltar para página principal](/README.md)