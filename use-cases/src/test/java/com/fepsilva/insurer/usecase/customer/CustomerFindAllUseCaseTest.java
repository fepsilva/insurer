package com.fepsilva.insurer.usecase.customer;

import com.fepsilva.insurer.converter.CustomerModelConverterTest;
import com.fepsilva.insurer.converter.DriverModelConverterTest;
import com.fepsilva.insurer.dao.CustomerDao;
import com.fepsilva.insurer.entity.CustomerEntity;
import com.fepsilva.insurer.entity.DriverEntity;
import com.fepsilva.insurer.model.Customer;
import com.fepsilva.insurer.usecase.customer.impl.CustomerFindAllUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class CustomerFindAllUseCaseTest {

	@Mock
	private CustomerDao customerDao;

	@InjectMocks
	private CustomerFindAllUseCaseImpl customerFindAllUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void executeSuccessful() {
		// Given
		DriverEntity driver = DriverModelConverterTest.createNewDriverEntity(1L, "teste", LocalDate.now());

		List<CustomerEntity> listCustomerEntity = List.of(
				CustomerModelConverterTest.createNewCustomerEntity(1L, "teste1", driver),
				CustomerModelConverterTest.createNewCustomerEntity(2L, "teste2", driver),
				CustomerModelConverterTest.createNewCustomerEntity(3L, "teste3", driver));

		// When
		Mockito.when(this.customerDao.findAll())
				.thenReturn(listCustomerEntity);

		// Then
		List<Customer> listCustomer = this.customerFindAllUseCase.execute();
		Assertions.assertNotNull(listCustomer);
		Assertions.assertEquals(listCustomer.size(), 3);

		for (int i = 0; i < listCustomerEntity.size(); i++) {
			CustomerModelConverterTest.isEquals(listCustomerEntity.get(i), listCustomer.get(i));
		}
	}

	@Test
	@DisplayName("Method execute() - Empty")
	public void executeEmpty() {
		// When
		Mockito.when(this.customerDao.findAll())
				.thenReturn(null);

		// Then
		List<Customer> listCustomer = this.customerFindAllUseCase.execute();
		Assertions.assertNotNull(listCustomer);
		Assertions.assertEquals(listCustomer.size(), 0);
	}

}