package com.fepsilva.insurer.dto.view;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Visualização do cadastro de vínculo entre carro e motorista")
public class CarDriverView {

	@Schema(description = "Identificador do motorista do veiculo")
	private Long id;

	@Schema(description = "Carro")
	private CarView car;

	@Schema(description = "Motorista que será condutor do veículo")
	private DriverView driver;

	@Schema(description = "Flag que sinaliza se o motorista é o principal condutor do veículo")
	private Boolean mainDriver;

}