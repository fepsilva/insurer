package com.fepsilva.insurer.usecase.riskage.impl;

import com.fepsilva.insurer.dao.RiskAgeDao;
import com.fepsilva.insurer.usecase.riskage.RiskAgeDeleteUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RiskAgeDeleteUseCaseImpl implements RiskAgeDeleteUseCase {

	@Autowired
	private RiskAgeDao riskAgeDao;

	public void execute(Long riskAgeId) {
		this.riskAgeDao.delete(riskAgeId);
	}

}