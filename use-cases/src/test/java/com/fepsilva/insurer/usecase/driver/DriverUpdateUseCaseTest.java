package com.fepsilva.insurer.usecase.driver;

import com.fepsilva.insurer.converter.DriverModelConverter;
import com.fepsilva.insurer.converter.DriverModelConverterTest;
import com.fepsilva.insurer.dao.DriverDao;
import com.fepsilva.insurer.entity.DriverEntity;
import com.fepsilva.insurer.model.Driver;
import com.fepsilva.insurer.usecase.driver.impl.DriverUpdateUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

@ExtendWith(MockitoExtension.class)
public class DriverUpdateUseCaseTest {

	@Mock
	private DriverDao driverDao;

	@InjectMocks
	private DriverUpdateUseCaseImpl driverUpdateUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void executeSuccessful() {
		// Given
		Long driverId = 1L;
		Driver driver = DriverModelConverterTest.createNewDriver(driverId, "teste", LocalDate.now());
		DriverEntity driverEntity = DriverModelConverter.modelToEntity(driver);

		// When
		Mockito.when(this.driverDao.update(DriverModelConverter.modelToEntity(driver)))
				.thenReturn(driverEntity);

		// Then
		driver = this.driverUpdateUseCase.execute(driverId, driver);
		Assertions.assertNotNull(driver);
	}

	@Test
	@DisplayName("Method execute() - Not Found")
	public void executeNotFound() {
		// Given
		Long driverId = 1L;
		Driver driver = DriverModelConverterTest.createNewDriver(driverId, "teste", LocalDate.now());

		// When
		Mockito.when(this.driverDao.update(DriverModelConverter.modelToEntity(driver)))
				.thenReturn(null);

		// Then
		driver = this.driverUpdateUseCase.execute(driverId, driver);
		Assertions.assertNull(driver);
	}

}