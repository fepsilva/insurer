package com.fepsilva.insurer.usecase.car;

import com.fepsilva.insurer.converter.CarModelConverter;
import com.fepsilva.insurer.converter.CarModelConverterTest;
import com.fepsilva.insurer.dao.CarDao;
import com.fepsilva.insurer.entity.CarEntity;
import com.fepsilva.insurer.model.Car;
import com.fepsilva.insurer.usecase.car.impl.CarUpdateUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CarUpdateUseCaseTest {

	@Mock
	private CarDao carDao;

	@InjectMocks
	private CarUpdateUseCaseImpl carUpdateUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void executeSuccessful() {
		// Given
		Long carId = 1L;
		Car car = CarModelConverterTest.createNewCar(carId, "teste", "teste", "2023", 0F);
		CarEntity carEntity = CarModelConverter.modelToEntity(car);

		// When
		Mockito.when(this.carDao.update(CarModelConverter.modelToEntity(car)))
				.thenReturn(carEntity);

		// Then
		car = this.carUpdateUseCase.execute(carId, car);
		Assertions.assertNotNull(car);
	}

	@Test
	@DisplayName("Method execute() - Not Found")
	public void executeNotFound() {
		// Given
		Long carId = 1L;
		Car car = CarModelConverterTest.createNewCar(carId, "teste", "teste", "2023", 0F);

		// When
		Mockito.when(this.carDao.update(CarModelConverter.modelToEntity(car)))
				.thenReturn(null);

		// Then
		car = this.carUpdateUseCase.execute(carId, car);
		Assertions.assertNull(car);
	}

}