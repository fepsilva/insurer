package com.fepsilva.insurer.usecase.cardriver;

import com.fepsilva.insurer.dao.CarDriverDao;
import com.fepsilva.insurer.usecase.cardriver.impl.CarDriverDeleteUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CarDriverDeleteUseCaseTest {

	@Mock
	private CarDriverDao carDriverDao;

	@InjectMocks
	private CarDriverDeleteUseCaseImpl carDriverDeleteUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void execute() {
		// Given
		Long carDriverId = 1L;

		// Then
		Assertions.assertDoesNotThrow(() -> this.carDriverDeleteUseCase.execute(carDriverId));
	}

}