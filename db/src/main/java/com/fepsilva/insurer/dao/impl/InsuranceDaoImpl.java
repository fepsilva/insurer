package com.fepsilva.insurer.dao.impl;

import com.fepsilva.insurer.dao.CarDao;
import com.fepsilva.insurer.dao.CustomerDao;
import com.fepsilva.insurer.dao.InsuranceDao;
import com.fepsilva.insurer.entity.InsuranceEntity;
import com.fepsilva.insurer.repository.InsuranceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class InsuranceDaoImpl implements InsuranceDao {

	@Autowired
	private InsuranceRepository insuranceRepository;

	@Autowired
	private CustomerDao customerDao;

	@Autowired
	private CarDao carDao;

	public InsuranceEntity persist(InsuranceEntity insuranceEntity) {
		insuranceEntity = this.insuranceRepository.save(insuranceEntity);
		this.fillComplementEntity(insuranceEntity);

		return insuranceEntity;
	}

	public List<InsuranceEntity> findAll() {
		return this.insuranceRepository.findAll();
	}

	public Optional<InsuranceEntity> find(Long insuranceId) {
		return this.insuranceRepository.findById(insuranceId);
	}

	public InsuranceEntity update(InsuranceEntity insuranceEntity) {
		if (this.insuranceRepository.existsById(insuranceEntity.getId())) {
			insuranceEntity = this.insuranceRepository.save(insuranceEntity);
			this.fillComplementEntity(insuranceEntity);

			return insuranceEntity;
		}

		return null;
	}

	public void delete(Long insuranceId) {
		this.insuranceRepository.deleteById(insuranceId);
	}

	private void fillComplementEntity(InsuranceEntity insuranceEntity) {
		this.customerDao.find(insuranceEntity.getCustomer().getId())
				.ifPresent(customer -> insuranceEntity.setCustomer(customer));

		this.carDao.find(insuranceEntity.getCar().getId())
				.ifPresent(car -> insuranceEntity.setCar(car));
	}

}