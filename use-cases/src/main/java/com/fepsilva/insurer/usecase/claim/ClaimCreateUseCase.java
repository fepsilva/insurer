package com.fepsilva.insurer.usecase.claim;

import com.fepsilva.insurer.model.Claim;

public interface ClaimCreateUseCase {

	public Claim execute(Claim carDriver);

}