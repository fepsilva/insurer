package com.fepsilva.insurer.converter;

import com.fepsilva.insurer.dto.form.DriverForm;
import com.fepsilva.insurer.dto.view.DriverView;
import com.fepsilva.insurer.model.Driver;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

@ExtendWith(MockitoExtension.class)
public class DriverDtoConverterTest {

	@Test
	@DisplayName("Method idToEntity() - Should execute successful")
	public void idToEntitySuccessful() {
		// Given
		Long driverId = 1L;

		// Then
		Driver driver = DriverDtoConverter.idToEntity(driverId);
		Assertions.assertNotNull(driver);
		Assertions.assertEquals(driver.getId(), driverId);
	}

	@Test
	@DisplayName("Method dtoToEntity() - Should execute successful")
	public void dtoToEntitySuccessful() {
		// Given
		DriverForm driverForm = createNewDriverForm("teste", LocalDate.now());

		// Then
		Driver driver = DriverDtoConverter.dtoToEntity(driverForm);
		isEquals(driver, driverForm);
	}

	@Test
	@DisplayName("Method dtoToEntity() - Without value")
	public void dtoToEntityWithoutValue() {
		// Given
		DriverForm driverForm = null;

		// Then
		Driver driver = DriverDtoConverter.dtoToEntity(driverForm);
		Assertions.assertNull(driver);
	}

	@Test
	@DisplayName("Method entityToDto() - Should execute successful")
	public void entityToDtoSuccessful() {
		// Given
		Driver driver = createNewDriver(1L, "teste", LocalDate.now());

		// Then
		DriverView driverView = DriverDtoConverter.entityToDto(driver);
		isEquals(driverView, driver);
	}

	@Test
	@DisplayName("Method entityToModel() - Without value")
	public void entityToModelWithoutValue() {
		// Given
		Driver driver = null;

		// Then
		DriverView driverView = DriverDtoConverter.entityToDto(driver);
		Assertions.assertNull(driverView);
	}

	public static DriverForm createNewDriverForm(String document, LocalDate birthDate) {
		return DriverForm.builder()
				.document(document)
				.birthDate(birthDate)
				.build();
	}

	public static Driver createNewDriver(Long id, String document, LocalDate birthDate) {
		return Driver.builder()
				.id(id)
				.document(document)
				.birthDate(birthDate)
				.build();
	}

	public static void isEquals(Driver driver, DriverForm driverForm) {
		Assertions.assertNotNull(driver);
		Assertions.assertNotNull(driverForm);

		Assertions.assertEquals(driver.getDocument(), driverForm.getDocument());
		Assertions.assertEquals(driver.getBirthDate(), driverForm.getBirthDate());
	}

	public static void isEquals(DriverView driverView, Driver driver) {
		Assertions.assertNotNull(driverView);
		Assertions.assertNotNull(driver);

		Assertions.assertEquals(driverView.getId(), driver.getId());
		Assertions.assertEquals(driverView.getDocument(), driver.getDocument());
		Assertions.assertEquals(driverView.getBirthDate(), driver.getBirthDate());
	}

}