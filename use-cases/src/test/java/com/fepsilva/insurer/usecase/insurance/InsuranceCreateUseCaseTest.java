package com.fepsilva.insurer.usecase.insurance;

import com.fepsilva.insurer.converter.CarModelConverterTest;
import com.fepsilva.insurer.converter.CustomerModelConverterTest;
import com.fepsilva.insurer.converter.DriverModelConverterTest;
import com.fepsilva.insurer.converter.InsuranceModelConverter;
import com.fepsilva.insurer.converter.InsuranceModelConverterTest;
import com.fepsilva.insurer.dao.InsuranceDao;
import com.fepsilva.insurer.entity.InsuranceEntity;
import com.fepsilva.insurer.model.Car;
import com.fepsilva.insurer.model.Customer;
import com.fepsilva.insurer.model.Driver;
import com.fepsilva.insurer.model.Insurance;
import com.fepsilva.insurer.usecase.car.CarFindUseCase;
import com.fepsilva.insurer.usecase.insurance.impl.InsuranceCreateUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalDateTime;

@ExtendWith(MockitoExtension.class)
public class InsuranceCreateUseCaseTest {

	@Mock
	private InsuranceBudgetCalculationUseCase insuranceBudgetCalculationUseCase;

	@Mock
	private CarFindUseCase carFindUseCase;

	@Mock
	private InsuranceDao insuranceDao;

	@InjectMocks
	private InsuranceCreateUseCaseImpl insuranceCreateUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void executeSuccessful() {
		// Given
		Float percentRisk = 0.0F;

		Car car = CarModelConverterTest.createNewCar(1L, "teste", "teste", "2023", 0F);
		Driver driver = DriverModelConverterTest.createNewDriver(1L, "teste", LocalDate.now());
		Customer customer = CustomerModelConverterTest.createNewCustomer(1L, "teste", driver);
		Insurance insurance = InsuranceModelConverterTest.createNewInsurance(null, customer, LocalDateTime.now(), LocalDateTime.now(), car, true, percentRisk, 0F);

		InsuranceEntity insuranceEntity = InsuranceModelConverter.modelToEntity(insurance);
		insuranceEntity.setId(1L);

		// When
		Mockito.when(this.insuranceBudgetCalculationUseCase.execute(insurance))
				.thenReturn(percentRisk);
		Mockito.when(this.carFindUseCase.execute(insurance.getCar().getId()))
				.thenReturn(car);
		Mockito.when(this.insuranceDao.persist(Mockito.any(InsuranceEntity.class)))
				.thenReturn(insuranceEntity);

		// Then
		insurance = this.insuranceCreateUseCase.execute(insurance);
		Assertions.assertNotNull(insurance);
		Assertions.assertNotNull(insurance.getId());
	}

	@Test
	@DisplayName("Method execute() - Should return null")
	public void executeNull() {
		// Given
		Float percentRisk = 0.0F;

		Car car = CarModelConverterTest.createNewCar(1L, "teste", "teste", "2023", 0F);
		Driver driver = DriverModelConverterTest.createNewDriver(1L, "teste", LocalDate.now());
		Customer customer = CustomerModelConverterTest.createNewCustomer(1L, "teste", driver);
		Insurance insurance = InsuranceModelConverterTest.createNewInsurance(null, customer, LocalDateTime.now(), LocalDateTime.now(), car, true, percentRisk, 0F);

		// When
		Mockito.when(this.insuranceBudgetCalculationUseCase.execute(insurance))
				.thenReturn(percentRisk);
		Mockito.when(this.carFindUseCase.execute(insurance.getCar().getId()))
				.thenReturn(car);
		Mockito.when(this.insuranceDao.persist(Mockito.any(InsuranceEntity.class)))
				.thenReturn(null);

		// Then
		insurance = this.insuranceCreateUseCase.execute(insurance);
		Assertions.assertNull(insurance);
	}

}