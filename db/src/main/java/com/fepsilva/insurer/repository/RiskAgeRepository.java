package com.fepsilva.insurer.repository;

import com.fepsilva.insurer.entity.RiskAgeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RiskAgeRepository extends JpaRepository<RiskAgeEntity, Long> {

}