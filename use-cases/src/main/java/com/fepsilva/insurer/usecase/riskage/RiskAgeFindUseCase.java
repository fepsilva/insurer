package com.fepsilva.insurer.usecase.riskage;

import com.fepsilva.insurer.model.RiskAge;

public interface RiskAgeFindUseCase {

	public RiskAge execute(Long riskAgeId);

}