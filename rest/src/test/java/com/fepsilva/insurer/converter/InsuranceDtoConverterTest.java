package com.fepsilva.insurer.converter;

import com.fepsilva.insurer.dto.form.InsuranceForm;
import com.fepsilva.insurer.dto.view.InsuranceView;
import com.fepsilva.insurer.model.Car;
import com.fepsilva.insurer.model.Customer;
import com.fepsilva.insurer.model.Driver;
import com.fepsilva.insurer.model.Insurance;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalDateTime;

@ExtendWith(MockitoExtension.class)
public class InsuranceDtoConverterTest {

	@Test
	@DisplayName("Method dtoToEntity() - Should execute successful")
	public void dtoToEntitySuccessful() {
		// Given
		InsuranceForm insuranceForm = createNewInsuranceForm(1L, 1L, true);

		// Then
		Insurance insurance = InsuranceDtoConverter.dtoToEntity(insuranceForm);
		isEquals(insurance, insuranceForm);
	}

	@Test
	@DisplayName("Method dtoToEntity() - Without value")
	public void dtoToEntityWithoutValue() {
		// Given
		InsuranceForm insuranceForm = null;

		// Then
		Insurance insurance = InsuranceDtoConverter.dtoToEntity(insuranceForm);
		Assertions.assertNull(insurance);
	}

	@Test
	@DisplayName("Method entityToDto() - Should execute successful")
	public void entityToDtoSuccessful() {
		// Given
		Car car = CarDtoConverterTest.createNewCar(1L, "teste", "teste", "2023", 0F);
		Driver driver = DriverDtoConverterTest.createNewDriver(1L, "teste", LocalDate.now());
		Customer customer = CustomerDtoConverterTest.createNewCustomer(1L, "teste", driver);
		Insurance insurance = createNewInsurance(1L, customer, LocalDateTime.now(), LocalDateTime.now(), car, true, 0F, 0F);

		// Then
		InsuranceView insuranceView = InsuranceDtoConverter.entityToDto(insurance);
		isEquals(insuranceView, insurance);
	}

	@Test
	@DisplayName("Method entityToModel() - Without value")
	public void entityToModelWithoutValue() {
		// Given
		Insurance insurance = null;

		// Then
		InsuranceView insuranceView = InsuranceDtoConverter.entityToDto(insurance);
		Assertions.assertNull(insuranceView);
	}

	public static InsuranceForm createNewInsuranceForm(Long carId, Long customerId, Boolean active) {
		return InsuranceForm.builder()
				.customerId(customerId)
				.carId(carId)
				.active(active)
				.build();
	}

	public static Insurance createNewInsurance(Long id, Customer customer, LocalDateTime creationDt, LocalDateTime updatedDt, Car car, Boolean active, Float percentRisk, Float fipeValue) {
		return Insurance.builder()
				.id(id)
				.customer(customer)
				.creationDt(creationDt)
				.updatedDt(updatedDt)
				.car(car)
				.active(active)
				.percentRisk(percentRisk)
				.fipeValue(fipeValue)
				.build();
	}

	public static void isEquals(Insurance insurance, InsuranceForm insuranceForm) {
		Assertions.assertNotNull(insurance);
		Assertions.assertNotNull(insuranceForm);

		Assertions.assertEquals(insurance.getActive(), insuranceForm.getActive());
		Assertions.assertEquals(insurance.getCustomer().getId(), insuranceForm.getCustomerId());
		Assertions.assertEquals(insurance.getCar().getId(), insuranceForm.getCarId());
	}

	public static void isEquals(InsuranceView insuranceView, Insurance insurance) {
		Assertions.assertNotNull(insuranceView);
		Assertions.assertNotNull(insurance);

		Assertions.assertEquals(insuranceView.getId(), insurance.getId());
		Assertions.assertEquals(insuranceView.getCreationDt(), insurance.getCreationDt());
		Assertions.assertEquals(insuranceView.getUpdatedDt(), insurance.getUpdatedDt());
		Assertions.assertEquals(insuranceView.getActive(), insurance.getActive());
		Assertions.assertEquals(insuranceView.getPercentRisk(), insurance.getPercentRisk());
		Assertions.assertEquals(insuranceView.getFipeValue(), insurance.getFipeValue());

		CustomerDtoConverterTest.isEquals(insuranceView.getCustomer(), insurance.getCustomer());
		CarDtoConverterTest.isEquals(insuranceView.getCar(), insurance.getCar());
	}

}