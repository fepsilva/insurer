package com.fepsilva.insurer.controller;

import com.fepsilva.insurer.converter.RiskAgeDtoConverter;
import com.fepsilva.insurer.dto.form.RiskAgeForm;
import com.fepsilva.insurer.dto.view.RiskAgeView;
import com.fepsilva.insurer.usecase.riskage.RiskAgeCreateUseCase;
import com.fepsilva.insurer.usecase.riskage.RiskAgeDeleteUseCase;
import com.fepsilva.insurer.usecase.riskage.RiskAgeFindAllUseCase;
import com.fepsilva.insurer.usecase.riskage.RiskAgeFindUseCase;
import com.fepsilva.insurer.usecase.riskage.RiskAgeUpdateUseCase;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Validated
@RestController
@RequestMapping("/risk/age")
@Tag(name = "API de Risco por idade", description = "Endpoints responsáveis pela gestão do cadastro de riscos por idade")
public class RiskAgeController {

	@Autowired
	private RiskAgeCreateUseCase riskAgeCreateUseCase;

	@Autowired
	private RiskAgeFindAllUseCase riskAgeFindAllUseCase;

	@Autowired
	private RiskAgeFindUseCase riskAgeFindUseCase;

	@Autowired
	private RiskAgeUpdateUseCase riskAgeUpdateUseCase;

	@Autowired
	private RiskAgeDeleteUseCase riskAgeDeleteUseCase;

	@Operation(summary = "Endpoint para criar um novo risco por idade")
	@ApiResponse(responseCode = "200", description = "Risco por idade cadastrado")
	@PostMapping(consumes = "application/json", produces = "application/json")
	public ResponseEntity<RiskAgeView> create(@Valid @RequestBody RiskAgeForm riskAgeForm) {
		try {
			return Optional.ofNullable(this.riskAgeCreateUseCase.execute(RiskAgeDtoConverter.dtoToEntity(riskAgeForm)))
					.map(RiskAgeDtoConverter::entityToDto)
					.map(ResponseEntity::ok)
					.get();
		} catch (DataIntegrityViolationException e) {
			log.error("Dados inválidos", e);
			return ResponseEntity.badRequest().build();
		} catch (Exception e) {
			log.error("Erro ao cadastrar risco por idade", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@Operation(summary = "Endpoint para consultar todos os riscos por idade")
	@ApiResponse(responseCode = "200", description = "Risco por idades consultados")
	@GetMapping
	public ResponseEntity<List<RiskAgeView>> findAll() {
		try {
			return ResponseEntity.ok(this.riskAgeFindAllUseCase.execute().stream()
					.map(RiskAgeDtoConverter::entityToDto)
					.collect(Collectors.toList()));
		} catch (Exception e) {
			log.error("Erro ao procurar risco por idade", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@Operation(summary = "Endpoint para consultar um risco por idade")
	@ApiResponse(responseCode = "200", description = "Risco por idade consultado")
	@ApiResponse(responseCode = "204", description = "Risco por idade não encontrado")
	@GetMapping("/{riskAgeId}")
	public ResponseEntity<RiskAgeView> find(@Parameter(description = "Id do Risco por Idade") @PathVariable Long riskAgeId) {
		try {
			return Optional.ofNullable(this.riskAgeFindUseCase.execute(riskAgeId))
					.map(RiskAgeDtoConverter::entityToDto)
					.map(ResponseEntity::ok)
					.orElseGet(() -> ResponseEntity.noContent().build());
		} catch (Exception e) {
			log.error("Erro ao procurar risco por idade", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@Operation(summary = "Endpoint para alterar um risco por idade")
	@ApiResponse(responseCode = "200", description = "Risco por idade alterado")
	@ApiResponse(responseCode = "204", description = "Risco por idade não encontrado")
	@PutMapping("/{riskAgeId}")
	public ResponseEntity<RiskAgeView> update(
			@Parameter(description = "Id do Risco por Idade") @PathVariable Long riskAgeId,
			@Valid @RequestBody RiskAgeForm riskAgeForm) {
		try {
			return Optional.ofNullable(this.riskAgeUpdateUseCase.execute(riskAgeId, RiskAgeDtoConverter.dtoToEntity(riskAgeForm)))
					.map(RiskAgeDtoConverter::entityToDto)
					.map(ResponseEntity::ok)
					.orElseGet(() -> ResponseEntity.noContent().build());
		} catch (DataIntegrityViolationException e) {
			log.error("Dados inválidos", e);
			return ResponseEntity.badRequest().build();
		} catch (Exception e) {
			log.error("Erro ao alterar risco por idade", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@Operation(summary = "Endpoint para remover um risco por idade")
	@ApiResponse(responseCode = "200", description = "Risco por idade removido")
	@ApiResponse(responseCode = "204", description = "Risco por idade não encontrado")
	@DeleteMapping("/{riskAgeId}")
	public ResponseEntity delete(@Parameter(description = "Id do Risco por Idade") @PathVariable Long riskAgeId) {
		try {
			this.riskAgeDeleteUseCase.execute(riskAgeId);
			return ResponseEntity.ok().build();
		} catch (EmptyResultDataAccessException e) {
			log.info("Risco por idade não encontrado");
			return ResponseEntity.noContent().build();
		} catch (Exception e) {
			log.error("Erro ao remover risco por idade", e);
			return ResponseEntity.internalServerError().build();
		}
	}

}