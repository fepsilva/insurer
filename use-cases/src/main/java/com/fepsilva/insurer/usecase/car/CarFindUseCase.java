package com.fepsilva.insurer.usecase.car;

import com.fepsilva.insurer.model.Car;

public interface CarFindUseCase {

	public Car execute(Long carId);

}