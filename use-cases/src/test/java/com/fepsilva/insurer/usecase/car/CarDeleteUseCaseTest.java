package com.fepsilva.insurer.usecase.car;

import com.fepsilva.insurer.dao.CarDao;
import com.fepsilva.insurer.usecase.car.impl.CarDeleteUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CarDeleteUseCaseTest {

	@Mock
	private CarDao carDao;

	@InjectMocks
	private CarDeleteUseCaseImpl carDeleteUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void execute() {
		// Given
		Long carId = 1L;

		// Then
		Assertions.assertDoesNotThrow(() -> this.carDeleteUseCase.execute(carId));
	}

}