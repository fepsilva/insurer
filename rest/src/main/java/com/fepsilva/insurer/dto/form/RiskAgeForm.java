package com.fepsilva.insurer.dto.form;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Formulário do cadastro de risco por idade")
public class RiskAgeForm {

	@NotNull(message = "Não pode estar vazio")
	@Schema(description = "Idade inicial (será considerada essa idade e para cima)")
	private Long beginAge;

	@NotNull(message = "Não pode estar vazio")
	@Schema(description = "Idade inicial (será considerada essa idade e para baixo)")
	private Long endAge;

	@NotNull(message = "Não pode estar vazio")
	@Schema(description = "Porcentagem cobrada baseado no risco")
	private BigDecimal percentRisk;

}