package com.fepsilva.insurer.usecase.driver;

import com.fepsilva.insurer.converter.DriverModelConverterTest;
import com.fepsilva.insurer.dao.DriverDao;
import com.fepsilva.insurer.entity.DriverEntity;
import com.fepsilva.insurer.model.Driver;
import com.fepsilva.insurer.usecase.driver.impl.DriverFindAllUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class DriverFindAllUseCaseTest {

	@Mock
	private DriverDao driverDao;

	@InjectMocks
	private DriverFindAllUseCaseImpl driverFindAllUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void executeSuccessful() {
		// Given
		List<DriverEntity> listDriverEntity = List.of(
				DriverModelConverterTest.createNewDriverEntity(1L, "teste1", LocalDate.now()),
				DriverModelConverterTest.createNewDriverEntity(2L, "teste2", LocalDate.now()),
				DriverModelConverterTest.createNewDriverEntity(3L, "teste3", LocalDate.now()));

		// When
		Mockito.when(this.driverDao.findAll())
				.thenReturn(listDriverEntity);

		// Then
		List<Driver> listDriver = this.driverFindAllUseCase.execute();
		Assertions.assertNotNull(listDriver);
		Assertions.assertEquals(listDriver.size(), 3);

		for (int i = 0; i < listDriverEntity.size(); i++) {
			DriverModelConverterTest.isEquals(listDriverEntity.get(i), listDriver.get(i));
		}
	}

	@Test
	@DisplayName("Method execute() - Empty")
	public void executeEmpty() {
		// When
		Mockito.when(this.driverDao.findAll())
				.thenReturn(null);

		// Then
		List<Driver> listDriver = this.driverFindAllUseCase.execute();
		Assertions.assertNotNull(listDriver);
		Assertions.assertEquals(listDriver.size(), 0);
	}

}