package com.fepsilva.insurer.usecase.riskage.impl;

import com.fepsilva.insurer.converter.RiskAgeModelConverter;
import com.fepsilva.insurer.dao.RiskAgeDao;
import com.fepsilva.insurer.model.RiskAge;
import com.fepsilva.insurer.usecase.riskage.RiskAgeUpdateUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RiskAgeUpdateUseCaseImpl implements RiskAgeUpdateUseCase {

	@Autowired
	private RiskAgeDao riskAgeDao;

	public RiskAge execute(Long riskAgeId, RiskAge riskAge) {
		riskAge.setId(riskAgeId);

		return Optional.ofNullable(this.riskAgeDao.update(RiskAgeModelConverter.modelToEntity(riskAge)))
				.map(RiskAgeModelConverter::entityToModel)
				.orElseGet(() -> null);
	}

}