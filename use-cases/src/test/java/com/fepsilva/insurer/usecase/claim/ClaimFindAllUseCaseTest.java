package com.fepsilva.insurer.usecase.claim;

import com.fepsilva.insurer.converter.CarModelConverterTest;
import com.fepsilva.insurer.converter.ClaimModelConverterTest;
import com.fepsilva.insurer.converter.DriverModelConverterTest;
import com.fepsilva.insurer.dao.ClaimDao;
import com.fepsilva.insurer.entity.CarEntity;
import com.fepsilva.insurer.entity.ClaimEntity;
import com.fepsilva.insurer.entity.DriverEntity;
import com.fepsilva.insurer.model.Claim;
import com.fepsilva.insurer.usecase.claim.impl.ClaimFindAllUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class ClaimFindAllUseCaseTest {

	@Mock
	private ClaimDao claimDao;

	@InjectMocks
	private ClaimFindAllUseCaseImpl claimFindAllUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void executeSuccessful() {
		// Given
		CarEntity car = CarModelConverterTest.createNewCarEntity(1L, "teste", "teste", "2023", 0F);
		DriverEntity driver = DriverModelConverterTest.createNewDriverEntity(1L, "teste", LocalDate.now());

		List<ClaimEntity> listClaimEntity = List.of(
				ClaimModelConverterTest.createNewClaimEntity(1L, car, driver, LocalDate.now()),
				ClaimModelConverterTest.createNewClaimEntity(2L, car, driver, LocalDate.now()),
				ClaimModelConverterTest.createNewClaimEntity(3L, car, driver, LocalDate.now()));

		// When
		Mockito.when(this.claimDao.findAll())
				.thenReturn(listClaimEntity);

		// Then
		List<Claim> listClaim = this.claimFindAllUseCase.execute();
		Assertions.assertNotNull(listClaim);
		Assertions.assertEquals(listClaim.size(), 3);

		for (int i = 0; i < listClaimEntity.size(); i++) {
			ClaimModelConverterTest.isEquals(listClaimEntity.get(i), listClaim.get(i));
		}
	}

	@Test
	@DisplayName("Method execute() - Empty")
	public void executeEmpty() {
		// When
		Mockito.when(this.claimDao.findAll())
				.thenReturn(null);

		// Then
		List<Claim> listClaim = this.claimFindAllUseCase.execute();
		Assertions.assertNotNull(listClaim);
		Assertions.assertEquals(listClaim.size(), 0);
	}

}