package com.fepsilva.insurer.usecase.car;

import com.fepsilva.insurer.model.Car;

public interface CarCreateUseCase {

	public Car execute(Car car);

}