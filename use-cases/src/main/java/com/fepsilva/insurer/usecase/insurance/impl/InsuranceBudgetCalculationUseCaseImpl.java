package com.fepsilva.insurer.usecase.insurance.impl;

import com.fepsilva.insurer.model.Insurance;
import com.fepsilva.insurer.usecase.claim.ClaimCarBudgetCalculationUseCase;
import com.fepsilva.insurer.usecase.claim.ClaimDriverBudgetCalculationUseCase;
import com.fepsilva.insurer.usecase.insurance.InsuranceBudgetCalculationUseCase;
import com.fepsilva.insurer.usecase.riskage.RiskAgeBudgetCalculationUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class InsuranceBudgetCalculationUseCaseImpl implements InsuranceBudgetCalculationUseCase {

	private static final BigDecimal PERCENT_FIXED = BigDecimal.valueOf(0.06);

	@Autowired
	private ClaimDriverBudgetCalculationUseCase claimBudgetCalculationUseCase;

	@Autowired
	private ClaimCarBudgetCalculationUseCase claimCarBudgetCalculationUseCase;

	@Autowired
	private RiskAgeBudgetCalculationUseCase riskAgeBudgetCalculationUseCase;

	public Float execute(Insurance insurance) {
		return PERCENT_FIXED
				.add(this.claimBudgetCalculationUseCase.execute(insurance))
				.add(this.claimCarBudgetCalculationUseCase.execute(insurance))
				.add(this.riskAgeBudgetCalculationUseCase.execute(insurance))
				.floatValue();
	}

}