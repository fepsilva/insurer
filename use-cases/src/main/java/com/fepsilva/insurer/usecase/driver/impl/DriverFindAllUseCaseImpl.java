package com.fepsilva.insurer.usecase.driver.impl;

import com.fepsilva.insurer.converter.DriverModelConverter;
import com.fepsilva.insurer.dao.DriverDao;
import com.fepsilva.insurer.model.Driver;
import com.fepsilva.insurer.usecase.driver.DriverFindAllUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DriverFindAllUseCaseImpl implements DriverFindAllUseCase {

	@Autowired
	private DriverDao driverDao;

	public List<Driver> execute() {
		return Optional.ofNullable(this.driverDao.findAll())
				.orElseGet(ArrayList::new)
				.stream().map(DriverModelConverter::entityToModel)
				.collect(Collectors.toList());
	}

}