package com.fepsilva.insurer.usecase.customer.impl;

import com.fepsilva.insurer.converter.CustomerModelConverter;
import com.fepsilva.insurer.dao.CustomerDao;
import com.fepsilva.insurer.model.Customer;
import com.fepsilva.insurer.usecase.customer.CustomerCreateUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerCreateUseCaseImpl implements CustomerCreateUseCase {

	@Autowired
	private CustomerDao customerDao;

	public Customer execute(Customer customer) {
		return Optional.ofNullable(this.customerDao.persist(CustomerModelConverter.modelToEntity(customer)))
				.map(CustomerModelConverter::entityToModel)
				.orElseGet(() -> null);
	}

}