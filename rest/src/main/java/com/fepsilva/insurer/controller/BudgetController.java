package com.fepsilva.insurer.controller;

import com.fepsilva.insurer.converter.InsuranceDtoConverter;
import com.fepsilva.insurer.dto.form.InsuranceForm;
import com.fepsilva.insurer.dto.view.InsuranceView;
import com.fepsilva.insurer.usecase.insurance.InsuranceCreateUseCase;
import com.fepsilva.insurer.usecase.insurance.InsuranceDeleteUseCase;
import com.fepsilva.insurer.usecase.insurance.InsuranceFindAllUseCase;
import com.fepsilva.insurer.usecase.insurance.InsuranceFindUseCase;
import com.fepsilva.insurer.usecase.insurance.InsuranceUpdateUseCase;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Validated
@RestController
@RequestMapping("/insurance/budget")
@Tag(name = "API de Orçamento", description = "Endpoints responsáveis pela gestão de orçamentos")
public class BudgetController {

	@Autowired
	private InsuranceCreateUseCase insuranceCreateUseCase;

	@Autowired
	private InsuranceFindAllUseCase insuranceFindAllUseCase;

	@Autowired
	private InsuranceFindUseCase insuranceFindUseCase;

	@Autowired
	private InsuranceUpdateUseCase insuranceUpdateUseCase;

	@Autowired
	private InsuranceDeleteUseCase insuranceDeleteUseCase;

	@Operation(summary = "Endpoint para solicitar um novo orçamento")
	@ApiResponse(responseCode = "200", description = "Orçamento cadastrado")
	@PostMapping(consumes = "application/json", produces = "application/json")
	public ResponseEntity<InsuranceView> create(@Valid @RequestBody InsuranceForm insuranceForm) {
		try {
			return Optional.ofNullable(this.insuranceCreateUseCase.execute(InsuranceDtoConverter.dtoToEntity(insuranceForm)))
					.map(InsuranceDtoConverter::entityToDto)
					.map(ResponseEntity::ok)
					.get();
		} catch (DataIntegrityViolationException e) {
			log.error("Dados inválidos", e);
			return ResponseEntity.badRequest().build();
		} catch (Exception e) {
			log.error("Erro ao cadastrar orçamento", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@Operation(summary = "Endpoint para consultar todos os orçamentos")
	@ApiResponse(responseCode = "200", description = "Orçamentos consultados")
	@GetMapping
	public ResponseEntity<List<InsuranceView>> findAll() {
		try {
			return ResponseEntity.ok(this.insuranceFindAllUseCase.execute().stream()
					.map(InsuranceDtoConverter::entityToDto)
					.collect(Collectors.toList()));
		} catch (Exception e) {
			log.error("Erro ao procurar orçamento", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@Operation(summary = "Endpoint para consultar um orçamento")
	@ApiResponse(responseCode = "200", description = "Orçamento consultado")
	@ApiResponse(responseCode = "204", description = "Orçamento não encontrado")
	@GetMapping("/{insuranceId}")
	public ResponseEntity<InsuranceView> find(@Parameter(description = "Id do Orçamento") @PathVariable Long insuranceId) {
		try {
			return Optional.ofNullable(this.insuranceFindUseCase.execute(insuranceId))
					.map(InsuranceDtoConverter::entityToDto)
					.map(ResponseEntity::ok)
					.orElseGet(() -> ResponseEntity.noContent().build());
		} catch (Exception e) {
			log.error("Erro ao procurar orçamento", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@Operation(summary = "Endpoint para alterar um orçamento")
	@ApiResponse(responseCode = "200", description = "Orçamento alterado")
	@ApiResponse(responseCode = "204", description = "Orçamento não encontrado")
	@PutMapping("/{insuranceId}")
	public ResponseEntity<InsuranceView> update(
			@Parameter(description = "Id do Orçamento") @PathVariable Long insuranceId,
			@Valid @RequestBody InsuranceForm insuranceForm) {
		try {
			return Optional.ofNullable(this.insuranceUpdateUseCase.execute(insuranceId, InsuranceDtoConverter.dtoToEntity(insuranceForm)))
					.map(InsuranceDtoConverter::entityToDto)
					.map(ResponseEntity::ok)
					.orElseGet(() -> ResponseEntity.noContent().build());
		} catch (DataIntegrityViolationException e) {
			log.error("Dados inválidos", e);
			return ResponseEntity.badRequest().build();
		} catch (Exception e) {
			log.error("Erro ao alterar orçamento", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@Operation(summary = "Endpoint para remover um orçamento")
	@ApiResponse(responseCode = "200", description = "Orçamento removido")
	@ApiResponse(responseCode = "204", description = "Orçamento não encontrado")
	@DeleteMapping("/{insuranceId}")
	public ResponseEntity delete(@Parameter(description = "Id do Orçamento") @PathVariable Long insuranceId) {
		try {
			this.insuranceDeleteUseCase.execute(insuranceId);
			return ResponseEntity.ok().build();
		} catch (EmptyResultDataAccessException e) {
			log.info("Orçamento não encontrado");
			return ResponseEntity.noContent().build();
		} catch (Exception e) {
			log.error("Erro ao remover orçamento", e);
			return ResponseEntity.internalServerError().build();
		}
	}

}