package com.fepsilva.insurer.dto.form;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Formulário de cadastro de cliente")
public class CustomerForm {

	@NotNull(message = "Não pode estar vazio")
	@Schema(description = "Nome do cliente")
	private String name;

	@NotNull(message = "Não pode estar vazio")
	@Schema(description = "Identificador dos dados de motorista")
	private Long driverId;

}