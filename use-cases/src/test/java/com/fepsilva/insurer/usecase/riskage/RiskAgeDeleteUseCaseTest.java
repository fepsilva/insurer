package com.fepsilva.insurer.usecase.riskage;

import com.fepsilva.insurer.dao.RiskAgeDao;
import com.fepsilva.insurer.usecase.riskage.impl.RiskAgeDeleteUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class RiskAgeDeleteUseCaseTest {

	@Mock
	private RiskAgeDao riskAgeDao;

	@InjectMocks
	private RiskAgeDeleteUseCaseImpl riskAgeDeleteUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void execute() {
		// Given
		Long riskAgeId = 1L;

		// Then
		Assertions.assertDoesNotThrow(() -> this.riskAgeDeleteUseCase.execute(riskAgeId));
	}

}