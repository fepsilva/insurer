package com.fepsilva.insurer.usecase.insurance;

import com.fepsilva.insurer.model.Insurance;

public interface InsuranceFindUseCase {

	public Insurance execute(Long insuranceId);

}