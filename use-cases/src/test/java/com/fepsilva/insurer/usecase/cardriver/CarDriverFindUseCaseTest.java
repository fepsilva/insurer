package com.fepsilva.insurer.usecase.cardriver;

import com.fepsilva.insurer.converter.CarDriverModelConverterTest;
import com.fepsilva.insurer.converter.CarModelConverterTest;
import com.fepsilva.insurer.converter.DriverModelConverterTest;
import com.fepsilva.insurer.dao.CarDriverDao;
import com.fepsilva.insurer.entity.CarDriverEntity;
import com.fepsilva.insurer.entity.CarEntity;
import com.fepsilva.insurer.entity.DriverEntity;
import com.fepsilva.insurer.model.CarDriver;
import com.fepsilva.insurer.usecase.cardriver.impl.CarDriverFindUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class CarDriverFindUseCaseTest {

	@Mock
	private CarDriverDao carDriverDao;

	@InjectMocks
	private CarDriverFindUseCaseImpl carDriverFindUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void executeSuccessful() {
		// Given
		Long carDriverId = 1L;
		CarEntity car = CarModelConverterTest.createNewCarEntity(1L, "teste", "teste", "2023", 0F);
		DriverEntity driver = DriverModelConverterTest.createNewDriverEntity(1L, "teste", LocalDate.now());
		CarDriverEntity carDriverEntity = CarDriverModelConverterTest.createNewCarDriverEntity(carDriverId, car, driver, true);

		// When
		Mockito.when(this.carDriverDao.find(carDriverId))
				.thenReturn(Optional.ofNullable(carDriverEntity));

		// Then
		CarDriver carDriver = this.carDriverFindUseCase.execute(carDriverId);
		Assertions.assertNotNull(carDriver);

		CarDriverModelConverterTest.isEquals(carDriverEntity, carDriver);
	}

	@Test
	@DisplayName("Method execute() - Not Found")
	public void executeNotFound() {
		// Given
		Long carDriverId = 1L;

		// When
		Mockito.when(this.carDriverDao.find(carDriverId))
				.thenReturn(Optional.ofNullable(null));

		// Then
		CarDriver carDriver = this.carDriverFindUseCase.execute(carDriverId);
		Assertions.assertNull(carDriver);
	}

}