package com.fepsilva.insurer.repository;

import com.fepsilva.insurer.entity.ClaimEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClaimRepository extends JpaRepository<ClaimEntity, Long> {

}