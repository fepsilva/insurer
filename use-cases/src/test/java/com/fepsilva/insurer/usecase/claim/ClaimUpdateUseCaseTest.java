package com.fepsilva.insurer.usecase.claim;

import com.fepsilva.insurer.converter.CarModelConverterTest;
import com.fepsilva.insurer.converter.ClaimModelConverter;
import com.fepsilva.insurer.converter.ClaimModelConverterTest;
import com.fepsilva.insurer.converter.DriverModelConverterTest;
import com.fepsilva.insurer.dao.ClaimDao;
import com.fepsilva.insurer.entity.ClaimEntity;
import com.fepsilva.insurer.model.Car;
import com.fepsilva.insurer.model.Claim;
import com.fepsilva.insurer.model.Driver;
import com.fepsilva.insurer.usecase.claim.impl.ClaimUpdateUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

@ExtendWith(MockitoExtension.class)
public class ClaimUpdateUseCaseTest {

	@Mock
	private ClaimDao claimDao;

	@InjectMocks
	private ClaimUpdateUseCaseImpl claimUpdateUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void executeSuccessful() {
		// Given
		Long claimId = 1L;
		Car car = CarModelConverterTest.createNewCar(1L, "teste", "teste", "2023", 0F);
		Driver driver = DriverModelConverterTest.createNewDriver(1L, "teste", LocalDate.now());
		Claim claim = ClaimModelConverterTest.createNewClaim(claimId, car, driver, LocalDate.now());
		ClaimEntity claimEntity = ClaimModelConverter.modelToEntity(claim);

		// When
		Mockito.when(this.claimDao.update(ClaimModelConverter.modelToEntity(claim)))
				.thenReturn(claimEntity);

		// Then
		claim = this.claimUpdateUseCase.execute(claimId, claim);
		Assertions.assertNotNull(claim);
	}

	@Test
	@DisplayName("Method execute() - Not Found")
	public void executeNotFound() {
		// Given
		Long claimId = 1L;
		Car car = CarModelConverterTest.createNewCar(1L, "teste", "teste", "2023", 0F);
		Driver driver = DriverModelConverterTest.createNewDriver(1L, "teste", LocalDate.now());
		Claim claim = ClaimModelConverterTest.createNewClaim(claimId, car, driver, LocalDate.now());

		// When
		Mockito.when(this.claimDao.update(ClaimModelConverter.modelToEntity(claim)))
				.thenReturn(null);

		// Then
		claim = this.claimUpdateUseCase.execute(claimId, claim);
		Assertions.assertNull(claim);
	}

}