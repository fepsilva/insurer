package com.fepsilva.insurer.usecase.insurance.impl;

import com.fepsilva.insurer.dao.InsuranceDao;
import com.fepsilva.insurer.usecase.insurance.InsuranceDeleteUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InsuranceDeleteUseCaseImpl implements InsuranceDeleteUseCase {

	@Autowired
	private InsuranceDao insuranceDao;

	public void execute(Long insuranceId) {
		this.insuranceDao.delete(insuranceId);
	}

}