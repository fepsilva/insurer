package com.fepsilva.insurer.usecase.riskage;

import com.fepsilva.insurer.model.RiskAge;

import java.util.List;

public interface RiskAgeFindAllUseCase {

	public List<RiskAge> execute();

}