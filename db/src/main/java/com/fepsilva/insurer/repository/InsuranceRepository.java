package com.fepsilva.insurer.repository;

import com.fepsilva.insurer.entity.InsuranceEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InsuranceRepository extends JpaRepository<InsuranceEntity, Long> {

}