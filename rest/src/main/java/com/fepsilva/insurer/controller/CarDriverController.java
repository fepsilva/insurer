package com.fepsilva.insurer.controller;

import com.fepsilva.insurer.converter.CarDriverDtoConverter;
import com.fepsilva.insurer.dto.form.CarDriverForm;
import com.fepsilva.insurer.dto.view.CarDriverView;
import com.fepsilva.insurer.usecase.cardriver.CarDriverCreateUseCase;
import com.fepsilva.insurer.usecase.cardriver.CarDriverDeleteUseCase;
import com.fepsilva.insurer.usecase.cardriver.CarDriverFindAllUseCase;
import com.fepsilva.insurer.usecase.cardriver.CarDriverFindUseCase;
import com.fepsilva.insurer.usecase.cardriver.CarDriverUpdateUseCase;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Validated
@RestController
@RequestMapping("/car-driver")
@Tag(name = "API de Motorista do Veículo", description = "Endpoints responsáveis pela gestão do cadastro de vínculo entre carros e motoristas")
public class CarDriverController {

	@Autowired
	private CarDriverCreateUseCase carCreateUseCase;

	@Autowired
	private CarDriverFindAllUseCase carFindAllUseCase;

	@Autowired
	private CarDriverFindUseCase carFindUseCase;

	@Autowired
	private CarDriverUpdateUseCase carUpdateUseCase;

	@Autowired
	private CarDriverDeleteUseCase carDeleteUseCase;

	@Operation(summary = "Endpoint para criar um novo vínculo")
	@ApiResponse(responseCode = "200", description = "Vínculo cadastrado")
	@PostMapping(consumes = "application/json", produces = "application/json")
	public ResponseEntity<CarDriverView> create(@Valid @RequestBody CarDriverForm carForm) {
		try {
			return Optional.ofNullable(this.carCreateUseCase.execute(CarDriverDtoConverter.dtoToEntity(carForm)))
					.map(CarDriverDtoConverter::entityToDto)
					.map(ResponseEntity::ok)
					.get();
		} catch (DataIntegrityViolationException e) {
			log.error("Dados inválidos", e);
			return ResponseEntity.badRequest().build();
		} catch (Exception e) {
			log.error("Erro ao cadastrar vínculo de carro e motorista", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@Operation(summary = "Endpoint para consultar todos os vínculos")
	@ApiResponse(responseCode = "200", description = "Vínculos consultados")
	@GetMapping
	public ResponseEntity<List<CarDriverView>> findAll() {
		try {
			return ResponseEntity.ok(this.carFindAllUseCase.execute().stream()
					.map(CarDriverDtoConverter::entityToDto)
					.collect(Collectors.toList()));
		} catch (Exception e) {
			log.error("Erro ao procurar vínculo de carro e motorista", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@Operation(summary = "Endpoint para consultar um vínculo")
	@ApiResponse(responseCode = "200", description = "Vínculo consultado")
	@ApiResponse(responseCode = "204", description = "Vínculo não encontrado")
	@GetMapping("/{carId}")
	public ResponseEntity<CarDriverView> find(@Parameter(description = "Id do vínculo") @PathVariable Long carId) {
		try {
			return Optional.ofNullable(this.carFindUseCase.execute(carId))
					.map(CarDriverDtoConverter::entityToDto)
					.map(ResponseEntity::ok)
					.orElseGet(() -> ResponseEntity.noContent().build());
		} catch (Exception e) {
			log.error("Erro ao procurar vínculo de carro e motorista", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@Operation(summary = "Endpoint para alterar um vínculo")
	@ApiResponse(responseCode = "200", description = "Vínculo alterado")
	@ApiResponse(responseCode = "204", description = "Vínculo não encontrado")
	@PutMapping("/{carId}")
	public ResponseEntity<CarDriverView> update(
			@Parameter(description = "Id do vínculo") @PathVariable Long carId,
			@Valid @RequestBody CarDriverForm carForm) {
		try {
			return Optional.ofNullable(this.carUpdateUseCase.execute(carId, CarDriverDtoConverter.dtoToEntity(carForm)))
					.map(CarDriverDtoConverter::entityToDto)
					.map(ResponseEntity::ok)
					.orElseGet(() -> ResponseEntity.noContent().build());
		} catch (DataIntegrityViolationException e) {
			log.error("Dados inválidos", e);
			return ResponseEntity.badRequest().build();
		} catch (Exception e) {
			log.error("Erro ao alterar vínculo de carro e motorista", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@Operation(summary = "Endpoint para remover um vínculo")
	@ApiResponse(responseCode = "200", description = "Vínculo removido")
	@ApiResponse(responseCode = "204", description = "Vínculo não encontrado")
	@DeleteMapping("/{carId}")
	public ResponseEntity delete(@Parameter(description = "Id do vínculo") @PathVariable Long carId) {
		try {
			this.carDeleteUseCase.execute(carId);
			return ResponseEntity.ok().build();
		} catch (EmptyResultDataAccessException e) {
			log.info("Vínculo não encontrado");
			return ResponseEntity.noContent().build();
		} catch (Exception e) {
			log.error("Erro ao remover vínculo de carro e motorista", e);
			return ResponseEntity.internalServerError().build();
		}
	}

}