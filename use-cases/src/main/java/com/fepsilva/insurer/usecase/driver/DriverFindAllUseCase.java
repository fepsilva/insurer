package com.fepsilva.insurer.usecase.driver;

import com.fepsilva.insurer.model.Driver;

import java.util.List;

public interface DriverFindAllUseCase {

	public List<Driver> execute();

}