package com.fepsilva.insurer.usecase.cardriver;

import com.fepsilva.insurer.converter.CarDriverModelConverter;
import com.fepsilva.insurer.converter.CarDriverModelConverterTest;
import com.fepsilva.insurer.converter.CarModelConverterTest;
import com.fepsilva.insurer.converter.DriverModelConverterTest;
import com.fepsilva.insurer.dao.CarDriverDao;
import com.fepsilva.insurer.entity.CarDriverEntity;
import com.fepsilva.insurer.model.Car;
import com.fepsilva.insurer.model.CarDriver;
import com.fepsilva.insurer.model.Driver;
import com.fepsilva.insurer.usecase.cardriver.impl.CarDriverUpdateUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

@ExtendWith(MockitoExtension.class)
public class CarDriverUpdateUseCaseTest {

	@Mock
	private CarDriverDao carDriverDao;

	@InjectMocks
	private CarDriverUpdateUseCaseImpl carDriverUpdateUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void executeSuccessful() {
		// Given
		Long carDriverId = 1L;
		Car car = CarModelConverterTest.createNewCar(1L, "teste", "teste", "2023", 0F);
		Driver driver = DriverModelConverterTest.createNewDriver(1L, "teste", LocalDate.now());
		CarDriver carDriver = CarDriverModelConverterTest.createNewCarDriver(carDriverId, car, driver, true);
		CarDriverEntity carDriverEntity = CarDriverModelConverter.modelToEntity(carDriver);

		// When
		Mockito.when(this.carDriverDao.update(CarDriverModelConverter.modelToEntity(carDriver)))
				.thenReturn(carDriverEntity);

		// Then
		carDriver = this.carDriverUpdateUseCase.execute(carDriverId, carDriver);
		Assertions.assertNotNull(carDriver);
	}

	@Test
	@DisplayName("Method execute() - Not Found")
	public void executeNotFound() {
		// Given
		Long carDriverId = 1L;
		Car car = CarModelConverterTest.createNewCar(1L, "teste", "teste", "2023", 0F);
		Driver driver = DriverModelConverterTest.createNewDriver(1L, "teste", LocalDate.now());
		CarDriver carDriver = CarDriverModelConverterTest.createNewCarDriver(carDriverId, car, driver, true);

		// When
		Mockito.when(this.carDriverDao.update(CarDriverModelConverter.modelToEntity(carDriver)))
				.thenReturn(null);

		// Then
		carDriver = this.carDriverUpdateUseCase.execute(carDriverId, carDriver);
		Assertions.assertNull(carDriver);
	}

}