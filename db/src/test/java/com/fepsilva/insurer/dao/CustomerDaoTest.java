package com.fepsilva.insurer.dao;

import com.fepsilva.insurer.dao.impl.CustomerDaoImpl;
import com.fepsilva.insurer.dao.impl.DriverDaoImpl;
import com.fepsilva.insurer.entity.CustomerEntity;
import com.fepsilva.insurer.entity.DriverEntity;
import com.fepsilva.insurer.repository.CustomerRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class CustomerDaoTest {

	@Mock
	private DriverDaoImpl driverDao;

	@Mock
	private CustomerRepository customerRepository;

	@InjectMocks
	private CustomerDaoImpl customerDao;

	@Test
	@DisplayName("Method persist() - Should execute successful")
	public void persistSuccessful() {
		// Given
		Long id = 1L;
		String name = "teste";
		DriverEntity driver = DriverDaoTest.createNewDriverEntity(1L, "document", LocalDate.now());

		CustomerEntity customerEntity = createNewCustomerEntity(null, name, driver);
		CustomerEntity customerEntityNew = createNewCustomerEntity(id, name, driver);

		// When
		Mockito.when(this.driverDao.find(driver.getId()))
				.thenReturn(Optional.of(driver));
		Mockito.when(this.customerRepository.save(customerEntity))
				.thenReturn(customerEntityNew);

		// Then
		customerEntity = customerDao.persist(customerEntity);
		Assertions.assertNotNull(customerEntity.getId());
		Assertions.assertNotNull(customerEntity.getDriver().getDocument());
	}

	@Test
	@DisplayName("Method persist() - Without complement entity")
	public void persistWithoutComplement() {
		// Given
		Long id = 1L;
		String name = "teste";
		DriverEntity driver = DriverDaoTest.createNewDriverEntity(1L, null, null);

		CustomerEntity customerEntity = createNewCustomerEntity(null, name, driver);
		CustomerEntity customerEntityNew = createNewCustomerEntity(id, name, driver);

		// When
		Mockito.when(this.driverDao.find(driver.getId()))
				.thenReturn(Optional.ofNullable(null));
		Mockito.when(this.customerRepository.save(customerEntity))
				.thenReturn(customerEntityNew);

		// Then
		customerEntity = customerDao.persist(customerEntity);
		Assertions.assertNotNull(customerEntity.getId());
		Assertions.assertNull(customerEntity.getDriver().getDocument());
	}

	@Test
	@DisplayName("Method findAll() - Should execute successful")
	public void findAllSuccessful() {
		// Given
		DriverEntity driver = DriverDaoTest.createNewDriverEntity(1L, "document", LocalDate.now());

		List<CustomerEntity> listCustomerEntity = List.of(
				createNewCustomerEntity(1L, "teste1", driver),
				createNewCustomerEntity(2L, "teste2", driver),
				createNewCustomerEntity(3L, "teste3", driver));

		// When
		Mockito.when(this.customerRepository.findAll())
				.thenReturn(listCustomerEntity);

		// Then
		listCustomerEntity = customerDao.findAll();
		Assertions.assertNotNull(listCustomerEntity);
		Assertions.assertEquals(listCustomerEntity.size(), 3);
	}

	@Test
	@DisplayName("Method find() - Should execute successful")
	public void findSuccessful() {
		// Given
		Long id = 1L;
		DriverEntity driver = DriverDaoTest.createNewDriverEntity(1L, "document", LocalDate.now());
		CustomerEntity customerEntity = createNewCustomerEntity(id, "teste1", driver);

		// When
		Mockito.when(this.customerRepository.findById(id))
				.thenReturn(Optional.of(customerEntity));

		// Then
		Optional<CustomerEntity> optionalCustomerEntity = customerDao.find(id);
		Assertions.assertNotNull(optionalCustomerEntity.get());
		Assertions.assertEquals(optionalCustomerEntity.get().getId(), id);
	}

	@Test
	@DisplayName("Method update() - Should execute successful")
	public void updateSuccessful() {
		// Given
		DriverEntity driver = DriverDaoTest.createNewDriverEntity(1L, "document", LocalDate.now());
		CustomerEntity customerEntity = createNewCustomerEntity(1L, "teste", driver);

		// When
		Mockito.when(this.customerRepository.existsById(customerEntity.getId()))
				.thenReturn(true);
		Mockito.when(this.driverDao.find(driver.getId()))
				.thenReturn(Optional.of(driver));
		Mockito.when(this.customerRepository.save(customerEntity))
				.thenReturn(customerEntity);

		// Then
		customerEntity = customerDao.update(customerEntity);
		Assertions.assertNotNull(customerEntity.getId());
		Assertions.assertNotNull(customerEntity.getDriver().getDocument());
	}

	@Test
	@DisplayName("Method update() - Without complement entity")
	public void updateWithoutComplement() {
		// Given
		DriverEntity driver = DriverDaoTest.createNewDriverEntity(1L, null, null);
		CustomerEntity customerEntity = createNewCustomerEntity(1L, "teste", driver);

		// When
		Mockito.when(this.customerRepository.existsById(customerEntity.getId()))
				.thenReturn(true);
		Mockito.when(this.driverDao.find(driver.getId()))
				.thenReturn(Optional.ofNullable(null));
		Mockito.when(this.customerRepository.save(customerEntity))
				.thenReturn(customerEntity);

		// Then
		customerEntity = customerDao.update(customerEntity);
		Assertions.assertNotNull(customerEntity.getId());
		Assertions.assertNull(customerEntity.getDriver().getDocument());
	}

	@Test
	@DisplayName("Method update() - Should not found")
	public void updateNotFound() {
		// Given
		DriverEntity driver = DriverDaoTest.createNewDriverEntity(1L, "document", LocalDate.now());
		CustomerEntity customerEntity = createNewCustomerEntity(1L, "teste", driver);

		// When
		Mockito.when(this.customerRepository.existsById(customerEntity.getId()))
				.thenReturn(false);

		// Then
		customerEntity = customerDao.update(customerEntity);
		Assertions.assertNull(customerEntity);
	}

	@Test
	@DisplayName("Method delete() - Should execute successful")
	public void deleteSuccessful() {
		// Given
		Long id = 1L;

		// Then
		Assertions.assertDoesNotThrow(() -> customerDao.delete(id));
	}

	public static CustomerEntity createNewCustomerEntity(Long id, String name, DriverEntity driver) {
		return CustomerEntity.builder()
				.id(id)
				.name(name)
				.driver(driver)
				.build();
	}

}