package com.fepsilva.insurer.usecase.claim.impl;

import com.fepsilva.insurer.model.Claim;
import com.fepsilva.insurer.model.Insurance;
import com.fepsilva.insurer.usecase.claim.ClaimCarBudgetCalculationUseCase;
import com.fepsilva.insurer.usecase.claim.ClaimFindAllUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class ClaimCarBudgetCalculationUseCaseImpl implements ClaimCarBudgetCalculationUseCase {

	private static final BigDecimal PERCENT_WITH_SINISTER = BigDecimal.valueOf(0.02);

	@Autowired
	private ClaimFindAllUseCase claimFindAllUseCase;

	public BigDecimal execute(Insurance insurance) {
		if (insurance.getCar() != null) {
			List<Claim> listClaim = claimFindAllUseCase.execute();

			if (listClaim != null) {
				return listClaim.stream()
						.filter(item -> item.getCar().getId() == insurance.getCar().getId())
						.findFirst()
						.map(item -> PERCENT_WITH_SINISTER)
						.orElseGet(() -> BigDecimal.ZERO);
			}
		}

		return BigDecimal.ZERO;
	}

}