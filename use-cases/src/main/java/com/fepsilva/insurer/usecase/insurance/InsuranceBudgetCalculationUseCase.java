package com.fepsilva.insurer.usecase.insurance;

import com.fepsilva.insurer.model.Insurance;

public interface InsuranceBudgetCalculationUseCase {

	public Float execute(Insurance insurance);

}