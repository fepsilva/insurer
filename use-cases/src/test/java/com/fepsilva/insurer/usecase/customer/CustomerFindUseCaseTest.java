package com.fepsilva.insurer.usecase.customer;

import com.fepsilva.insurer.converter.CustomerModelConverterTest;
import com.fepsilva.insurer.converter.DriverModelConverterTest;
import com.fepsilva.insurer.dao.CustomerDao;
import com.fepsilva.insurer.entity.CustomerEntity;
import com.fepsilva.insurer.entity.DriverEntity;
import com.fepsilva.insurer.model.Customer;
import com.fepsilva.insurer.usecase.customer.impl.CustomerFindUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class CustomerFindUseCaseTest {

	@Mock
	private CustomerDao customerDao;

	@InjectMocks
	private CustomerFindUseCaseImpl customerFindUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void executeSuccessful() {
		// Given
		Long customerId = 1L;
		DriverEntity driver = DriverModelConverterTest.createNewDriverEntity(1L, "teste", LocalDate.now());
		CustomerEntity customerEntity = CustomerModelConverterTest.createNewCustomerEntity(customerId, "teste", driver);

		// When
		Mockito.when(this.customerDao.find(customerId))
				.thenReturn(Optional.ofNullable(customerEntity));

		// Then
		Customer customer = this.customerFindUseCase.execute(customerId);
		Assertions.assertNotNull(customer);

		CustomerModelConverterTest.isEquals(customerEntity, customer);
	}

	@Test
	@DisplayName("Method execute() - Not Found")
	public void executeNotFound() {
		// Given
		Long customerId = 1L;

		// When
		Mockito.when(this.customerDao.find(customerId))
				.thenReturn(Optional.ofNullable(null));

		// Then
		Customer customer = this.customerFindUseCase.execute(customerId);
		Assertions.assertNull(customer);
	}

}