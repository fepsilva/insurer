package com.fepsilva.insurer.usecase.claim;

import com.fepsilva.insurer.converter.CarModelConverterTest;
import com.fepsilva.insurer.converter.ClaimModelConverter;
import com.fepsilva.insurer.converter.ClaimModelConverterTest;
import com.fepsilva.insurer.converter.DriverModelConverterTest;
import com.fepsilva.insurer.dao.ClaimDao;
import com.fepsilva.insurer.entity.ClaimEntity;
import com.fepsilva.insurer.model.Car;
import com.fepsilva.insurer.model.Claim;
import com.fepsilva.insurer.model.Driver;
import com.fepsilva.insurer.usecase.claim.impl.ClaimCreateUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

@ExtendWith(MockitoExtension.class)
public class ClaimCreateUseCaseTest {

	@Mock
	private ClaimDao claimDao;

	@InjectMocks
	private ClaimCreateUseCaseImpl claimCreateUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void executeSuccessful() {
		// Given
		Car car = CarModelConverterTest.createNewCar(1L, "teste", "teste", "2023", 0F);
		Driver driver = DriverModelConverterTest.createNewDriver(1L, "teste", LocalDate.now());
		Claim claim = ClaimModelConverterTest.createNewClaim(null, car, driver, LocalDate.now());
		ClaimEntity claimEntity = ClaimModelConverter.modelToEntity(claim);
		claimEntity.setId(1L);

		// When
		Mockito.when(this.claimDao.persist(ClaimModelConverter.modelToEntity(claim)))
				.thenReturn(claimEntity);

		// Then
		claim = this.claimCreateUseCase.execute(claim);
		Assertions.assertNotNull(claim);
		Assertions.assertNotNull(claim.getId());
	}

	@Test
	@DisplayName("Method execute() - Should return null")
	public void executeNull() {
		// Given
		Car car = CarModelConverterTest.createNewCar(1L, "teste", "teste", "2023", 0F);
		Driver driver = DriverModelConverterTest.createNewDriver(1L, "teste", LocalDate.now());
		Claim claim = ClaimModelConverterTest.createNewClaim(null, car, driver, LocalDate.now());

		// When
		Mockito.when(this.claimDao.persist(ClaimModelConverter.modelToEntity(claim)))
				.thenReturn(null);

		// Then
		claim = this.claimCreateUseCase.execute(claim);
		Assertions.assertNull(claim);
	}

}