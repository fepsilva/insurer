package com.fepsilva.insurer.usecase.claim;

import com.fepsilva.insurer.converter.CarModelConverterTest;
import com.fepsilva.insurer.converter.ClaimModelConverterTest;
import com.fepsilva.insurer.converter.DriverModelConverterTest;
import com.fepsilva.insurer.dao.ClaimDao;
import com.fepsilva.insurer.entity.CarEntity;
import com.fepsilva.insurer.entity.ClaimEntity;
import com.fepsilva.insurer.entity.DriverEntity;
import com.fepsilva.insurer.model.Claim;
import com.fepsilva.insurer.usecase.claim.impl.ClaimFindUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class ClaimFindUseCaseTest {

	@Mock
	private ClaimDao claimDao;

	@InjectMocks
	private ClaimFindUseCaseImpl claimFindUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void executeSuccessful() {
		// Given
		Long claimId = 1L;
		CarEntity car = CarModelConverterTest.createNewCarEntity(1L, "teste", "teste", "2023", 0F);
		DriverEntity driver = DriverModelConverterTest.createNewDriverEntity(1L, "teste", LocalDate.now());
		ClaimEntity claimEntity = ClaimModelConverterTest.createNewClaimEntity(claimId, car, driver, LocalDate.now());

		// When
		Mockito.when(this.claimDao.find(claimId))
				.thenReturn(Optional.ofNullable(claimEntity));

		// Then
		Claim claim = this.claimFindUseCase.execute(claimId);
		Assertions.assertNotNull(claim);

		ClaimModelConverterTest.isEquals(claimEntity, claim);
	}

	@Test
	@DisplayName("Method execute() - Not Found")
	public void executeNotFound() {
		// Given
		Long claimId = 1L;

		// When
		Mockito.when(this.claimDao.find(claimId))
				.thenReturn(Optional.ofNullable(null));

		// Then
		Claim claim = this.claimFindUseCase.execute(claimId);
		Assertions.assertNull(claim);
	}

}