package com.fepsilva.insurer.dao.impl;

import com.fepsilva.insurer.dao.CarDao;
import com.fepsilva.insurer.dao.ClaimDao;
import com.fepsilva.insurer.dao.DriverDao;
import com.fepsilva.insurer.entity.ClaimEntity;
import com.fepsilva.insurer.repository.ClaimRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClaimDaoImpl implements ClaimDao {

	@Autowired
	private ClaimRepository claimRepository;

	@Autowired
	private CarDao carDao;

	@Autowired
	private DriverDao driverDao;

	public ClaimEntity persist(ClaimEntity claimEntity) {
		claimEntity = this.claimRepository.save(claimEntity);
		this.fillComplementEntity(claimEntity);

		return claimEntity;
	}

	public List<ClaimEntity> findAll() {
		return this.claimRepository.findAll();
	}

	public Optional<ClaimEntity> find(Long claimId) {
		return this.claimRepository.findById(claimId);
	}

	public ClaimEntity update(ClaimEntity claimEntity) {
		if (this.claimRepository.existsById(claimEntity.getId())) {
			claimEntity = this.claimRepository.save(claimEntity);
			this.fillComplementEntity(claimEntity);

			return claimEntity;
		}

		return null;
	}

	public void delete(Long claimId) {
		this.claimRepository.deleteById(claimId);
	}

	private void fillComplementEntity(ClaimEntity claimEntity) {
		this.carDao.find(claimEntity.getCar().getId())
				.ifPresent(car -> claimEntity.setCar(car));

		this.driverDao.find(claimEntity.getDriver().getId())
				.ifPresent(driver -> claimEntity.setDriver(driver));
	}

}