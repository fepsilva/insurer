package com.fepsilva.insurer.dao.impl;

import com.fepsilva.insurer.dao.RiskAgeDao;
import com.fepsilva.insurer.entity.RiskAgeEntity;
import com.fepsilva.insurer.repository.RiskAgeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RiskAgeDaoImpl implements RiskAgeDao {

	@Autowired
	private RiskAgeRepository riskAgeRepository;

	public RiskAgeEntity persist(RiskAgeEntity riskAgeEntity) {
		return this.riskAgeRepository.save(riskAgeEntity);
	}

	public List<RiskAgeEntity> findAll() {
		return this.riskAgeRepository.findAll();
	}

	public Optional<RiskAgeEntity> find(Long riskAgeId) {
		return this.riskAgeRepository.findById(riskAgeId);
	}

	public RiskAgeEntity update(RiskAgeEntity riskAgeEntity) {
		if (this.riskAgeRepository.existsById(riskAgeEntity.getId())) {
			return this.riskAgeRepository.save(riskAgeEntity);
		}

		return null;
	}

	public void delete(Long riskAgeId) {
		this.riskAgeRepository.deleteById(riskAgeId);
	}

}