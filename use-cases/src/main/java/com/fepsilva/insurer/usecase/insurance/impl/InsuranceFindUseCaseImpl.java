package com.fepsilva.insurer.usecase.insurance.impl;

import com.fepsilva.insurer.converter.InsuranceModelConverter;
import com.fepsilva.insurer.dao.InsuranceDao;
import com.fepsilva.insurer.model.Insurance;
import com.fepsilva.insurer.usecase.insurance.InsuranceFindUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InsuranceFindUseCaseImpl implements InsuranceFindUseCase {

	@Autowired
	private InsuranceDao insuranceDao;

	public Insurance execute(Long insuranceId) {
		return this.insuranceDao.find(insuranceId)
				.map(InsuranceModelConverter::entityToModel)
				.orElseGet(() -> null);
	}

}