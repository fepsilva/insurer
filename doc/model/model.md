# Modelagem da Aplicação

Essa documentação é para exibir o modelo do banco de dados (modificado)

## Modelo

![model01.PNG](model01.PNG)

## Dicionário de Dados

<details><summary>RISK_AGES</summary>

- **ID:** Id da idade de risco
- **BEGIN_AGE:** Idade inicial
- **END_AGE:** Idade final
- **PERCENT_RISK:** Porcentagem cobrada baseado no risco
</details>

<details><summary>CARS</summary>

  - **ID:** Id do carro
  - **MODEL:** Modelo do veículo
  - **MANUFACTURER:** Fabricante do veículo
  - **MODEL_YEAR:** Ano do modelo do veículo
  - **FIPE_VALUE:** Valor do veículo
</details>

<details><summary>DRIVERS</summary>

  - **ID:** Id do motorista
  - **DOCUMENT:** CNH do motorista
  - **BIRTHDATE:** Data de nascimento do motorista
</details>

<details><summary>CUSTOMERS</summary>

  - **ID:**  Id do cliente
  - **NAME:** Nome do cliente
  - **DRIVER_ID:** Id dos dados de motorista
</details>

<details><summary>CLAIMS</summary>

  - **ID:** Id do sinistro
  - **CAR_ID:** Id do veículo envolvido no sinistro
  - **DRIVER_ID:** Id do motorista envolvido no sinistro
  - **EVEMT_DATE:** Data do evento do sinistro
</details>

<details><summary>CAR_DRIVERS</summary>

  - **ID:** Id do motorista do veiculo
  - **CAR_ID:** Id do carro
  - **DRIVER_ID:** Id do motorista que será condutor do veículo
  - **IS_MAIN_DRIVER:** Flag que sinaliza se o motorista é o principal condutor do veículo
</details>

<details><summary>INSURANCES</summary>

  - **ID:** Id do seguro
  - **CUSTOMER_ID:** Id do cliente
  - **CREATION_DT:** Data da solicitação do orçamento
  - **UPDATED_DT:** Data da atualização do orçamento
  - **CAR_ID:** Id do carro
  - **IS_ACTIVE:** Flag se o orçamento está ativo
  - **PERCENT_RISK:** Porcentagem cobrada baseado no risco
  - **FIPE_VALUE:** Valor fipe no momento do orçamento
</details>


[<< Voltar para página principal](/README.md)
