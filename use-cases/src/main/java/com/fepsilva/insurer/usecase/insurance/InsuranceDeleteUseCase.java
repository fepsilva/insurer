package com.fepsilva.insurer.usecase.insurance;

public interface InsuranceDeleteUseCase {

	public void execute(Long insuranceId);

}