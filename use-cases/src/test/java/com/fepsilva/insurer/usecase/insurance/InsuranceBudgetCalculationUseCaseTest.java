package com.fepsilva.insurer.usecase.insurance;

import com.fepsilva.insurer.converter.CarModelConverterTest;
import com.fepsilva.insurer.converter.CustomerModelConverterTest;
import com.fepsilva.insurer.converter.DriverModelConverterTest;
import com.fepsilva.insurer.converter.InsuranceModelConverterTest;
import com.fepsilva.insurer.model.Car;
import com.fepsilva.insurer.model.Customer;
import com.fepsilva.insurer.model.Driver;
import com.fepsilva.insurer.model.Insurance;
import com.fepsilva.insurer.usecase.claim.ClaimCarBudgetCalculationUseCase;
import com.fepsilva.insurer.usecase.claim.ClaimDriverBudgetCalculationUseCase;
import com.fepsilva.insurer.usecase.insurance.impl.InsuranceBudgetCalculationUseCaseImpl;
import com.fepsilva.insurer.usecase.riskage.RiskAgeBudgetCalculationUseCase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@ExtendWith(MockitoExtension.class)
public class InsuranceBudgetCalculationUseCaseTest {

	private static final Float PERCENT_FIXED = 0.06F;

	@Mock
	private ClaimDriverBudgetCalculationUseCase claimBudgetCalculationUseCase;

	@Mock
	private ClaimCarBudgetCalculationUseCase claimCarBudgetCalculationUseCase;

	@Mock
	private RiskAgeBudgetCalculationUseCase riskAgeBudgetCalculationUseCase;

	@InjectMocks
	private InsuranceBudgetCalculationUseCaseImpl insuranceBudgetCalculationUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void execute() {
		// Given
		Car car = CarModelConverterTest.createNewCar(1L, "teste", "teste", "2023", 0F);
		Driver driver = DriverModelConverterTest.createNewDriver(1L, "teste", LocalDate.now());
		Customer customer = CustomerModelConverterTest.createNewCustomer(1L, "teste", driver);
		Insurance insurance = InsuranceModelConverterTest.createNewInsurance(1L, customer, LocalDateTime.now(), LocalDateTime.now(), car, true, 0F, 0F);

		// When
		Mockito.when(this.claimBudgetCalculationUseCase.execute(insurance))
				.thenReturn(BigDecimal.ZERO);
		Mockito.when(this.claimCarBudgetCalculationUseCase.execute(insurance))
				.thenReturn(BigDecimal.ZERO);
		Mockito.when(this.riskAgeBudgetCalculationUseCase.execute(insurance))
				.thenReturn(BigDecimal.ZERO);

		// Then
		Float value = this.insuranceBudgetCalculationUseCase.execute(insurance);
		Assertions.assertNotNull(value);
		Assertions.assertEquals(value, PERCENT_FIXED);
	}

}