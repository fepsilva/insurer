package com.fepsilva.insurer.dao.impl;

import com.fepsilva.insurer.dao.DriverDao;
import com.fepsilva.insurer.entity.DriverEntity;
import com.fepsilva.insurer.repository.DriverRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DriverDaoImpl implements DriverDao {

	@Autowired
	private DriverRepository driverRepository;

	public DriverEntity persist(DriverEntity driverEntity) {
		return this.driverRepository.save(driverEntity);
	}

	public List<DriverEntity> findAll() {
		return this.driverRepository.findAll();
	}

	public Optional<DriverEntity> find(Long driverId) {
		return this.driverRepository.findById(driverId);
	}

	public DriverEntity update(DriverEntity driverEntity) {
		if (this.driverRepository.existsById(driverEntity.getId())) {
			return this.driverRepository.save(driverEntity);
		}

		return null;
	}

	public void delete(Long driverId) {
		this.driverRepository.deleteById(driverId);
	}

}