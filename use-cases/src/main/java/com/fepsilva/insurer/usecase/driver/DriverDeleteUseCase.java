package com.fepsilva.insurer.usecase.driver;

public interface DriverDeleteUseCase {

	public void execute(Long driverId);

}