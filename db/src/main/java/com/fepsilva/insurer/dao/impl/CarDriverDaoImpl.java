package com.fepsilva.insurer.dao.impl;

import com.fepsilva.insurer.dao.CarDao;
import com.fepsilva.insurer.dao.CarDriverDao;
import com.fepsilva.insurer.dao.DriverDao;
import com.fepsilva.insurer.entity.CarDriverEntity;
import com.fepsilva.insurer.repository.CarDriverRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CarDriverDaoImpl implements CarDriverDao {

	@Autowired
	private CarDriverRepository carDriverRepository;

	@Autowired
	private CarDao carDao;

	@Autowired
	private DriverDao driverDao;

	public CarDriverEntity persist(CarDriverEntity carDriverEntity) {
		carDriverEntity = this.carDriverRepository.save(carDriverEntity);
		this.fillComplementEntity(carDriverEntity);

		return carDriverEntity;
	}

	public List<CarDriverEntity> findAll() {
		return this.carDriverRepository.findAll();
	}

	public Optional<CarDriverEntity> find(Long carDriverId) {
		return this.carDriverRepository.findById(carDriverId);
	}

	public CarDriverEntity update(CarDriverEntity carDriverEntity) {
		if (this.carDriverRepository.existsById(carDriverEntity.getId())) {
			carDriverEntity = this.carDriverRepository.save(carDriverEntity);
			this.fillComplementEntity(carDriverEntity);

			return carDriverEntity;
		}

		return null;
	}

	public void delete(Long carDriverId) {
		this.carDriverRepository.deleteById(carDriverId);
	}

	private void fillComplementEntity(CarDriverEntity carDriverEntity) {
		this.carDao.find(carDriverEntity.getCar().getId())
				.ifPresent(car -> carDriverEntity.setCar(car));

		this.driverDao.find(carDriverEntity.getDriver().getId())
				.ifPresent(driver -> carDriverEntity.setDriver(driver));
	}

}