package com.fepsilva.insurer.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "RISK_AGES")
public class RiskAgeEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false)
	private Long id;

	@Column(name = "BEGIN_AGE", nullable = false)
	private Long beginAge;

	@Column(name = "END_AGE", nullable = false)
	private Long endAge;

	@Column(name = "PERCENT_RISK", nullable = false)
	private BigDecimal percentRisk;

}