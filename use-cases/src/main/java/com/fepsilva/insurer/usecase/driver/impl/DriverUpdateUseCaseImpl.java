package com.fepsilva.insurer.usecase.driver.impl;

import com.fepsilva.insurer.converter.DriverModelConverter;
import com.fepsilva.insurer.dao.DriverDao;
import com.fepsilva.insurer.model.Driver;
import com.fepsilva.insurer.usecase.driver.DriverUpdateUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DriverUpdateUseCaseImpl implements DriverUpdateUseCase {

	@Autowired
	private DriverDao driverDao;

	public Driver execute(Long driverId, Driver driver) {
		driver.setId(driverId);

		return Optional.ofNullable(this.driverDao.update(DriverModelConverter.modelToEntity(driver)))
				.map(DriverModelConverter::entityToModel)
				.orElseGet(() -> null);
	}

}