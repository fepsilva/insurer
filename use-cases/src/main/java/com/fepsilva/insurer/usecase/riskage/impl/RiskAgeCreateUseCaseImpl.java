package com.fepsilva.insurer.usecase.riskage.impl;

import com.fepsilva.insurer.converter.RiskAgeModelConverter;
import com.fepsilva.insurer.model.RiskAge;
import com.fepsilva.insurer.dao.RiskAgeDao;
import com.fepsilva.insurer.usecase.riskage.RiskAgeCreateUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RiskAgeCreateUseCaseImpl implements RiskAgeCreateUseCase {

	@Autowired
	private RiskAgeDao riskAgeDao;

	public RiskAge execute(RiskAge riskAge) {
		return Optional.ofNullable(this.riskAgeDao.persist(RiskAgeModelConverter.modelToEntity(riskAge)))
				.map(RiskAgeModelConverter::entityToModel)
				.orElseGet(() -> null);
	}

}