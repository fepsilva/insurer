package com.fepsilva.insurer.usecase.cardriver.impl;

import com.fepsilva.insurer.converter.CarDriverModelConverter;
import com.fepsilva.insurer.dao.CarDriverDao;
import com.fepsilva.insurer.model.CarDriver;
import com.fepsilva.insurer.usecase.cardriver.CarDriverFindAllUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CarDriverFindAllUseCaseImpl implements CarDriverFindAllUseCase {

	@Autowired
	private CarDriverDao carDriverDao;

	public List<CarDriver> execute() {
		return Optional.ofNullable(this.carDriverDao.findAll())
				.orElseGet(ArrayList::new)
				.stream().map(CarDriverModelConverter::entityToModel)
				.collect(Collectors.toList());
	}

}