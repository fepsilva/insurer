package com.fepsilva.insurer.usecase.insurance;

import com.fepsilva.insurer.model.Insurance;

public interface InsuranceCreateUseCase {

	public Insurance execute(Insurance insurance);

}