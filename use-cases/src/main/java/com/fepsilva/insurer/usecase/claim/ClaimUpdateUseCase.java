package com.fepsilva.insurer.usecase.claim;

import com.fepsilva.insurer.model.Claim;

public interface ClaimUpdateUseCase {

	public Claim execute(Long carDriverId, Claim carDriver);

}