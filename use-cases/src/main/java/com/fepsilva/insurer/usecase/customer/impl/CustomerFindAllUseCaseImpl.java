package com.fepsilva.insurer.usecase.customer.impl;

import com.fepsilva.insurer.converter.CustomerModelConverter;
import com.fepsilva.insurer.dao.CustomerDao;
import com.fepsilva.insurer.model.Customer;
import com.fepsilva.insurer.usecase.customer.CustomerFindAllUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CustomerFindAllUseCaseImpl implements CustomerFindAllUseCase {

	@Autowired
	private CustomerDao customerDao;

	public List<Customer> execute() {
		return Optional.ofNullable(this.customerDao.findAll())
				.orElseGet(ArrayList::new)
				.stream().map(CustomerModelConverter::entityToModel)
				.collect(Collectors.toList());
	}

}