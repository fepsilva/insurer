package com.fepsilva.insurer.converter;

import com.fepsilva.insurer.entity.RiskAgeEntity;
import com.fepsilva.insurer.model.RiskAge;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

@ExtendWith(MockitoExtension.class)
public class RiskAgeModelConverterTest {

	@Test
	@DisplayName("Method modelToEntity() - Should execute successful")
	public void modelToEntitySuccessful() {
		// Given
		RiskAge riskAge = createNewRiskAge(1L, 18L, 25L, BigDecimal.ZERO);

		// Then
		RiskAgeEntity riskAgeEntity = RiskAgeModelConverter.modelToEntity(riskAge);
		isEquals(riskAgeEntity, riskAge);
	}

	@Test
	@DisplayName("Method modelToEntity() - Without value")
	public void modelToEntityWithoutValue() {
		// Given
		RiskAge riskAge = null;

		// Then
		RiskAgeEntity riskAgeEntity = RiskAgeModelConverter.modelToEntity(riskAge);
		Assertions.assertNull(riskAgeEntity);
	}

	@Test
	@DisplayName("Method entityToModel() - Should execute successful")
	public void entityToModelSuccessful() {
		// Given
		RiskAgeEntity riskAgeEntity = createNewRiskAgeEntity(1L, 18L, 25L, BigDecimal.ZERO);

		// Then
		RiskAge riskAge = RiskAgeModelConverter.entityToModel(riskAgeEntity);
		isEquals(riskAgeEntity, riskAge);
	}

	@Test
	@DisplayName("Method entityToModel() - Without value")
	public void entityToModelWithoutValue() {
		// Given
		RiskAgeEntity riskAgeEntity = null;

		// Then
		RiskAge riskAge = RiskAgeModelConverter.entityToModel(riskAgeEntity);
		Assertions.assertNull(riskAge);
	}

	public static RiskAgeEntity createNewRiskAgeEntity(Long id, Long beginAge, Long endAge, BigDecimal percentRisk) {
		return RiskAgeEntity.builder()
				.id(id)
				.beginAge(beginAge)
				.endAge(endAge)
				.percentRisk(percentRisk)
				.build();
	}

	public static RiskAge createNewRiskAge(Long id, Long beginAge, Long endAge, BigDecimal percentRisk) {
		return RiskAge.builder()
				.id(id)
				.beginAge(beginAge)
				.endAge(endAge)
				.percentRisk(percentRisk)
				.build();
	}

	public static void isEquals(RiskAgeEntity riskAgeEntity, RiskAge riskAge) {
		Assertions.assertNotNull(riskAgeEntity);
		Assertions.assertNotNull(riskAge);

		Assertions.assertEquals(riskAgeEntity.getId(), riskAge.getId());
		Assertions.assertEquals(riskAgeEntity.getBeginAge(), riskAge.getBeginAge());
		Assertions.assertEquals(riskAgeEntity.getEndAge(), riskAge.getEndAge());
		Assertions.assertEquals(riskAgeEntity.getPercentRisk(), riskAge.getPercentRisk());
	}

}