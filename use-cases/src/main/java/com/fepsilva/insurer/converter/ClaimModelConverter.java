package com.fepsilva.insurer.converter;

import com.fepsilva.insurer.entity.ClaimEntity;
import com.fepsilva.insurer.model.Claim;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

public class ClaimModelConverter {

	public static ClaimEntity modelToEntity(Claim claim) {
		if (claim == null) {
			return null;
		}

		ClaimEntity claimEntity = getModelMapper().map(claim, ClaimEntity.class);
		claimEntity.setCar(CarModelConverter.modelToEntity(claim.getCar()));
		claimEntity.setDriver(DriverModelConverter.modelToEntity(claim.getDriver()));

		return claimEntity;
	}

	public static Claim entityToModel(ClaimEntity claimEntity) {
		if (claimEntity == null) {
			return null;
		}

		Claim claim = getModelMapper().map(claimEntity, Claim.class);
		claim.setCar(CarModelConverter.entityToModel(claimEntity.getCar()));
		claim.setDriver(DriverModelConverter.entityToModel(claimEntity.getDriver()));

		return claim;
	}

	private static ModelMapper getModelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

		return modelMapper;
	}

}