package com.fepsilva.insurer.usecase.insurance.impl;

import com.fepsilva.insurer.converter.InsuranceModelConverter;
import com.fepsilva.insurer.dao.InsuranceDao;
import com.fepsilva.insurer.model.Insurance;
import com.fepsilva.insurer.usecase.insurance.InsuranceFindAllUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class InsuranceFindAllUseCaseImpl implements InsuranceFindAllUseCase {

	@Autowired
	private InsuranceDao insuranceDao;

	public List<Insurance> execute() {
		return Optional.ofNullable(this.insuranceDao.findAll())
				.orElseGet(ArrayList::new)
				.stream().map(InsuranceModelConverter::entityToModel)
				.collect(Collectors.toList());
	}

}