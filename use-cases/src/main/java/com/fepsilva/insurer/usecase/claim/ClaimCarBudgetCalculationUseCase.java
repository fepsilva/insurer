package com.fepsilva.insurer.usecase.claim;

import com.fepsilva.insurer.model.Insurance;

import java.math.BigDecimal;

public interface ClaimCarBudgetCalculationUseCase {

	public BigDecimal execute(Insurance insurance);

}