package com.fepsilva.insurer.usecase.insurance;

import com.fepsilva.insurer.model.Insurance;

import java.util.List;

public interface InsuranceFindAllUseCase {

	public List<Insurance> execute();

}