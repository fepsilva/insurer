package com.fepsilva.insurer.converter;

import com.fepsilva.insurer.dto.form.CarDriverForm;
import com.fepsilva.insurer.dto.view.CarDriverView;
import com.fepsilva.insurer.dto.view.ClaimView;
import com.fepsilva.insurer.model.CarDriver;
import com.fepsilva.insurer.model.Claim;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

public class CarDriverDtoConverter {

	public static CarDriver dtoToEntity(CarDriverForm carForm) {
		if (carForm == null) {
			return null;
		}

		CarDriver carDriver = getModelMapper().map(carForm, CarDriver.class);
		carDriver.setCar(CarDtoConverter.idToEntity(carForm.getCarId()));
		carDriver.setDriver(DriverDtoConverter.idToEntity(carForm.getDriverId()));

		return carDriver;
	}

	public static CarDriverView entityToDto(CarDriver car) {
		if (car == null) {
			return null;
		}

		CarDriverView carDriverView = getModelMapper().map(car, CarDriverView.class);
		carDriverView.setCar(CarDtoConverter.entityToDto(car.getCar()));
		carDriverView.setDriver(DriverDtoConverter.entityToDto(car.getDriver()));

		return carDriverView;
	}

	private static ModelMapper getModelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

		return modelMapper;
	}

}