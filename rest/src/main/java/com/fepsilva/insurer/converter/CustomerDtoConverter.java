package com.fepsilva.insurer.converter;

import com.fepsilva.insurer.dto.form.CustomerForm;
import com.fepsilva.insurer.dto.view.CustomerView;
import com.fepsilva.insurer.model.Customer;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

public class CustomerDtoConverter {

	public static Customer idToEntity(Long customerId) {
		return Customer.builder()
				.id(customerId)
				.build();
	}

	public static Customer dtoToEntity(CustomerForm customerForm) {
		if (customerForm == null) {
			return null;
		}

		Customer customer = getModelMapper().map(customerForm, Customer.class);
		customer.setDriver(DriverDtoConverter.idToEntity(customerForm.getDriverId()));

		return customer;
	}

	public static CustomerView entityToDto(Customer customer) {
		if (customer == null) {
			return null;
		}

		CustomerView customerView = getModelMapper().map(customer, CustomerView.class);
		customerView.setDriver(DriverDtoConverter.entityToDto(customer.getDriver()));

		return customerView;
	}

	private static ModelMapper getModelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

		return modelMapper;
	}

}