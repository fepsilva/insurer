package com.fepsilva.insurer.usecase.car;

import com.fepsilva.insurer.converter.CarModelConverterTest;
import com.fepsilva.insurer.dao.CarDao;
import com.fepsilva.insurer.entity.CarEntity;
import com.fepsilva.insurer.model.Car;
import com.fepsilva.insurer.usecase.car.impl.CarFindAllUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

@ExtendWith(MockitoExtension.class)
public class CarFindAllUseCaseTest {

	@Mock
	private CarDao carDao;

	@InjectMocks
	private CarFindAllUseCaseImpl carFindAllUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful")
	public void executeSuccessful() {
		// Given
		List<CarEntity> listCarEntity = List.of(
				CarModelConverterTest.createNewCarEntity(1L, "teste1", "teste1", "2023", 1F),
				CarModelConverterTest.createNewCarEntity(2L, "teste2", "teste2", "2023", 2F),
				CarModelConverterTest.createNewCarEntity(3L, "teste3", "teste3", "2023", 3F));

		// When
		Mockito.when(this.carDao.findAll())
				.thenReturn(listCarEntity);

		// Then
		List<Car> listCar = this.carFindAllUseCase.execute();
		Assertions.assertNotNull(listCar);
		Assertions.assertEquals(listCar.size(), 3);

		for (int i = 0; i < listCarEntity.size(); i++) {
			CarModelConverterTest.isEquals(listCarEntity.get(i), listCar.get(i));
		}
	}

	@Test
	@DisplayName("Method execute() - Empty")
	public void executeEmpty() {
		// When
		Mockito.when(this.carDao.findAll())
				.thenReturn(null);

		// Then
		List<Car> listCar = this.carFindAllUseCase.execute();
		Assertions.assertNotNull(listCar);
		Assertions.assertEquals(listCar.size(), 0);
	}

}