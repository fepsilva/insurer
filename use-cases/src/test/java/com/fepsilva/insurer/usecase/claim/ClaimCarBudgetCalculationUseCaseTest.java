package com.fepsilva.insurer.usecase.claim;

import com.fepsilva.insurer.converter.CarModelConverterTest;
import com.fepsilva.insurer.converter.ClaimModelConverterTest;
import com.fepsilva.insurer.converter.CustomerModelConverterTest;
import com.fepsilva.insurer.converter.DriverModelConverterTest;
import com.fepsilva.insurer.converter.InsuranceModelConverterTest;
import com.fepsilva.insurer.dao.ClaimDao;
import com.fepsilva.insurer.model.Car;
import com.fepsilva.insurer.model.Claim;
import com.fepsilva.insurer.model.Customer;
import com.fepsilva.insurer.model.Driver;
import com.fepsilva.insurer.model.Insurance;
import com.fepsilva.insurer.usecase.claim.impl.ClaimCarBudgetCalculationUseCaseImpl;
import com.fepsilva.insurer.usecase.customer.CustomerFindUseCase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class ClaimCarBudgetCalculationUseCaseTest {

	private static final BigDecimal PERCENT_WITH_SINISTER = BigDecimal.valueOf(0.02);

	@Mock
	private ClaimDao riskAgeDao;

	@Mock
	private ClaimFindAllUseCase claimFindAllUseCase;

	@Mock
	private CustomerFindUseCase customerFindUseCase;

	@InjectMocks
	private ClaimCarBudgetCalculationUseCaseImpl claimCarBudgetCalculationUseCase;

	@Test
	@DisplayName("Method execute() - Should execute successful with claim")
	public void executeSuccessfulWithClaim() {
		// Given
		Car car = CarModelConverterTest.createNewCar(1L, "teste", "teste", "2023", 0F);
		Driver driver = DriverModelConverterTest.createNewDriver(1L, "teste", LocalDate.now());

		List<Claim> listClaim = List.of(
				ClaimModelConverterTest.createNewClaim(1L, car, driver, LocalDate.now()));

		// Then
		executeSuccessfulAux(car, listClaim, PERCENT_WITH_SINISTER);
	}

	@Test
	@DisplayName("Method execute() - Should execute successful without claim")
	public void executeSuccessfulWithoutClaim() {
		// Given
		Driver driver = DriverModelConverterTest.createNewDriver(1L, "teste", LocalDate.now());
		Car carWithClaim = CarModelConverterTest.createNewCar(1L, "teste", "teste", "2023", 0F);
		Car carWithoutClaim = CarModelConverterTest.createNewCar(2L, "teste", "teste", "2023", 0F);

		List<Claim> listClaim = List.of(
				ClaimModelConverterTest.createNewClaim(1L, carWithClaim, driver, LocalDate.now()));

		// Then
		executeSuccessfulAux(carWithoutClaim, listClaim, BigDecimal.ZERO);
	}

	@Test
	@DisplayName("Method execute() - Without list of claim")
	public void executeWithoutList() {
		// Given
		Car car = CarModelConverterTest.createNewCar(1L, "teste", "teste", "2023", 0F);
		Driver driver = DriverModelConverterTest.createNewDriver(1L, "teste", LocalDate.now());
		Customer customer = CustomerModelConverterTest.createNewCustomer(1L, "teste", driver);
		Insurance insurance = InsuranceModelConverterTest.createNewInsurance(1L, customer, LocalDateTime.now(), LocalDateTime.now(), car, true, 0F, 0F);

		// When
		Mockito.when(this.claimFindAllUseCase.execute())
				.thenReturn(null);

		// Then
		BigDecimal value = this.claimCarBudgetCalculationUseCase.execute(insurance);
		Assertions.assertNotNull(value);
		Assertions.assertEquals(value, BigDecimal.ZERO);
	}

	@Test
	@DisplayName("Method execute() - Without car")
	public void executeWithoutCar() {
		// Given
		Insurance insurance = InsuranceModelConverterTest.createNewInsurance(1L, null, LocalDateTime.now(), LocalDateTime.now(), null, true, 0F, 0F);

		// Then
		BigDecimal value = this.claimCarBudgetCalculationUseCase.execute(insurance);
		Assertions.assertNotNull(value);
		Assertions.assertEquals(value, BigDecimal.ZERO);
	}

	private void executeSuccessfulAux(Car car, List<Claim> listClaim, BigDecimal result) {
		// Given
		Driver driver = DriverModelConverterTest.createNewDriver(1L, "teste", LocalDate.now());
		Customer customer = CustomerModelConverterTest.createNewCustomer(1L, "teste", driver);
		Insurance insurance = InsuranceModelConverterTest.createNewInsurance(1L, customer, LocalDateTime.now(), LocalDateTime.now(), car, true, 0F, 0F);

		// When
		Mockito.when(this.claimFindAllUseCase.execute())
				.thenReturn(listClaim);

		// Then
		BigDecimal value = this.claimCarBudgetCalculationUseCase.execute(insurance);
		Assertions.assertNotNull(value);
		Assertions.assertEquals(value, result);
	}

}