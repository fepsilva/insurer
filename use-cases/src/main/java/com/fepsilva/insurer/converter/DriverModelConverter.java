package com.fepsilva.insurer.converter;

import com.fepsilva.insurer.entity.DriverEntity;
import com.fepsilva.insurer.model.Driver;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

public class DriverModelConverter {

	public static DriverEntity modelToEntity(Driver driver) {
		if (driver == null) {
			return null;
		}

		return getModelMapper().map(driver, DriverEntity.class);
	}

	public static Driver entityToModel(DriverEntity driverEntity) {
		if (driverEntity == null) {
			return null;
		}

		return getModelMapper().map(driverEntity, Driver.class);
	}

	private static ModelMapper getModelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

		return modelMapper;
	}

}