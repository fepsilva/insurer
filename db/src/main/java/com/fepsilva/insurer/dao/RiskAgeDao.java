package com.fepsilva.insurer.dao;

import com.fepsilva.insurer.entity.RiskAgeEntity;

import java.util.List;
import java.util.Optional;

public interface RiskAgeDao {

	public RiskAgeEntity persist(RiskAgeEntity riskAgeEntity);
	public List<RiskAgeEntity> findAll();
	public Optional<RiskAgeEntity> find(Long riskAgeId);
	public RiskAgeEntity update(RiskAgeEntity riskAgeEntity);
	public void delete(Long riskAgeId);

}