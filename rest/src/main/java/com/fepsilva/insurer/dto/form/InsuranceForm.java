package com.fepsilva.insurer.dto.form;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Formulário de cadastro de seguro")
public class InsuranceForm {

	@NotNull(message = "Não pode estar vazio")
	@Schema(description = "Identificador do cliente")
	private Long customerId;

	@NotNull(message = "Não pode estar vazio")
	@Schema(description = "Identificador do carro")
	private Long carId;

	@NotNull(message = "Não pode estar vazio")
	@Schema(description = "Flag se o orçamento está ativo")
	private Boolean active;

}