package com.fepsilva.insurer.converter;

import com.fepsilva.insurer.entity.CarEntity;
import com.fepsilva.insurer.entity.ClaimEntity;
import com.fepsilva.insurer.entity.DriverEntity;
import com.fepsilva.insurer.model.Car;
import com.fepsilva.insurer.model.Claim;
import com.fepsilva.insurer.model.Driver;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

@ExtendWith(MockitoExtension.class)
public class ClaimModelConverterTest {

	@Test
	@DisplayName("Method modelToEntity() - Should execute successful")
	public void modelToEntitySuccessful() {
		// Given
		Car car = CarModelConverterTest.createNewCar(1L, "teste", "teste", "2023", 0F);
		Driver driver = DriverModelConverterTest.createNewDriver(1L, "teste", LocalDate.now());
		Claim claim = createNewClaim(1L, car, driver, LocalDate.now());

		// Then
		ClaimEntity claimEntity = ClaimModelConverter.modelToEntity(claim);
		isEquals(claimEntity, claim);
	}

	@Test
	@DisplayName("Method modelToEntity() - Without value")
	public void modelToEntityWithoutValue() {
		// Given
		Claim claim = null;

		// Then
		ClaimEntity claimEntity = ClaimModelConverter.modelToEntity(claim);
		Assertions.assertNull(claimEntity);
	}

	@Test
	@DisplayName("Method entityToModel() - Should execute successful")
	public void entityToModelSuccessful() {
		// Given
		CarEntity car = CarModelConverterTest.createNewCarEntity(1L, "teste", "teste", "2023", 0F);
		DriverEntity driver = DriverModelConverterTest.createNewDriverEntity(1L, "teste", LocalDate.now());
		ClaimEntity claimEntity = createNewClaimEntity(1L, car, driver, LocalDate.now());

		// Then
		Claim claim = ClaimModelConverter.entityToModel(claimEntity);
		isEquals(claimEntity, claim);
	}

	@Test
	@DisplayName("Method entityToModel() - Without value")
	public void entityToModelWithoutValue() {
		// Given
		ClaimEntity claimEntity = null;

		// Then
		Claim claim = ClaimModelConverter.entityToModel(claimEntity);
		Assertions.assertNull(claim);
	}

	public static ClaimEntity createNewClaimEntity(Long id, CarEntity car, DriverEntity driver, LocalDate eventDate) {
		return ClaimEntity.builder()
				.id(id)
				.car(car)
				.driver(driver)
				.eventDate(eventDate)
				.build();
	}

	public static Claim createNewClaim(Long id, Car car, Driver driver, LocalDate eventDate) {
		return Claim.builder()
				.id(id)
				.car(car)
				.driver(driver)
				.eventDate(eventDate)
				.build();
	}

	public static void isEquals(ClaimEntity claimEntity, Claim claim) {
		Assertions.assertNotNull(claimEntity);
		Assertions.assertNotNull(claim);

		Assertions.assertEquals(claimEntity.getId(), claim.getId());
		Assertions.assertEquals(claimEntity.getEventDate(), claim.getEventDate());

		CarModelConverterTest.isEquals(claimEntity.getCar(), claim.getCar());
		DriverModelConverterTest.isEquals(claimEntity.getDriver(), claim.getDriver());
	}

}