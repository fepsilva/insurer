package com.fepsilva.insurer.dto.view;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Visualização do cadastro de cliente")
public class CustomerView {

	@Schema(description = "Identificador do carro")
	private Long id;

	@Schema(description = "Nome do cliente")
	private String name;

	@Schema(description = "Dados do motorista")
	private DriverView driver;

}