package com.fepsilva.insurer.usecase.customer;

import com.fepsilva.insurer.model.Customer;

public interface CustomerUpdateUseCase {

	public Customer execute(Long customerId, Customer customer);

}