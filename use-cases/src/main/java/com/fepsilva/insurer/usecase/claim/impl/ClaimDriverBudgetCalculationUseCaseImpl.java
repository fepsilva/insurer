package com.fepsilva.insurer.usecase.claim.impl;

import com.fepsilva.insurer.model.Claim;
import com.fepsilva.insurer.model.Driver;
import com.fepsilva.insurer.model.Insurance;
import com.fepsilva.insurer.usecase.claim.ClaimDriverBudgetCalculationUseCase;
import com.fepsilva.insurer.usecase.claim.ClaimFindAllUseCase;
import com.fepsilva.insurer.usecase.customer.CustomerFindUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class ClaimDriverBudgetCalculationUseCaseImpl implements ClaimDriverBudgetCalculationUseCase {

	private static final BigDecimal PERCENT_WITH_SINISTER = BigDecimal.valueOf(0.02);

	@Autowired
	private ClaimFindAllUseCase claimFindAllUseCase;

	@Autowired
	private CustomerFindUseCase customerFindUseCase;

	public BigDecimal execute(Insurance insurance) {
		if (insurance.getCustomer() != null) {
			if (insurance.getCustomer().getDriver() == null) {
				insurance.setCustomer(this.customerFindUseCase.execute(insurance.getCustomer().getId()));
			}

			Driver driver = insurance.getCustomer().getDriver();
			List<Claim> listClaim = claimFindAllUseCase.execute();

			if (listClaim != null) {
				return listClaim.stream()
						.filter(item -> item.getDriver().getId() == driver.getId())
						.findFirst()
						.map(item -> PERCENT_WITH_SINISTER)
						.orElseGet(() -> BigDecimal.ZERO);
			}
		}

		return BigDecimal.ZERO;
	}

}