package com.fepsilva.insurer.converter;

import com.fepsilva.insurer.entity.DriverEntity;
import com.fepsilva.insurer.model.Driver;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

@ExtendWith(MockitoExtension.class)
public class DriverModelConverterTest {

	@Test
	@DisplayName("Method modelToEntity() - Should execute successful")
	public void modelToEntitySuccessful() {
		// Given
		Driver driver = createNewDriver(1L, "teste", LocalDate.now());

		// Then
		DriverEntity driverEntity = DriverModelConverter.modelToEntity(driver);
		isEquals(driverEntity, driver);
	}

	@Test
	@DisplayName("Method modelToEntity() - Without value")
	public void modelToEntityWithoutValue() {
		// Given
		Driver driver = null;

		// Then
		DriverEntity driverEntity = DriverModelConverter.modelToEntity(driver);
		Assertions.assertNull(driverEntity);
	}

	@Test
	@DisplayName("Method entityToModel() - Should execute successful")
	public void entityToModelSuccessful() {
		// Given
		DriverEntity driverEntity = createNewDriverEntity(1L, "teste", LocalDate.now());

		// Then
		Driver driver = DriverModelConverter.entityToModel(driverEntity);
		isEquals(driverEntity, driver);
	}

	@Test
	@DisplayName("Method entityToModel() - Without value")
	public void entityToModelWithoutValue() {
		// Given
		DriverEntity driverEntity = null;

		// Then
		Driver driver = DriverModelConverter.entityToModel(driverEntity);
		Assertions.assertNull(driver);
	}

	public static DriverEntity createNewDriverEntity(Long id, String document, LocalDate birthDate) {
		return DriverEntity.builder()
				.id(id)
				.document(document)
				.birthDate(birthDate)
				.build();
	}

	public static Driver createNewDriver(Long id, String document, LocalDate birthDate) {
		return Driver.builder()
				.id(id)
				.document(document)
				.birthDate(birthDate)
				.build();
	}

	public static void isEquals(DriverEntity driverEntity, Driver driver) {
		Assertions.assertNotNull(driverEntity);
		Assertions.assertNotNull(driver);

		Assertions.assertEquals(driverEntity.getId(), driver.getId());
		Assertions.assertEquals(driverEntity.getDocument(), driver.getDocument());
		Assertions.assertEquals(driverEntity.getBirthDate(), driver.getBirthDate());
	}

}