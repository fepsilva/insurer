package com.fepsilva.insurer.dto.view;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Visualização do cadastro de motorista")
public class DriverView {

	@Schema(description = "Identificador do carro")
	private Long id;

	@Schema(description = "CNH do motorista")
	private String document;

	@Schema(description = "Data de nascimento do motorista")
	private LocalDate birthDate;

}