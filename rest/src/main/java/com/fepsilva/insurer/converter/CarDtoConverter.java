package com.fepsilva.insurer.converter;

import com.fepsilva.insurer.dto.form.CarForm;
import com.fepsilva.insurer.dto.view.CarView;
import com.fepsilva.insurer.model.Car;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

public class CarDtoConverter {

	public static Car idToEntity(Long carId) {
		return Car.builder()
				.id(carId)
				.build();
	}

	public static Car dtoToEntity(CarForm carForm) {
		if (carForm == null) {
			return null;
		}

		return getModelMapper().map(carForm, Car.class);
	}

	public static CarView entityToDto(Car car) {
		if (car == null) {
			return null;
		}

		return getModelMapper().map(car, CarView.class);
	}

	private static ModelMapper getModelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

		return modelMapper;
	}

}