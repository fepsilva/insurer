package com.fepsilva.insurer.usecase.car;

public interface CarDeleteUseCase {

	public void execute(Long carId);

}