package com.fepsilva.insurer.converter;

import com.fepsilva.insurer.entity.CustomerEntity;
import com.fepsilva.insurer.model.Customer;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

public class CustomerModelConverter {

	public static CustomerEntity modelToEntity(Customer customer) {
		if (customer == null) {
			return null;
		}

		CustomerEntity customerEntity = getModelMapper().map(customer, CustomerEntity.class);
		customerEntity.setDriver(DriverModelConverter.modelToEntity(customer.getDriver()));

		return customerEntity;
	}

	public static Customer entityToModel(CustomerEntity customerEntity) {
		if (customerEntity == null) {
			return null;
		}

		Customer customer = getModelMapper().map(customerEntity, Customer.class);
		customer.setDriver(DriverModelConverter.entityToModel(customerEntity.getDriver()));

		return customer;
	}

	private static ModelMapper getModelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

		return modelMapper;
	}

}